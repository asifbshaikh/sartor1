module Spree
  class GiftCardsController < Spree::StoreController

    def new
      find_gift_card_variants
      @gift_card = GiftCard.new
      @gift_card.send_on = 'now';
      @gift_card.delivery_type = 'email'
      
      @ties = Spree::Product.active.is_tie
    end

    def create
      begin
        # Wrap the transaction script in a transaction so it is an atomic operation
        Spree::GiftCard.transaction do
          
          @gift_card = GiftCard.new(gift_card_params)
          
          if (gift_card_params["send_on_date"].length > 1)
            @gift_card.send_on_date = Date::strptime(gift_card_params["send_on_date"], "%m/%d/%Y")
          end
          
          @gift_card.save!
          
          order = current_order(create_order_if_necessary: true)
                    
          # Create line item
          line_item = LineItem.new(quantity: 1)
          line_item.gift_card = @gift_card
          line_item.variant = @gift_card.variant
          line_item.price = @gift_card.variant.price
          # Create sub line item
          tie_variant_id = @gift_card.tie_variant_id
          unless tie_variant_id.nil? || tie_variant_id == 0
            tie_variant = Spree::Variant.find(tie_variant_id)
            sub_line_item = LineItem.new(quantity: 1)
            line_item.sub_lineitems << sub_line_item
            sub_line_item.parent_lineitem = line_item
            sub_line_item.variant = tie_variant
            sub_line_item.price = tie_variant.price

            line_item.sub_lineitems.first.order = order
          end
          
          # Add to order          
          order.line_items << line_item
          line_item.order = order

          order.update_totals
          order.save!
          # Save gift card
          @gift_card.line_item = line_item
          @gift_card.save!
        end
        redirect_to cart_path
      rescue ActiveRecord::RecordInvalid
        find_gift_card_variants
        render :action => :new
      end
    end

    private

    def find_gift_card_variants
      gift_card_product_ids = Product.not_deleted.where(is_gift_card: true).where(product_type: 'GIFT').pluck(:id)
      @gift_card_variants = Variant.joins(:prices).where(["amount > 0 AND product_id IN (?)", gift_card_product_ids]).order("amount")
    end

    def gift_card_params
      params.require(:gift_card).permit(:email, :name, :note, :variant_id, :send_on, :tie_variant_id, :send_on_date, :delivery_type)
    end

  end
end
