Spree::UsersController.class_eval do
  respond_to :html

  def show
    @orders = @user.orders
    @profiles = @user.profiles
  end

  def redeem_gift_cards
    redemption_code = params[:redemption_code]
    gift_card = Spree::VirtualGiftCard.find_by(redemption_code: redemption_code)
    @user = spree_current_user
    @profiles = @user.profiles
    check_validity gift_card
    redirect_to '/account'
  end

  def show_orders
    @user = spree_current_user
    @orders = @user.orders.order(:completed_at).where("spree_orders.completed_at IS NOT NULL")
    @ordersdesc = @orders.sort!.reverse
    @profiles = @user.profiles
  end

  def show_history
    @user = spree_current_user
    @wishlists = spree_current_user.wishlists
    @profiles = @user.profiles
    respond_with(@wishlist)
  end

  def show_address
    @user = spree_current_user
    @profiles = @user.profiles
  end

  def sartor_wardrobe
    @user = spree_current_user
    @orders = @user.orders
    @profiles = @user.profiles
  end

  def loyalty_points
    @user = spree_current_user
    @profiles = @user.profiles
  end

  def redeem_cards
    @user = spree_current_user
    @profiles = @user.profiles
  end

  def check_validity(gift_card)
    if !gift_card.nil?
      if gift_card.redeem(spree_current_user)
        flash[:success] = Spree.t('Gift Card redeemed successfully!
        Amount credited in your store credit!')
      else
        flash[:error] = Spree.t('This gift code is not valid anymore!')
      end
    else
      flash[:error] = Spree.t('Invalid code! Enter again')
    end
  end
end
