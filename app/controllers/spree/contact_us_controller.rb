class Spree::ContactUsController < Spree::StoreController
	def show
    session[:return_to] = request.fullpath
		@contactUsSelected = "selected"
	end

	def create
		email = params[:email]
		name = params[:name]
		message = params[:message]
		contact_mailer_object = Spree::ContactMailerController.new
       	contact_mailer_object.send_mail(email, name, message)
       	redirect_to "/contact"
	end
end
