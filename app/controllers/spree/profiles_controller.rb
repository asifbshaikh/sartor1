class Spree::ProfilesController < Spree::BaseController
  rescue_from ActiveRecord::RecordNotFound, :with => :render_404
  ssl_required
  respond_to :html
  load_and_authorize_resource :except => [:get_detailed_profile, :createnewmeasurements]

  include Spree::Core::ControllerHelpers::Order  # needed to display cart; don't want to inherit from StoreController because authorization failure fwds to blank page

  include ReturnTo
  after_filter :store_referer

  def new_profile
    # descendents must override
  end

  def profile_params
    # todo unused - remove?
    # descendents must override
  end

  def new
    @user ||= spree_current_user
    # store_location  #superseded by store_referer
    authorize! params[:action].to_sym, @user
    @profile = new_profile
    @profile.name ||= "My Measurements"
    @profile.embroidery_name = "#{@user.first_name} #{@user.last_name}"
  end

  def create
    #@profile = new_profile
    #@profile.update_attributes(params[:profile])
    #@profile.predecessor.user = spree_current_user
   
    if Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile').order("created_at DESC").length > 0
       
      #mapped the params for body profile
      if params[:controller] == "spree/detailed_profiles"
        @historic = Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile').order("created_at DESC").first
        @historic.heir.neck = params[:profile][:neck]
        @historic.heir.sleeve = params[:profile][:sleeve]
        @historic.heir.yoke = params[:profile][:yoke]
        @historic.heir.chest = params[:profile][:chest] 
        @historic.heir.waist = params[:profile][:waist]
        @historic.heir.hip = params[:profile][:hip]
        @historic.heir.length = params[:profile][:length]
        @historic.heir.bicep = params[:profile][:bicep]
        @historic.heir.wrist = params[:profile][:wrist]
        @historic.heir.length_untucked = params[:profile][:length_untucked]
        @historic.heir.wrist_watch = params[:profile][:wrist_watch]
        @historic.heir.watch_hand = params[:profile][:watch_hand]

        #update height and weight
        @historic.height_ft =  params[:profile][:height_ft]
        @historic.height_in =  params[:profile][:height_in]
        @historic.weight =  params[:profile][:weight]
        @historic.save
 
        if @historic.heir.save 
          if @historic.heir_type == "ShirtProfile"
            redirect_to profiles_path
            flash[:success] = 'Successfully Created.'
          else
            redirect_to '/shirt_profiles/new?dpid='+@historic.id.to_s+''
          end
        else
          render action: "new"
        end
      else
          #shirt measurement
        if params[:controller] == "spree/shirt_profiles"
          @profile = new_profile
          update_attributes(@profile)
          @profile.predecessor.user = spree_current_user
          if @profile.save
            if @profile.predecessor.heir_type == "ShirtProfile"
              redirect_to profiles_path
              flash[:success] = 'Successfully Created.'
            else
              redirect_to '/shirt_profiles/new?dpid='+@profile.predecessor.id.to_s+''
            end
          else
          render action: "new"
          end
        end
      end
    else 
    #new shirt or body measurement profile 
      @profile = new_profile
      update_attributes(@profile)
      @profile.predecessor.user = spree_current_user
      if @profile.save
        if @profile.predecessor.heir_type == "ShirtProfile"
          redirect_to profiles_path
          flash[:success] = 'Successfully Created.'
        else
          redirect_to '/shirt_profiles/new?dpid='+@profile.predecessor.id.to_s+''
        end
      else
        render action: "new"
      end
    end  
  end

  def index
    @detailed_profiles = Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile')
    @shirt_profiles = Profile.where(:user_id => spree_current_user, :heir_type => 'ShirtProfile')
    $redirect_to_product_detail = false;
    if(params[:id] != nil)
      $redirect_to_product_detail = true;
    end
  end

  def edit
    # TODO handle not found case?
    @detailedProfiles = Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile')
    @profile = @profile.heir
    @user = spree_current_user
    @profiles = @user.profiles
    session[:return_to] ||= request.referer  # store referrer so we can go back to the page
    @showdeleteButton = true;

      @shirtProfiles = Profile.find(params[:id]).heir

    if @profile.is_a? DetailedProfile
      render "spree/detailed_profiles/edit"
    else
      render "spree/shirt_profiles/edit"
    end

  end

  def destroy
    #@profile = Profile.find(params[:id])
    @profile.destroy
       #todo: I18n for resource name
    flash[:notice] = I18n.t(:successfully_removed, :resource => "Measurement profile")
    redirect_to(request.env['HTTP_REFERER'] || account_path) unless request.xhr?
  end

 private
  def detailed_profile_params
    params.require(:profile).permit(:id ,:name, :embroidery_name, :default_fit_code, :height_ft, :height_in, :weight, :neck, :yoke, :sleeve, :chest, :waist, :hip, :length, :bicep, :wrist, :wrist_watch, :watch_hand, :length, :length_untucked, :default_length_code)
  end  

end
