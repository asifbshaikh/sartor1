Spree::UserSessionsController.class_eval do
  
    # Store referer page for the case of login/create acct to add measurements to a shirt in product or cart view
  
    include ReturnTo
    
    after_filter :store_referer, :only => [:create, :new]
    
end