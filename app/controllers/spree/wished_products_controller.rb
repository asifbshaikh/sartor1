class Spree::WishedProductsController < Spree::StoreController
  respond_to :html

  def create
    @wished_product = Spree::WishedProduct.new(wished_product_attributes)
    @wishlist = spree_current_user.wishlist
    if @wishlist.include? params[:wished_product][:variant_id]
      @wished_product = @wishlist.wished_products.detect {|wp| wp.variant_id == params[:wished_product][:variant_id].to_i }
    else
      @wished_product.wishlist = spree_current_user.wishlist
      if @wished_product.save
        wished_product_configuration = ShirtConfiguration.new(shirt_configuration_attributes)
        wished_product_configuration.wished_product_id =  @wished_product.id
        wished_product_configuration.save
      end
    end

    respond_with(@wished_product) do |format|
      format.html { redirect_to wishlist_url(@wishlist) }
    end
  end

  def destroy
    @wished_product = Spree::WishedProduct.find(params[:id])
    @wished_product.destroy

    respond_with(@wished_product) do |format|
      format.html { redirect_to wishlist_url(@wished_product.wishlist) }
    end
  end

  def update_wished_product
    if params[:wished_product_configuration]
      @wished_product_configuration = Spree::WishedProduct.find(params[:wished_product_configuration][:wishlist_id])
      @wished_product_configuration.shirt_configuration.update(shirt_configuration_attributes)
      if @wished_product_configuration.update(:profile_id => params[:wished_product_configuration][:profile_id].to_i)
        respond_to do |format|
            msg = { :status => "200", :message => "Success!", :wishlist_path => wishlist_url(@wished_product_configuration.wishlist)}
            format.json  { render :json => msg }
        end
      end
    else
      respond_to do |format|
          msg = { :status => "500", :message => "Unsuccess!", :wishlist_path => wishlist_url(@wished_product_configuration.wishlist)}
          format.json  { render :json => msg }
      end
    end
  end

  def wished_product_attributes
    params.require(:wished_product).permit(:variant_id, :wishlist_id, :remark, :profile_id)
  end

  def shirt_configuration_attributes
    if params[:wished_product_configuration]
      params.require(:wished_product_configuration).permit(:collar_code, :cuff_code, :pocket_code, :button_code, :placket_code, :fit_code, :length_code, :comment)
    else
      params.require(:wished_product).permit(:collar_code, :cuff_code, :pocket_code, :button_code, :placket_code, :fit_code, :length_code, :comment)
    end
  end

end
