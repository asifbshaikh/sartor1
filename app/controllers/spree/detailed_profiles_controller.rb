class Spree::DetailedProfilesController < Spree::ProfilesController
  ssl_required

  def new_profile
    profile = DetailedProfile.new
    profile.default_fit_code = 1 # Slim; see ShirtConfiguration::FITS
    @showdeleteButton = false;
    profile
  end

  def update
    if update_attributes(@detailed_profile)
      #todo: I18n for resource name
      flash[:notice] = "Successfully Updated"
    end
    if (params['_stay_in'] == 'edit')
      redirect_to profiles_path(@detailed_profile.predecessor)
    else
      redirect_to profiles_path
    end
  end

  def destroy
    @detailed_profile.destroy
       #todo: I18n for resource name
       flash[:notice] = "Successfully Removed"
    redirect_to profiles_path
  end

  def show
    # TODO handle not found case?
    @profile = @profile.heir
    session[:return_to] ||= request.referer  # store referrer so we can go back to the page
    render "spree/detailed_profiles/show"
  end

  def update_attributes(profile)
    profile.update_attributes(detailed_profile_params)
  end

  def detailed_profile_params
    params.require(:profile).permit(:name, :embroidery_name, :default_fit_code, :height_ft, :height_in, :weight, :neck, :yoke, :sleeve, :chest, :waist, :hip, :length, :bicep, :wrist, :wrist_watch, :watch_hand, :length, :length_untucked, :default_length_code)
  end

end
