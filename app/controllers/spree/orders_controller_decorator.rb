module Spree
  OrdersController.class_eval do

    def populate
      variant = Spree::Variant.find_by(:id => params[:variant_id])
      quantity = params[:quantity].to_i > 0 ? params[:quantity].to_i : 1
      flag = false

      if($current_line_item_id != nil)
        config = params[:order]
        Spree::LineItem.find($current_line_item_id).shirt_configuration.update(:collar_code => config['collar_code'], :cuff_code => config['cuff_code'],
          :pocket_code => config['pocket_code'], :placket_code => config['placket_code'], :button_code => config['button_code'], :comment => config['comment'])
        Spree::LineItem.find($current_line_item_id).update(:profile_id => params[:profile_id])
        flag = true
      else
        quantity.times do |q|
          populator = Spree::OrderPopulator.new(current_order(create_order_if_necessary: true), current_currency)
          config = ShirtConfiguration.new(config_params) if params[:product_type] == "SHIRT"
          if populator.populate(params[:variant_id], params[:profile_id], config, 1)
            current_order.ensure_updated_shipments
            flag = true
          else
            flag = false
            break
          end

        end
      end

      if flag
        respond_with(@order) do |format|
          if($current_line_item_id==nil)
              format.html { redirect_to cart_path}
          else
              format.html { redirect_to cart_path+'?pid='+$current_line_item_id.to_s}
          end
        end
      else
        flash[:error] = populator.errors.full_messages.join("")
        redirect_to :back
      end
    end

    def edit
      session[:return_to] = request.fullpath
      @order = current_order || Order.new
      @update_line_item_id = params[:pid]
      @shirt_profiles = Profile.where(:user_id => spree_current_user, :heir_type => 'ShirtProfile')
      associate_user

      # Set Default Shirt Configurations If Nil
      @order.line_items.each do |line_item|
        if line_item.shirt_configuration
          shirt_configuration = line_item.shirt_configuration
          default_shirt_configuration = line_item.variant.product.shirt_configuration.dup
          if shirt_configuration.collar_code == 0 or shirt_configuration.collar_code == nil
            line_item.shirt_configuration.update(:collar_code => default_shirt_configuration.collar_code)
          end
          if shirt_configuration.cuff_code == 0 or shirt_configuration.cuff_code == nil
            line_item.shirt_configuration.update(:cuff_code => default_shirt_configuration.cuff_code)
          end
          if shirt_configuration.pocket_code == 0 or shirt_configuration.pocket_code == nil
            line_item.shirt_configuration.update(:pocket_code => default_shirt_configuration.pocket_code)
          end
          if shirt_configuration.placket_code == 0 or shirt_configuration.placket_code == nil
            line_item.shirt_configuration.update(:placket_code => default_shirt_configuration.placket_code)
          end
          if shirt_configuration.button_code == 0 or shirt_configuration.button_code == nil or shirt_configuration.button_code <=3
            line_item.shirt_configuration.update(:button_code => default_shirt_configuration.button_code)
          end
        end
      end
      # End

       if spree_current_user and current_order and !current_order.promotions.empty?
           $promotion_rules_count = current_order.promotions[0].promotion_rules[0].preferences[:count];
           calculator_id = current_order.promotions[0].promotion_actions[0].id;
          action_perc = Spree::Calculator.where(:calculable_id => calculator_id)[0];
           if action_perc
             $promotion_perc = action_perc.preferences[:percent].to_s;
           end
       end
    end

    def update
      @order.line_items.each do |line_item|
        #if !line_item.variant.product.is_available
           # redirect_to 'cart/' and return
       # end
        giftCard = VirtualGiftCardsDetails.where(:line_item_id => line_item.id)
        if giftCard.length > 0 and spree_current_user
          giftCard[0].update_attribute(:purchaser_id, spree_current_user.id)
          giftCard[0].update_attribute(:recipient_mail, spree_current_user.email)
        end

      end
      if @order.contents.update_cart(order_params)
        respond_with(@order) do |format|
          format.html do
            if params.has_key?(:checkout)
              @order.next if @order.cart?
              redirect_to checkout_state_path(@order.checkout_steps.first)
            else
              redirect_to cart_path
            end
          end
        end
      else
        respond_with(@order)
      end
    end

    def removeUnavailableLineItem
      @unavailable_line_item_ids = Array.new
      if current_order
        lia=Hash.new
        li = Hash.new
        count = 0
        current_order.line_items.each do |line_item|
          if !line_item.variant.product.is_available
            l = Hash.new
            l["id"] = line_item.id
            l["quantity"] = 0
            li[count.to_s] = l
            count = count + 1
          end
        end
        lia["line_items_attributes"] = li

        if current_order.contents.update_cart(lia)
          respond_with(current_order) do |format|
            format.html do
              if params.has_key?(:checkout)
                current_order.next if current_order.cart?
                redirect_to checkout_state_path(current_order.checkout_steps.first)
              else
                redirect_to cart_path
              end
            end
          end
        else
          respond_with(current_order)
        end
      end
    end

    def config_params
      params.require(:order).permit(:collar_code, :cuff_code, :pocket_code, :button_code, :placket_code, :fit_code, :length_code, :comment)
    end
  end
end
