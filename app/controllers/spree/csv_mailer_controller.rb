class Spree::CsvMailerController < Spree::StoreController


  def send_mail(csv_error ,email, product_path, image_path, submitted_at)
    Spree::CsvMailer.send_notification_to_admin(csv_error, email, product_path, image_path, submitted_at).deliver
  end
  
  def send_mail_orders(csv_error ,email, order_path, submitted_at)
    Spree::CsvMailer.send_notification_to_admin_orders(csv_error, email, order_path, submitted_at).deliver
  end
  
end
