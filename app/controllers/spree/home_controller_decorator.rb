module Spree
  class HomeController < Spree::StoreController
    helper 'spree/products'
    layout "spree/layouts/home"
    respond_to :html

    # def index
    #   @searcher = build_searcher(params)
    #   @products = @searcher.retrieve_products
    #   @taxonomies = Spree::Taxonomy.includes(root: :children)
    # end
    # dummy commit
  end
end