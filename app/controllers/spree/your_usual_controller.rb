class Spree::YourUsualController < Spree::BaseController
  def add

    configurations = params[:configurations]
    # puts 'collar_code: '+configurations["collar_code"]
    # user_id is set from back-end (admin portal) via params.
      user_id = configurations["user_id"]
      if(!user_id)
        user_id = spree_current_user.id
      end
      collar_code = configurations["collar_code"]
      cuff_code = configurations["cuff_code"]
      pocket_code = configurations["pocket_code"]
      #length_code = configurations["length_code"]
      button_code = configurations["button_code"]
      placket_code = configurations["placket_code"]
      profile_id = configurations["profile_id"]

      object = UsualStyle.find_or_initialize_by(user_id: user_id)
      object.update(collar_code: collar_code,cuff_code: cuff_code,
      pocket_code: pocket_code,button_code: button_code,placket_code: placket_code, profile_id: profile_id)

	  respond_to do |format|
	    msg = { :status => "ok", :message => "Success!"}
	    format.json  { render :json => msg }
	  end
  end

end
