class Spree::WardrobesController < Spree::BaseController
  include Spree::Core::ControllerHelpers::Order
  def show
    shirt_slug = params[:shirt_slug]
    puts "Showing unavailable shirts. Shirt_Slug: " + shirt_slug.to_s
    selected_product = Spree::Product.find_by(slug: shirt_slug)
    user = spree_current_user
    orders = user.orders
    user_has_taken_shirt = false
    line_item_id = nil    
    
    orders.each do |order|
      shirts = order.line_items
      shirts.each do |shirt|
        product = shirt.product
        if(selected_product.id == product.id)
           line_item_id = shirt.id
           user_has_taken_shirt = true
           break;
        end
       end
    end    
    
    
    @products = Spree::Product.allshirts(current_currency)
    @product = @products.friendly.find(params[:shirt_slug])
    shirt_config = ShirtConfiguration.find_by(line_item_id: line_item_id)
    @shirt_configuration = shirt_config
    @shirt_configuration.product_id = nil   
  end
  
  def current_currency
    Spree::Config[:currency]
  end

  
end