module Spree
  module Admin
    class ShowMockupController < Spree::Admin::BaseController

      def save_jobs_to_line_items
        order_id = params[:id]
        order = Spree::Order.find_by(:number => order_id)
        jobs = params[:jobs]
        if jobs != nil
          jobs.each do |line_item_id, job_number|
            line_item = Spree::LineItem.find_by(:id => line_item_id)
            line_item.job_number = job_number
            line_item.save!
          end
        end
        redirect_to show_admin_order_put_path(order)
      end
      
      def add_tracking_number
        order_id = params[:id]
        order = Spree::Order.find_by(:number => order_id)
        shipment_number = params[:shipment_number]
        tracking_number = params[:tracking_number]
        shipment = Spree::Shipment.find_by(:number => shipment_number)
        shipment.tracking = tracking_number
        shipment.save!
        redirect_to show_admin_order_put_path(order)
      end

      def update_shirt_status
        order_id = params[:id]
        order = Spree::Order.find_by(:number => order_id)
        checkboxMap = params[:checkboxMap]

        shipment = order.shipments.create(stock_location_id: params[:new_stock_location_id])
        shipment.address = order.ship_address
        shipment.save!
        puts "***********order id: " + order_id.to_s
        puts "***********New shipment create: " + shipment.number.to_s + " Shipment_id: " +shipment.id.to_s

        checkboxMap.each do |line_item_id, is_status_changed|
          puts "line_item_id:  " + line_item_id.to_s + " is_status_changed:  " + is_status_changed.to_s
          if is_status_changed == "true"
            puts "Chaning status of line item:  " + line_item_id.to_s
            line_item = Spree::LineItem.find_by(:id => line_item_id)
            change_shirts_status line_item, shipment
          end
        end
        shipment.add_shipping_methods
        shipment.determine_shipment_state
        if shipment.stock_location.name == "Ship to Customer"
          shipment.ship!
        end

        redirect_to show_admin_order_put_path(order)
      end

      def change_shirts_status line_item , shipment
        puts "************ IN change_status"
        inventory_unit = Spree::InventoryUnit.find_by(:line_item_id => line_item.id)
        puts "Inventory unit: " + inventory_unit.id.to_s + " Shipment id: " + inventory_unit.shipment_id.to_s
        previous_shipment = inventory_unit.shipment
        inventory_unit.shipment = shipment
        inventory_unit.save!
        puts "Inventory unit: " + inventory_unit.id.to_s + " Shipment id: " + inventory_unit.shipment_id.to_s
        previous_shipment.destroy if previous_shipment.inventory_units.count == 0

      end

    end
  end
end
