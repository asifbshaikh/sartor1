module Spree
  module Admin
    class DetailedProfilesController < Spree::Admin::ProfilesController

      def index
        @profiles = @user.profiles
        respond_with(@profiles)
      end

      def new
        # load default historic profile 
        if Profile.where(:user_id=> params[:user_id],:heir_type => 'DetailedProfile').length > 0
          @historic_profile = Profile.where(:user_id => params[:user_id], :heir_type => 'DetailedProfile').order("created_at DESC").first
        end
        @orderID = params[:id]
        @profile = DetailedProfile.new
        @profile.name = spree_current_user.first_name + spree_current_user.last_name
      end

      def create
        orderID = params[:order_id]

        if Profile.where(:user_id=> params[:user_id],:heir_type => 'DetailedProfile').length > 0
          @historic = Profile.where(:user_id => params[:user_id], :heir_type => 'DetailedProfile').order("created_at DESC").first
           if @historic.heir.update(detailed_profile_params) 
            flash[:success] = 'Measurement profile successfully updated.'
           end
        else
          @profile = DetailedProfile.new
          update_attributes(@profile)
          @profile.predecessor.user = @user
          @profile.name = spree_current_user.first_name + spree_current_user.last_name
          if @profile.save
            flash[:success] = 'Measurement profile successfully created.'
          end
        end


        if session[:urlorder]
              redirect_to session[:urlorder]
        elsif(orderID != nil)
          redirect_to edit_admin_order_url(@order, :id => orderID)
        else
          redirect_to spree.edit_polymorphic_url([:admin, @user])
        end


      end

      def show
        @profile = Profile.find(params[:id])
      end

      def edit
        # TODO handle not found case?
        @profile = Profile.find(params[:id])
        @profile = @profile.heir
      end

      def update
        profile = Profile.find(params[:id])
        detailed_profile= profile.heir
        if update_attributes(detailed_profile)
          #todo: I18n for resource name
          flash[:notice] = "Measurement profile successfully updated"
        end
        redirect_to spree.edit_polymorphic_url([:admin, @user])
      end

      def destroy
        @profile = Profile.find(params[:id])
        if @profile.destroy
           #todo: I18n for resource name
          flash[:notice] = "Measurement profile successfully removed"
          respond_with(@profile) do |format|
            format.html {  redirect_to spree.polymorphic_url([:admin, @user, model_class]) }
            format.js   { render :partial => "spree/admin/shared/destroy" }
          end
        else
          invoke_callbacks(:destroy, :fails)
          respond_with(@profile) do |format|
            format.html {  redirect_to spree.polymorphic_url([:admin, @user, model_class]) }
          end
        end
      end

      def update_attributes(profile)
        profile.update_attributes(detailed_profile_params)
      end

      def detailed_profile_params
        params.require(:detailed_profile).permit(:name, :embroidery_name, :height_ft, :height_in, :weight, :neck, :yoke, :sleeve, :chest, :waist, :hip, :length, :bicep, :wrist, :length_untucked, :wrist_watch, :watch_hand, :default_fit_code, :default_length_code)
      end
    end
  end
end
