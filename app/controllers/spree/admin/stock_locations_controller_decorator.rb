Spree::Admin::StockLocationsController.class_eval do

  def update_stocks_for_all_products
    products = Spree::Product.all
    products.each do |product|
      puts "******Processing Product: " + product.name.to_s
      product.add_quantity_stock_locations
    end
    redirect_to admin_stock_locations_path
  end

end
