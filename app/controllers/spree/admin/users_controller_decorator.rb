Spree::Admin::UsersController.class_eval do

  def invite
    authorize! :invite, @user, :message => 'Not authorized as an administrator.'
    @user = Spree::User.find(params[:id])
    @user.send_invite_instructions
    @user.invite_sent_at = DateTime.now
    @user.save(:validate => false)
    redirect_to :back, :only_path => true, :notice => "Sent invitation to #{@user.email}."
  end

  def new
    @orderID = params[:id]
    invoke_callbacks(:new_action, :before)
    respond_with(@object) do |format|
      format.html { render :layout => !request.xhr? }
      if request.xhr?
        format.js   { render :layout => false }
      end
    end
  end

  def show
    user_id = params[:id]
    @user = Spree::User.find_by(:id => user_id)
    puts "In SHOW"
  end

  def edit
    session[:urlorder] = nil
    @user_id = params[:id]
    @usual_shirt_configuration = UsualStyle.find_by(user_id: params[:id])
  end

  def update
    @user_id = params[:id]
    @usual_shirt_configuration = UsualStyle.find_by(user_id:  params[:id])

    if params[:user]
      roles = params[:user].delete("spree_role_ids")
    end

    if @user.update_attributes(user_params)
      if roles
        @user.spree_roles = roles.reject(&:blank?).collect{|r| Spree::Role.find(r)}
      end
      flash.now[:success] = Spree.t(:account_updated)
    end

    render :edit
  end


  def create
    orderID = params[:order_id]
    if params[:user]
      roles = params[:user].delete("spree_role_ids")
    end

    @user = Spree.user_class.new(user_params)
    if @user.save
      if roles
        @user.spree_roles = roles.reject(&:blank?).collect{|r| Spree::Role.find(r)}
      end
      if(orderID !=nil)
        flash[:success] = Spree.t(:created_successfully)
        redirect_to edit_admin_order_url(@order, :id => orderID)
      else
        flash.now[:success] = Spree.t(:created_successfully)
        render :edit
      end

    else
      if(orderID != nil)
        #TODO this needs improvements as it should be redirect to new_admin_user_path
        flash[:error] = Spree.t(:Something_went_wrong)
        redirect_to edit_admin_order_url(@order, :id => orderID)
      else
        render :new
      end
    end
  end


end
