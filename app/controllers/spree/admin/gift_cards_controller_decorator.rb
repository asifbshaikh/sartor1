Spree::Admin::GiftCardsController.class_eval do
  before_filter :load_gift_card, only: [:show]

  def index
  end

  def show
  end

  def redeem_gift_cards
    @id = params[:id]
  end

  def redeem_gift_cards_complete
    id = params[:user_id]
    redemption_code = params[:redemption_code]
    user = Spree::User.find_by(:id => id)

    gift_card = Spree::VirtualGiftCard.find_by(:redemption_code => redemption_code)
    if gift_card != nil
      if gift_card.redeem(user)
        flash[:success] = Spree.t("Gift Card redeemed successfully! Amount credited in user's store credit!")
        redirect_to admin_users_path
      else
        flash[:error] = Spree.t("This gift code is not valid anymore!")
        redirect_to admin_redeem_gift_cards_path(id)
      end
    else
      flash[:error] = Spree.t("Invalid code! Enter again")
      redirect_to admin_redeem_gift_cards_path(id)
    end

  end

  private

  def load_gift_card
    redemption_code = Spree::RedemptionCodeGenerator.format_redemption_code_for_lookup(params[:id])
    @gift_cards = Spree::VirtualGiftCard.where(:redemption_code => redemption_code)
    if @gift_cards.empty?
      @gift_cards = Spree::VirtualGiftCard.where(:purchaser_id => params[:id])
    end

    if @gift_cards.empty?
      flash[:error] = Spree.t('admin.gift_cards.errors.not_found')
      redirect_to(admin_virtual_gift_cards_path)
    end
  end
end
