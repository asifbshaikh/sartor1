module Spree
  module Admin
    class ShirtProfilesController < Spree::Admin::ProfilesController
      def index
        @profiles = @user.profiles
        respond_with(@profiles)
      end

      def new
        @profile = ShirtProfile.new
        @historic_profile = Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile').order("created_at DESC").first
        @selected_profile_id = @historic_profile.id
        @detailedProfiles = Profile.where(:user_id => params[:user_id], :heir_type => 'DetailedProfile')
      end

      def create
        @profile = ShirtProfile.new
        update_attributes(@profile)
        @profile.predecessor.user = @user
        @detailedProfiles = Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile')
        shirt_length = params[:lengths]
        @profile.name ||=  Profile.find(params[:detailedProfile]).name
        if @profile.save
          flash[:success] = 'Measurement profile successfully created.'
        end

        if session[:urlorder]
              redirect_to session[:urlorder]
        else
              redirect_to spree.edit_polymorphic_url([:admin, @user])
        end
      end

      def show
        @profile = Profile.find(params[:id])
      end

      def edit
        # TODO handle not found case?
        @profile = Profile.find(params[:id])
        @profile = @profile.heir
      end

      def update
        profile = Profile.find(params[:id])
        shirt_profile= profile.heir
        if (update_attributes(shirt_profile))
          #todo: I18n for resource name
          flash[:notice] = "Measurement profile successfully updated"
        end
        redirect_to spree.edit_polymorphic_url([:admin, @user])
      end

      def destroy
        @profile = Profile.find(params[:id])
        if @profile.destroy

           #todo: I18n for resource name
          flash[:notice] = "Measurement profile successfully removed"
          respond_with(@profile) do |format|
            format.html {  redirect_to spree.polymorphic_url([:admin, @user, model_class]) }
            format.js   { render :partial => "spree/admin/shared/destroy" }
          end
        else
          invoke_callbacks(:destroy, :fails)
          respond_with(@profile) do |format|
            format.html {  redirect_to spree.polymorphic_url([:admin, @user, model_class]) }
          end
        end
      end

      def update_attributes(profile)
        profile.update_attributes(shirt_profile_params)
      end

      def shirt_profile_params
        params.require(:shirt_profile).permit(:name, :embroidery_name, :height_ft, :height_in, :weight, :neck, :yoke, :sleeve, :chest, :waist, :hip, :length, :bicep, :wrist, :default_fit_code, :classification_code, :length_code, :waist_hips_code, :chest_sleeves_code)
      end

    end
  end
end
