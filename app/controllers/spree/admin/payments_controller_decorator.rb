Spree::Admin::PaymentsController.class_eval do
  def can_transition_to_payment
    if(@order.manual_order == false)
      unless @order.billing_address.present?
        flash[:notice] = Spree.t(:fill_in_customer_info)
        redirect_to edit_admin_order_customer_url(@order)
      end
    end
  end
end

