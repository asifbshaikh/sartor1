Spree::Admin::OrdersController.class_eval do

  def new
    @order = Spree::Order.create
    @order.created_by = try_spree_current_user
    @order.manual_order = true
    @order.save
    redirect_to edit_admin_order_url(@order)
  end

  def edit
    @id = params[:id]
    @url =  request.fullpath
    session[:urlorder] = @url
    country_id = Spree::Address.default.country.id
    @order.build_ship_address(:country_id => country_id) if @order.ship_address.nil?
    @order.ship_address.country_id = country_id if @order.ship_address.country.nil?
    unless @order.complete?
      @order.refresh_shipment_rates
    end
  end

  def show
    @stock_locations_for_select = Spree::StockLocation.active
  end

  def update
    if @order.update_attributes(order_params)
      if params[:guest_checkout] == "false"
        @order.associate_user!(Spree.user_class.find(params[:user_id]), @order.email.blank?)
      end
      while @order.next; end
      new_profile_id = get_profile_id;

      @order.update_attributes(:payment_known => payment_known)
      @order.update_attributes(:offline_order_date => Date.civil(offline_order_date[:year].to_i, offline_order_date[:month].to_i, offline_order_date[:day].to_i))
      if (new_profile_id != nil && new_profile_id != "")
        @order.update_attributes(:profile_id => new_profile_id)
        @line_item = Spree::LineItem.where(:order_id => @order.id)
        @line_item.each do |item|
          item.update(profile_id: new_profile_id)
        end
      end
      old_tax_amount = @order.tax_amount
      new_tax_amount = get_tax_amount

      if(new_tax_amount != nil and new_tax_amount != '')
        new_tax_amount = new_tax_amount.to_d
        #use tax amount instead of tax rate
        if(old_tax_amount != new_tax_amount)
          @order.update_attributes(:tax_amount => new_tax_amount)
          add_custom_tax_amount
        end
      end

      #This method is causing data loss for table spree_inventory_units.
      #So this is disabled for now until we figure out what's the reason.
      #@order.refresh_shipment_rates
      flash[:success] = Spree.t('customer_details_updated')
      redirect_to edit_admin_order_path(@order)
    else
      render :action => :edit
    end
  end

  def add_custom_tax_amount
    if(@order.tax_amount != nil)
       taxAdjustment = Spree::Adjustment.find_by(
          :source_id => @order.id,
          :adjustable_id => @order.id,
          :adjustable_type => "Spree::Order"
          )

        #If custom-tax adjustment already exists, delete it.
        if(taxAdjustment != nil)
          taxAdjustment.destroy
        end

        #apply new tax amount adjustment to order
        amount = @order.tax_amount

        #create custom-tax-amount adjustment
        Spree::Adjustment.create(
          amount: amount,
          order: @order,
          adjustable: @order,
          source: @order,
          label: "Custom tax amount"
        )
      end
      reload_totals
  end

 def reload_totals
    order_updater = Spree::OrderUpdater.new(@order)
    order_updater.update_item_count
    order_updater.update_item_total
    order_updater.update_adjustment_total
    order_updater.persist_totals
    @order.reload
  end

  private

  def order_params
    if (skip_shipping_address.to_i == 1)
      params.require(:order).permit(
      :email,
      :ship_address_attributes => permitted_address_attributes
     )
    else
      params.require(:order).permit(
      :email,
      #Manual orders will not have billing address as mentioned by Dan
      #:use_billing,
      #:bill_address_attributes => permitted_address_attributes,
      :ship_address_attributes => permitted_address_attributes
    )
    end
  end

  def get_profile_id
    params['profile_id']
  end

  def get_tax_amount
    params['tax_amount']
  end

  def skip_shipping_address
    params['skip_address']
  end

  def offline_order_date
    params['offline_order_date']
  end

  def payment_known
    if (params['payment_unknown'].to_i == 1)
      false
    else
      true
    end
  end
end
