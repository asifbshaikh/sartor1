Spree::Admin::GiftCardsController.class_eval do
    
  private
    
  def gift_card_params
    params[object_name].permit(:code, :email, :name, :note, :value, :variant_id)
  end
    
end