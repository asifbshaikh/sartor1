module Spree
  module Admin
    class CsvUploadersController < Spree::Admin::BaseController
      
      def show
        puts "Showing Page"
      end
      
      def notify
        puts "Notifying status"
      end
      
      def read_csv
        product_path = params[:products]
        image_path = params[:images]
        email = params[:email]
        temp_path = image_path
        csv_error = []; error_info = []; invalid_csv = []; product_update = []; product_error = []; image_error = []
        csv_parse = true
        if(product_path.to_s != "" && image_path.to_s !="")
                  path = product_path
                  csv_data = open(path)
                  begin
                    csv_rows = CSV.parse(csv_data.read)
                    total_column_in_header = csv_rows[0].length
                  rescue => error_message
                    csv_parse = false
                    flash[:error] = "CSV Error: " + error_message.to_s 
                  end  
                  nil_column_flag = false
         if(csv_parse)      
          Thread.new do
                  csv_rows.drop(1).each_with_index{ |csv_line, csv_line_index|
                          csv_clean_record_flag = true
                          csv_null_column_flag = true
                          if(csv_line.length != total_column_in_header )
                              csv_clean_record_flag = false
                              csv_line_number = csv_line_index + 2
                              invalid_csv = prepare_error_info("Error message: Incorrected CSV Format", csv_line_number.to_s, csv_line.to_s)
                              csv_error.push invalid_csv
                              invalid_csv = []; csv_line_number = 0
                          end  
                          if(csv_clean_record_flag)
                              csv_line.each_with_index { |csv_column, csv_column_index|
                                  if(csv_column == nil)
                                      csv_null_column_flag = false
                                      csv_line_number = csv_line_index + 2
                                      error_info = prepare_error_info("Missing Field: " + csv_rows[0][csv_column_index], csv_line_number.to_s, csv_line.to_s)
                                      csv_error.push error_info
                                      error_info = []; csv_line_number = 0
                                  end
                              }                      
                          end
                          
                        if(csv_null_column_flag && csv_clean_record_flag)
                             clean_record = true
                             existing_order_flag = false
                             name =  csv_line[0]
                             price = csv_line[1]
                             shipping_category = csv_line[2]
                             product_type = csv_line[3]
                             available_date = csv_line[4]
                             description = csv_line[5]
                             sku = csv_line[6]  
                             begin
                                product, variant_id = find_product_by_sku_name sku
                                if product
                                  product_id  = product.id
                                  Product.where(id: product.id).update_all({:name => name, :shipping_category_id => shipping_category, :product_type => product_type, :available_on => available_date, :description => description}) 
                                  Price.where(variant_id: variant_id).update_all({:amount => price})
                                  existing_order_flag = true
                                end  
                             rescue => error_message
                                 line_number =csv_line_index + 2
                                 product_update = prepare_error_info("For a product: " + error_message.to_s, line_number.to_s, csv_line.to_s)
                                 csv_error.push product_update
                                 product_update = []; line_number = 0
                                 existing_order_flag = false 
                             end
                             if(!existing_order_flag)
                                 begin 
                                      object = Spree::Product.new(name: name, price: price, shipping_category_id: shipping_category, product_type: product_type, available_on: available_date, description: description, sku: sku) 
                                      object.save!
                                      product_id = object.id
                                   rescue ActiveRecord::RecordInvalid => error_message
                                      line_number = csv_line_index + 2
                                      product_error = prepare_error_info("For a product: " + error_message.to_s, line_number.to_s, csv_line.to_s)
                                      csv_error.push product_error
                                      product_error = []; line_number = 0
                                      clean_record = false
                                 end
                             end
                             if(clean_record)
                                 begin
                                     image_path = temp_path + '/' + name + '.jpg'
                                     add_image_to_product(product_id, image_path)
                                     
                                 rescue => error_message
                                     line_number =csv_line_index + 2
                                     image_error = prepare_error_info("For a image: " + error_message.to_s, line_number.to_s, csv_line.to_s)
                                     csv_error.push image_error
                                     image_error = []; line_number = 0                  
                                 end                          
                             end                                       
                          end 
                     }   
                     send_notification(csv_error, email, product_path, temp_path)      
              end #end of thread
             end #end of csv parse flag 
           end  #end of if(csv path)  
          if(csv_parse) 
            redirect_to admin_import_notify_path
          else
            redirect_to admin_csv_show_path          
          end  
      end
      
      def find_variants(product_id)
          @variants = Variant.find_by(product_id: product_id)
          @variants.id
      end
      
      def find_product_by_sku_name(sku)
          variant = Variant.find_by(sku: sku)
          if variant
            product = Product.find_by(id: variant.product_id)
            return product, variant.id
          else
            return false,false 
          end   
      end
      
      def add_image_to_product(product_id,image_path)
         viewable_id = find_variants(product_id)
         image_instance = Image.new
         image_instance.viewable_id = viewable_id
         image_instance.image_type = "Main"
         image_instance.viewable_type = "Spree::Variant"
         encoded_url = URI.encode(image_path)
         image_instance.attachment = URI.parse(encoded_url)
         image_instance.save!                       
      end
      
      def send_notification(csv_error, email, product_path, image_path)
          submitted_at = Time.now
          csv_mailer_object = Spree::CsvMailerController.new
          csv_mailer_object.send_mail(csv_error, email, product_path, image_path, submitted_at)
      end  

      def prepare_error_info(message, line_number, record)
          temp = []
          temp.push message
          temp.push line_number
          temp.push record
          temp 
      end
     
     
    end
  end
end    