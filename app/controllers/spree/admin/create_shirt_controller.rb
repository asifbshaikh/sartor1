module Spree
  module Admin
    class CreateShirtController < Spree::Admin::BaseController
      def create_my_shirt
        shirt_length = params[:length]
        ease =Hash.new
        generated_shirt = Hash.new

        #Measurement style selected
        classification_id = params[:classification]
        chest_sleeves_id = params[:chest_sleeves]
        waist_hips_id = params[:waist_hips]
        length_id = params[:length]
        profile_id = params[:profile]

        #Selected Body Measurement Profile
        @detailedProfiles = Profile.where(:user_id => spree_current_user, :heir_type => 'DetailedProfile')
        @selected_profile = Profile.find(profile_id)
        @detailedProfile = @selected_profile.heir

        #Fixed allowances
        generated_shirt['neck']   = @detailedProfile.shirt_collar.to_f
        generated_shirt['yoke']   = @detailedProfile.shirt_shoulder.to_f
        generated_shirt['wrist']  = @detailedProfile.shirt_cuff.to_f
        generated_shirt['length'] = @detailedProfile.shirt_length.to_f
        ease['neck']  = (generated_shirt['neck']- @detailedProfile.neck).to_f
        ease['yoke']  = (generated_shirt['yoke'] - @detailedProfile.yoke).to_f
        ease['wrist'] = (generated_shirt['wrist'] - @detailedProfile.wrist).to_f
        ease['length']= (generated_shirt['length'] - @detailedProfile.length).to_f

        #WaistHips
        @detailedProfile.Generator_Fit = Profile::WAIST_HIPS_CODE[waist_hips_id.to_i]
        generated_shirt['waist'] = @detailedProfile.shirt_waist.to_f
        generated_shirt['hip']   = @detailedProfile.shirt_hip.to_f
        ease['waist'] = (generated_shirt['waist'] - @detailedProfile.waist).to_f
        ease['hip']   = (generated_shirt['hip'] - @detailedProfile.hip).to_f

        #ChestSleeves
        @detailedProfile.Generator_Fit = Profile::CHEST_SLEEVES_CODE[chest_sleeves_id.to_i]
        generated_shirt['sleeve'] = @detailedProfile.shirt_sleeve.to_f
        generated_shirt['chest']  = @detailedProfile.shirt_chest.to_f
        generated_shirt['bicep']  = @detailedProfile.shirt_bicep.to_f
        ease['sleeve']= (generated_shirt['sleeve'] - @detailedProfile.sleeve).to_f
        ease['chest'] = (generated_shirt['chest'] - @detailedProfile.chest).to_f
        ease['bicep'] = (generated_shirt['bicep'] - @detailedProfile.bicep).to_f

        #Response
        respond_to do |format|
          msg = { :status => "ok", :message => "Success!" ,:shirt_profile => @detailedProfile ,:ease => ease ,:generated_shirt  => generated_shirt}
          format.json  { render :json => msg }
        end
      end
    end
  end
end
