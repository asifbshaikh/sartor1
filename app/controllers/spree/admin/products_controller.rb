module Spree
  module Admin
    class ProductsController < ResourceController
      helper 'spree/products'

      before_filter :load_data, :except => :index
      create.before :create_before
      update.before :update_before
      helper_method :clone_object_url

      def show
        session[:return_to] ||= request.referer
        redirect_to( :action => :edit )
      end

      def index
        session[:return_to] = request.url
        respond_with(@collection)
      end

      def edit
            @current_product = Spree::Product.where(slug: params[:id])
            @product_buttons = ProductsButtons.where(product_id: @current_product[0].id)
            if @product_buttons
              @button_ids = Array.new
                @product_buttons.each do |button|
                @button_ids.push(button.button_id.to_s)
                end
            end
      end

      def update
        if params[:buttons].present?
           if params[:buttons][:button_ids].present?
              params[:buttons][:button_ids].push(params[:product][:shirt_configuration_attributes][:button_code])
           end
        end
        if params[:buttons].present?
          if params[:buttons][:button_ids].present?
            ProductsButtons.where(product_id: @object.id).delete_all
            params[:buttons][:button_ids].each do |button|
              ProductsButtons.create(product_id: @object.id,button_id: button)
            end
          end
        else
          ProductsButtons.where(product_id: @object.id).delete_all
          ProductsButtons.create(product_id: @object.id,button_id: params[:product][:shirt_configuration_attributes][:button_code])
        end

        if params[:product][:is_available].present? and params[:product][:is_available] == "1"
          params[:product][:is_available] = true
        else
          params[:product][:is_available] = false
        end
        if params[:product][:taxon_ids].present?
          params[:product][:taxon_ids] = params[:product][:taxon_ids].split(',')
        end
        if params[:product][:option_type_ids].present?
          params[:product][:option_type_ids] = params[:product][:option_type_ids].split(',')
        end
        invoke_callbacks(:update, :before)

        if @object.update_attributes(permitted_resource_params)
          invoke_callbacks(:update, :after)
          flash[:success] = flash_message_for(@object, :successfully_updated)
          respond_with(@object) do |format|
            format.html { redirect_to location_after_save }
            format.js   { render :layout => false }
        end
        else
          # Stops people submitting blank slugs, causing errors when they try to update the product again
          @product.slug = @product.slug_was if @product.slug.blank?
          invoke_callbacks(:update, :fails)
          respond_with(@object)
        end
      end

      def destroy
        @product = Product.friendly.find(params[:id])
        @product.destroy

        flash[:success] = Spree.t('notice_messages.product_deleted')

        respond_with(@product) do |format|
          format.html { redirect_to collection_url }
          format.js  { render_js_for_destroy }
        end
      end

      def clone
        @new = @product.duplicate

        if @new.save
          flash[:success] = Spree.t('notice_messages.product_cloned')
        else
          flash[:error] = Spree.t('notice_messages.product_not_cloned')
        end

        redirect_to edit_admin_product_url(@new)
      end

      def stock
        @variants = @product.variants
        @variants = [@product.master] if @variants.empty?
        @stock_locations = StockLocation.accessible_by(current_ability, :read)
        if @stock_locations.empty?
          flash[:error] = Spree.t(:stock_management_requires_a_stock_location)
          redirect_to admin_stock_locations_path
        end
      end

      protected

        def find_resource
          Product.with_deleted.friendly.find(params[:id])
        end

        def location_after_save
          spree.edit_admin_product_url(@product)
        end

        def load_data
          @taxons = Taxon.order(:name)
          @option_types = OptionType.order(:name)
          @tax_categories = TaxCategory.order(:name)
          @shipping_categories = ShippingCategory.order(:name)
        end

        def collection
        return @collection if @collection.present?
          @index_products_checked = false
          params[:q] ||= {}
          params[:q][:deleted_at_null] ||= "1"
          params[:q][:s] ||= "name asc"
          @collection = super
          if params[:q][:is_index_product] != nil
            if params[:q][:is_index_product] == "1"
              @collection = @collection.where(:is_index_product => "1")
              @index_products_checked = true
            end
            params[:q].delete :is_index_product
          end
          @collection = @collection.with_deleted if params[:q][:deleted_at_null] == '0'
          # @search needs to be defined as this is passed to search_form_for
          @search = @collection.ransack(params[:q])
          @collection = @search.result.
          distinct_by_product_ids(params[:q][:s]).
          includes(product_includes).
          page(params[:page]).
          per(Spree::Config[:admin_products_per_page])
          @collection
        end

        def create_before
          return if params[:product][:prototype_id].blank?
          @prototype = Spree::Prototype.find(params[:product][:prototype_id])
        end

        def update_before
          # note: we only reset the product properties if we're receiving a post from the form on that tab
          return unless params[:clear_product_properties]
          params[:product] ||= {}
        end

        def product_includes
          [{ :variants => [:images, { :option_values => :option_type }], :master => [:images, :default_price]}]
        end

        def clone_object_url resource
          clone_admin_product_url resource
        end

        def permit_attributes
          params.require(:product).permit!
        end
    end
  end
end
