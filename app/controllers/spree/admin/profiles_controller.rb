module Spree
  module Admin
    class ProfilesController < Spree::Admin::BaseController
       before_filter :load_user

      respond_to :html

      # TODO - do we need security/auth for these actions?

      def new_profile
        # descendents must override
      end

      def profile_params
        # todo unused - remove?
        # descendents must override
      end

      def new
        @profile = new_profile
        @profile.name ||= "My Measurements"
        @profile.embroidery_name = "#{@user.first_name} #{@user.last_name}"
      end

      def index
      end

      def edit
        # TODO handle not found case?
        @profile = Profile.find(params[:id])
        @profile = @profile.heir

        if @profile.is_a? DetailedProfile
          render "spree/admin/detailed_profiles/edit"
        else
          render "spree/admin/shirt_profiles/edit"
        end
      end

      def destroy
        @profile = Profile.find(params[:id])
        if @profile.destroy
          #todo: I18n for resource name
          flash[:notice] = I18n.t(:successfully_removed, :resource => "Measurement profile")
          respond_with(@profile) do |format|
            format.html { redirect_to spree.polymorphic_url([:admin, @user, model_class]) }
            format.js { render :partial => "spree/admin/shared/destroy" }
          end
        else
          invoke_callbacks(:destroy, :fails)
          respond_with(@profile) do |format|
            format.html { redirect_to spree.polymorphic_url([:admin, @user, model_class]) }
          end
        end
      end

      private

      def load_user
        userId = params[:cust_id]
        if(userId != nil)
          @user = User.find(userId)
          authorize! action, @user
        else
          userId = params[:user_id]
          @user = User.find(userId)
          authorize! action, @user
        end
      end
      protected

      def model_class
        const_name = controller_name.classify
        if Spree.const_defined?(const_name)
          return const_name.constantize
        end
        nil
      end

    end
  end
end
