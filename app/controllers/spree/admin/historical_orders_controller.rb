module Spree
  module Admin
    class HistoricalOrdersController < Spree::Admin::BaseController

        def show
        end
        
        def import_historical_orders
            csv_path = params[:orders]
            email = params[:email]
            #Csv Read
            csv_parse_error = false
            if(csv_path != "")
                  #Log File
                  begin
                      csv_data = open(csv_path)
                      csv_rows = CSV.parse(csv_data.read)
                      total_column_in_header = csv_rows[0].length
                  rescue => parse_error
                      csv_parse_error = true
                  end    
                  puts "Header Length: " + total_column_in_header.to_s
                  clean_csv = []
                  csv_error = []
                   if(!csv_parse_error) 
                       Thread.new do    
                              csv_rows.drop(1).each_with_index{ |csv_line, csv_line_index|
                                    csv_clean_record_flag = true
                                    csv_null_column_flag = true
                                    if(csv_line.length != total_column_in_header )
                                        csv_clean_record_flag = false
                                        csv_line_number = csv_line_index + 2
                                        error_info = "Error message: Incorrected CSV Format at line number: " + csv_line_number.to_s + " for a record: " + csv_line.to_s
                                        csv_error.push error_info
                                        error_info = nil
                                        csv_line_number = 0
                                    end  
                                    if(csv_clean_record_flag)
                                        csv_line.each_with_index { |csv_column, csv_column_index|
                                            if(csv_column == nil)
                                                csv_null_column_flag = false
                                                csv_line_number = csv_line_index + 2
                                                error_info = "Missing Field: " + csv_rows[0][csv_column_index] + " at line Number: " + csv_line_number.to_s + " for a record: " + csv_line.to_s 
                                                csv_error.push error_info
                                                csv_line_number = 0
                                            end
                                        }                      
                                    end
                                    
                                    if(csv_null_column_flag && csv_clean_record_flag)
                                        clean_csv.push csv_line                                         
                                    end 
                               }
                              #Finished Validation
                              same_order = []
                              clean_csv.sort!.each_with_index { |csv_line, csv_line_index|
                                  current_order = clean_csv[csv_line_index]
                                  current_order_id = current_order[0]
                                  next_index = csv_line_index + 1
                                  next_order = clean_csv[next_index]
                                  next_index = 0
                                  last_row = false
                                  if(next_order != nil)
                                      next_order_id = next_order[0]
                                  else
                                    next_order_id = current_order_id
                                    last_row = true   
                                  end
                                  
                                  if(current_order_id == next_order_id)
                                       same_order.push csv_line
                                       if(last_row)
                                        error_message = import_order same_order
                                            if(!error_message.empty?)
                                                csv_error.push error_message
                                            end  
                                       end 
                                  end
                                  
                                  if(current_order_id != next_order_id)
                                    same_order.push csv_line
                                    error_messages = import_order same_order
                                        if(!error_messages.empty?)
                                          csv_error.push error_messages
                                        end
                                    same_order.clear
                                  end
                              }
                        
                          csv_error = csv_error.flatten
                          send_notification(csv_error, email, csv_path)
                      end #End of Thread
                   end  #end of if(!csv_parse)  
               end # end of if(!csv_parse_error) 
              if(!csv_parse_error)  
                flash[:notice] = "You Will receive notification email on " + email.to_s
              else
                flash[:error] = "CSV Parse Error: " + parse_error.to_s  
              end   
              redirect_to admin_historical_orders_show_path
        end
        
        def find_user(email)
          @user = User.find_by(email: email)
          @user.id  
        end
        
        def find_line_item(id)
          line_item = LineItem.find_by(id: id)
        end  
        
        def find_variants(sku)
            @variants = Variant.find_by(sku: sku)
            @variants.id
        end
        
        def find_price(variant_id)
            @price = Price.find_by(variant_id: variant_id)
            @price.amount
        end
        
        def order_updater(order)
            @updater = OrderUpdater.new(order)
        end
  
        def reload_totals(order)
            order_updater(order).update_item_count
            order_updater(order).update_item_total
            order_updater(order).update_adjustment_total
            order_updater(order).persist_totals
            order.reload
        end
        
        def find_total_profiles(user_id)
            Profile.where(user_id: user_id).count
        end
        
        def find_profile(user_id)
            profile = Profile.find_by(user_id: user_id)
            profile.id  
        end
        
        def create_order(email, user_id)
              imported_at = Time.now
              payment_known = 'f'
              manual_order = 't'
              state = "complete"
              payment_state = "paid"
              shipment_state = "shipped"
              object = Spree::Order.new(email: email, user_id: user_id, imported_at: imported_at, payment_known: payment_known, manual_order: manual_order, state: state, payment_state: payment_state, shipment_state: shipment_state)                                  
              object.save!
              order_id = object.id
              return order_id, object           
        end
        
        def create_line_item(variant_id, order_id, note)
                quantity = 1
                amount = find_price(variant_id)
                line_item = LineItem.new(quantity: quantity, variant_id: variant_id, order_id: order_id, price: amount, note: note)
                line_item.save!
                line_item_id = line_item.id
                return line_item_id, line_item
        end
        
        def create_shipment(order_id)
            state = "shipped"
            stock_location_id = find_shiped_stock_location 
            shipment = Shipment.new(order_id: order_id, state: state, stock_location_id: stock_location_id)
            shipment.save!
            shipment_id = shipment.id
        end
        
        def find_shiped_stock_location
            stock_location = StockLocation.find_by(name: "Ship to Customer")
            stock_location.id       
        end
        
        def create_inventory(variant_id, order_id, line_item_id, shipment_id)
            state = "shipped"
            inventory = InventoryUnit.new(variant_id: variant_id, order_id: order_id, line_item_id: line_item_id, shipment_id: shipment_id, state: state)
            inventory.save!
        end
        
        def import_order(same_order)
            first_row = same_order[0]
            email = first_row[2]
            found_user = true
            temp = []
            begin
              user_id = find_user email
              order_id,current_order = create_order(email,user_id)
              shipment_id = create_shipment order_id
            rescue => error_message
              found_user = false
              error_info = "Error Message: " + error_message.to_s + " (Customer not found) For a record: " + first_row.to_s
              temp.push error_info
              error_info = nil
            end
            if(found_user)
                same_order.each{ |row |
                    sku = row[3]
                    begin
                        variant_id = find_variants sku
                    rescue => error_message
                        error_info = "Error Message: " + error_message.to_s + " (Variant Not Found) For a record: " + first_row.to_s
                        temp.push error_info
                        error_info = nil
                        delete_order order_id
                        break;
                    end  
                    note = row[9]
                    begin
                        line_item_id,line_item = create_line_item(variant_id, order_id, note)
                    rescue => error_message
                        error_info = "Error Message: " + error_message.to_s + " (Unable to create line items) For a record: " + first_row.to_s
                        temp.push error_info
                        error_info = nil
                        delete_order order_id
                        break;
                    end
                    item_shirt_configuration = ShirtConfiguration.new(
                        :cuff_code => row[4],
                        :pocket_code => row[5],
                        :fit_code => row[6],
                        :length_code => row[7],
                        :collar_code => row[8]
                    )
                    line_item.shirt_configuration = item_shirt_configuration
                    create_inventory(variant_id,order_id,line_item_id,shipment_id)
                    reload_totals current_order
             }
            end
            if(current_order)
                update_completed_at current_order
            end
            temp     
        end
        
        def delete_order(order_id)
            order =  Order.find_by(id: order_id)
            order.destroy!
        end
        
      def update_completed_at(current_order)
          current_order.completed_at = Time.now
          current_order.save!
      end
      
      def send_notification(csv_error, email, order_path)
          submitted_at = Time.now
          csv_mailer_object = Spree::CsvMailerController.new
          csv_mailer_object.send_mail_orders(csv_error, email, order_path, submitted_at)            
      end

    end
  end
end  