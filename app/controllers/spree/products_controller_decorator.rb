Spree::ProductsController.class_eval do
  respond_to :js, only: [:show, :show_no_tie]
  alias super_show show

  def shirtConfigSession

    #keeping shirt config in session to keep changes
     session[:collar] = params[:configurations][:collar_code]
     session[:cuff] = params[:configurations][:cuff_code]
     session[:pocket] = params[:configurations][:pocket_code]
     session[:button] = params[:configurations][:button_code]
     session[:placket] = params[:configurations][:placket_code]
     #Login and go to Measurement Profiles
     if params[:configurations][:redirect_to]
        session[:return_to] = params[:configurations][:redirect_to]
     end
     respond_to do |format|
                msg = { :status => "ok", :message => "Success!"}
                format.json  { render :json => msg }
     end
  end

  def shirtresponse
  
      #initializing product
      @product=Spree::Product.find_by(id: params[:product_id]);
      @style = params[:config_tab]
      user = spree_current_user
      @taxon_buttons = Spree::Taxon.order(:name).where("permalink LIKE ?","button/%")
      @taxon_buttons =  @taxon_buttons.where("depth > ?", 1)
      @default_shirt_configuration = @product.shirt_configuration.dup
      #Set default values for shirt_config
      @default_shirt_configuration.collar_code = @default_shirt_configuration.collar_code == nil ? @default_shirt_configuration.collar_code = 1 : @default_shirt_configuration.collar_code
      @default_shirt_configuration.cuff_code = @default_shirt_configuration.cuff_code == nil ? @default_shirt_configuration.cuff_code = 1 : @default_shirt_configuration.cuff_code
      @default_shirt_configuration.pocket_code =@default_shirt_configuration.pocket_code == nil ? @default_shirt_configuration.pocket_code = 1 : @default_shirt_configuration.pocket_code
      if @taxon_buttons.length > 0
        @default_shirt_configuration.button_code =  @default_shirt_configuration.button_code > 3 ? @default_shirt_configuration.button_code : @taxon_buttons.first.id
      end
      @default_shirt_configuration.placket_code = @default_shirt_configuration.placket_code == nil ? @default_shirt_configuration.placket_code = 1 : @default_shirt_configuration.placket_code
      #USUAL STYLE
      if @style == "usual_style"
          #Your usual configurations
          @usual_shirt_configuration = UsualStyle.find_by(user_id: user.id)
          if @usual_shirt_configuration.button_code and Spree::Taxon.order(:name).where("permalink LIKE ?", "button/%").first
            @usual_shirt_configuration.button_code =  @usual_shirt_configuration.button_code > 3 ? @usual_shirt_configuration.button_code : Spree::Taxon.order(:name).where("permalink LIKE ?", "button/%").first.id
          end          
          if (@usual_shirt_configuration != nil)
              @usual_profile = Profile.find_by('id'=>@usual_shirt_configuration.profile_id);
          else
              @usual_shirt_configuration = @default_shirt_configuration
          end
          #response
          msg = { :status => "ok", :message => "Success!", :usual_shirt_configuration => @usual_shirt_configuration}
      #SARTOR STYLE
      elsif @style == "sartor_style"
          #response
          msg = { :status => "ok", :message => "Success!", :default_shirt_configuration => @default_shirt_configuration}
      #CUSTOMIZED STYLE
      elsif @style == "customized_style"
          #Shirt config in session
          if session[:collar]
              @default_shirt_configuration.collar_code = session[:collar]
          end
          if session[:cuff]
              @default_shirt_configuration.cuff_code = session[:cuff]
          end
          if session[:pocket]
              @default_shirt_configuration.pocket_code = session[:pocket]
          end
          if session[:button]
             @default_shirt_configuration.button_code = session[:button]
          end
          if session[:placket]
              @default_shirt_configuration.placket_code = session[:placket]
          end
          #response
          msg = { :status => "ok", :message => "Success!", :default_shirt_configuration =>  @default_shirt_configuration}
      #LINE ITEM EDIT STYLE
      elsif @style == "lineitem_style"
          #Change Details
          $current_line_item_id = params[:pid]
          line_item = Spree::LineItem.find(params[:pid])
          if line_item and line_item.shirt_configuration
            @line_item_notes = line_item.shirt_configuration.comment
          end
          @updateCartFlag = true;
          @shirt_config = ShirtConfiguration.find_by(line_item_id: $current_line_item_id)

          #response
          msg = { :status => "ok", :message => "Success!", :default_shirt_configuration =>  @shirt_config}
      elsif @style == "wishlist_style"
          #Change Details
          
         if params[:wished_product_id]
            line_item = Spree::WishedProduct.find(params[:wished_product_id].to_i)
            if line_item and line_item.shirt_configuration
              @line_item_notes = line_item.shirt_configuration.comment
            end
            @updateCartFlag = true;
            @shirt_config = line_item.shirt_configuration;
          end

          #response
          msg = { :status => "ok", :message => "Success!", :default_shirt_configuration =>  @shirt_config}
      end
      respond_to do |format|
          format.json  { render :json => msg }
      end
  end

  def show
    @button_ids = Array.new
    @product_buttons = ProductsButtons.where(product_id: @product.id)
    @taxon_buttons = Spree::Taxon.order(:name).where("permalink LIKE ?","button/%")
    @taxon_buttons =  @taxon_buttons.where("depth > ?", 1)
    if @product_buttons.length > 0
      @product_buttons.each do |button|
        @button_ids.push(button.button_id.to_s)
      end
    else
      if @taxon_buttons and @taxon_buttons.first
          @button_ids.push(@taxon_buttons.first.id.to_s)
      end
    end
    session[:return_to] = request.fullpath
    #Profile id while updating in change details
    if(params[:shirtprofile_id] != nil && params[:shirtprofile_id] != " ")
        @shirtprofileid = params[:shirtprofile_id]
    else
        @shirtprofileid = 0
    end
    @shirtsSelected = "selected"
    @updateCartFlag = false
    @UpdateWishListFlag=false
    super_show
    @shirt_profiles = Profile.where(:user_id => spree_current_user, :heir_type => 'ShirtProfile')
    if (@product.product_type == 'SHIRT')
      slug = params[:id]
      wished_products_id=params[:wished_products_id]
      $current_product_id = slug
      selected_product = Spree::Product.find_by(slug: slug)
      @product_shirt_images = @product.images.where.not(image_type: 'Main')
      if(spree_current_user)
          user = spree_current_user
          @usual_shirt_configuration = UsualStyle.find_by(user_id: user.id)
          if (@usual_shirt_configuration != nil)
              @usual_profile = Profile.find_by('id'=>@usual_shirt_configuration.profile_id);
          else
              @usual_shirt_configuration = @product.shirt_configuration.dup
          end
          @userShirtProfiles = spree_current_user.profiles.where(heir_type: "ShirtProfile");
          if(@userShirtProfiles.length > 0)
              @default_shirt_profile = @userShirtProfiles[0]
          end
      end
    end
    $current_line_item_id = params[:pid]
    $is_save_as_usual = params[:usual]
    if($current_line_item_id != nil)
      @updateCartFlag = true;
    end

    @wishlist_update = params[:wished_products_id]
    if @wishlist_update
      @UpdateWishListFlag=true
    end
  end

  def show_no_tie
  end

  # Default to shirts only
  def index
    @products = Spree::Product.active.is_shirt.is_available
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    render 'index'
  end

  def index_shirts
    session[:return_to] = request.fullpath
    @products = Spree::Product.active.is_shirt.is_available
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    @shirtsSelected = "selected"
    taxon_id = Spree::Taxon.find_by(permalink: "all-shirts")
    new_arrival_taxon_id = Spree::Taxon.find_by(permalink: "new-arrivals")
    if(new_arrival_taxon_id !=nil)
      @new_arrivals = Spree::Taxon.where(taxonomy_id: new_arrival_taxon_id.taxonomy_id)
    end
    if taxon_id != nil
      taxon_banner_image(taxon_id)
    end

    render 'index'
  end

  def index_ties
    @products = Spree::Product.active.is_tie.is_available
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    @tiesSelected = "selected"
    render 'index'
  end


  def shirts_collections
    collection = params[:collection]
    puts "************Collection : " + collection.to_s

    if collection == "All shirts"
      taxon_id = Spree::Taxon.find_by(permalink: "all-shirts")
      @products = Spree::Product.active.is_shirt.is_available
      if taxon_id != nil
        taxon_banner_image(taxon_id)
      end
    else
      taxon_id = Spree::Taxon.find_by(permalink: collection)
      if taxon_id != nil
        final_array = Spree::Classification.where(taxon_id: taxon_id).pluck(:product_id)
        @products = products = Spree::Product.active.is_shirt.is_available.where(id: final_array)
        taxon_banner_image(taxon_id)
      else
        @products = [];
      end
    end

    if @products == nil || @products.empty?
      #@products = Spree::Product.active.is_shirt
    end
    respond_to do |format|
      format.js
    end
  end

def taxon_banner_image(taxon_id)
      @taxon = Spree::Taxon.find(taxon_id)
      #returns paperclip url with amazon s3 support
      @taxon_url = @taxon.icon.url(:banner)
      @taxon_id = @taxon.id
      @taxon_icon_name = @taxon.icon_file_name
  end


  def filtered_shirts
    product_ids, at_least_one_category_selected = handle_taxons
    final_array = get_intersection_product_id_arrays product_ids
    products = Spree::Product.active.is_shirt.is_available.where(id: final_array)
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    if at_least_one_category_selected

      @products = products
    else
      @products = Spree::Product.active.is_shirt.is_available
    end
    respond_to do |format|
      format.js
    end
  end

  def handle_taxons
    check_box_map = params[:checkboxMap]
    product_ids_2d = []
    at_least_one_category_selected = false
    if check_box_map == nil
      return product_ids_2d, at_least_one_category_selected
    end

    all_taxonomies = Spree::Taxonomy.includes(root: :children)
    all_taxonomies.each do |taxonomy|
      parent_taxon = Spree::Taxon.find_by(permalink: taxonomy.permalink)
      taxon_filter_map = get_values_from_checkboxes parent_taxon, check_box_map
      valid_taxons, filter_category_selected = get_selected_taxons taxon_filter_map
      product_ids = Spree::Classification.where(taxon_id: valid_taxons).pluck(:product_id)
      if filter_category_selected
        at_least_one_category_selected = true
        product_ids_2d.push product_ids
      end
    end
    return product_ids_2d, at_least_one_category_selected
  end

  def get_values_from_checkboxes(parent_taxon, check_box_map)
    taxons = Spree::Taxon.where(parent_id: parent_taxon.id)
    taxon_filter_map = {}
    taxons.each do |taxon|
      checkbox_name = parent_taxon.name.tr(' ', '_') + "_" + taxon.id.to_s
      if check_box_map[checkbox_name] == 'true'
        taxon_filter_map[taxon.name] = 'on'
      else
        taxon_filter_map[taxon.name] = ''
      end
    end
    taxon_filter_map
  end

  def get_selected_taxons(taxon_filter_map)
    filter_category_selected = false
    valid_taxons = []
    taxon_filter_map.each do |taxon_name, value|
      if value == 'on'
        filter_category_selected = true
        valid_taxon = Spree::Taxon.find_by(name: taxon_name)
        valid_taxons.push valid_taxon.id
      end
    end
    return valid_taxons, filter_category_selected
  end

  def get_intersection_product_id_arrays(product_ids)
    final_array = []
    is_first_array = true
    product_ids.each do |product_id_array|
      if is_first_array == true
        final_array = product_id_array
        is_first_array = false
      else  if final_array != 0
              final_array &= product_id_array
            else
              break
            end
      end
    end
    final_array
  end
end
