class Spree::VirtualGiftCardsController < Spree::StoreController

  def new
    session[:return_to] = request.fullpath
    find_gift_card_variants
    @ties = Spree::Product.active.is_tie
    country = Spree::Country.find_by(:iso_name => "UNITED STATES")
    @address = Spree::Address.new(:country_id => country.id)
    @virtualGiftSelected = "selected"
    @pid = params[:pid]
    if @pid == nil || @pid == " "
      @updateflag=false
    else
      @updateflag=true
      @virtual_card = VirtualGiftCardsDetails.find_by(:line_item_id => params[:pid])
      @first_name = @virtual_card.recipient_name.split(' ')[0]
      @last_name = @virtual_card.recipient_name.split(' ')[1]
      if @virtual_card != nil
        if @virtual_card.card_type === 'virtual'
          @virtual_card.ship_address.address1 = ''
          @virtual_card.ship_address.city = ''
          @virtual_card.ship_address.zipcode = ''
          @virtual_card.ship_address.phone = ''
        end
      end

    end
  end

  def create
    address = add_address
    gift_card_details_id = nil
    variant_id = params[:variant][:id]
    tie_variant_id = params[:tie_variant_id]
    variant = Spree::Variant.find_by(:id => variant_id)
    type = params[:gift_card_type]

      # if(!spree_current_user)
      #     redirect_to("/login", :notice => Spree.t('Please Sign-in to add Gift card!'));
      #     return
      # end
      if spree_current_user and variant.product.gift_card
        gift_card_details = VirtualGiftCardsDetails.new(
          :recipient_name => params[:options_name].to_s + " " + params[:giftcard_address_lastname].to_s,
          :recipient_mail => spree_current_user.email,
          :amount => variant.price,
          :email_message => params[:options_note],
          :purchaser_id => spree_current_user.id,
          :card_type => params[:gift_card_type],
          :ship_address_id => address.id
        )
        gift_card_details.save!
        gift_card_details_id = gift_card_details.id
      elsif variant.product.gift_card
        gift_card_details = VirtualGiftCardsDetails.new(
          :recipient_name => params[:options_name].to_s + " " + params[:giftcard_address_lastname].to_s,
          # :recipient_mail => spree_current_user.email,
          :amount => variant.price,
          :email_message => params[:options_note],
          # :purchaser_id => spree_current_user.id,
          :card_type => params[:gift_card_type],
          :ship_address_id => address.id
        )
        gift_card_details.save!
        gift_card_details_id = gift_card_details.id
      end
        

      begin
        # Wrap the transaction script in a transaction so it is an atomic operation
        order = current_order(create_order_if_necessary: true)
        # Create line item
        line_item = Spree::LineItem.new(quantity: 1)
        line_item.variant = variant
        line_item.price = variant.price
        # Create sub line item
        tie_variant_id = tie_variant_id
        unless tie_variant_id.nil? || tie_variant_id == 0
          tie_variant = Spree::Variant.find(tie_variant_id)
          sub_line_item = Spree::LineItem.new(quantity: 1)
          line_item.sub_lineitems << sub_line_item
          sub_line_item.parent_lineitem = line_item
          sub_line_item.variant = tie_variant
          sub_line_item.price = tie_variant.price
          line_item.sub_lineitems.first.order = order
        end

      # Add to order
      order.line_items << line_item
      line_item.order = order
      order.item_count = order.item_count + 1
      order.update_totals
      order.save!

      gift_card_details = VirtualGiftCardsDetails.find_by(:id => gift_card_details_id)
      gift_card_details.line_item_id = line_item.id
      gift_card_details.save!

      # Remove Previous LineItem
      if(params[:update_gift_card_id] != "" and params[:update_gift_card_id])
        previousLineItem = Spree::LineItem.where(:id => params[:update_gift_card_id])[0]
        if previousLineItem
          lia=Hash.new
          li = Hash.new
          l = Hash.new
          l["id"] = previousLineItem.id
          l["quantity"] = 0
          li[0] = l

          lia["line_items_attributes"] = li
          if order.contents.update_cart(lia)
            respond_with(order) do |format|
              format.html do
                if params.has_key?(:checkout)
                  order.next if order.cart?
                  redirect_to checkout_state_path(order.checkout_steps.first)
                else
                  redirect_to cart_path
                end
              end
            end
          else
            respond_with(order)
          end        
        end
      else
        redirect_to cart_path
      end
      # End

      
    rescue ActiveRecord::RecordInvalid
      find_gift_card_variants
      render :action => :new
    end
  end

  def add_address
    @address = Spree::Address.new()
    @address.firstname = params[:options_name]
    @address.lastname = params[:giftcard_address_lastname]
    @address.address1 = params[:giftcard_address_address1]
    @address.address2 = " "
    # United States Country Id
    @address.country_id = 49
    # @address.country_id = params[:shipping_address][:country_id]
    @address.city = params[:giftcard_address_city]
    @address.state_name = params[:giftcard_address_state]
    if params[:shipping_address][:state_id] != nil && params[:shipping_address][:state_id] != ""
      @address.state_id = params[:shipping_address][:state_id]
    else
      @address.state_id = 1
    end
    @address.zipcode = params[:giftcard_address_zipcode]
    @address.phone = params[:giftcard_address_phone]
    @address.save!
    @address
  end

  def find_gift_card_variants
    gift_card_product_ids = Spree::Product.not_deleted.where(gift_card: true).where(product_type: 'VIRTUALGIFTCARD').pluck(:id)
    @gift_card_variants = Spree::Variant.joins(:prices).where(["amount > 0 AND product_id IN (?)", gift_card_product_ids]).order("amount")
  end

end
