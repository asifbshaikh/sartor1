class Spree::ReferralMailerController < Spree::StoreController 
  
  def email_form
    @referral_code = params[:link]
  end
  
  def send_mail
    referral_code = params[:referral_code].to_s
    @referral_link = referral_url(referral_code)
    sender_name = params[:sender_name]
    sender_email = spree_current_user.email
    recipient_name = params[:recipient_name]
    recipient_email = params[:recipient_email]
    options_note = params[:options_note]
    referral_link = @referral_link

    Spree::ReferralMailer.send_referral_url_to_friend(sender_name, sender_email, recipient_name, recipient_email, options_note, referral_link).deliver
    redirect_to '/account'
  end
  
end