Spree::WishlistsController.class_eval do
  def index
    @wishlists = spree_current_user.wishlists
    @wishlist = Spree::Wishlist.new
  end
end
