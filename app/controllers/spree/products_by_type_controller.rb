module Spree
  class ProductsByTypeController < Spree::StoreController
    before_filter :load_taxon, :only => :index

    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/taxons'

    respond_to :html

    def index
      @type = params[:type]
      @products = Spree::Product.is_product_type(@type)
      @taxonomies = Spree::Taxonomy.includes(root: :children)
    end
    
  end
end
