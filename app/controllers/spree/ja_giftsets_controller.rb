module Spree
  class JaGiftsetsController < Spree::GiftCardsController

    def new
      find_gift_card_variants
      @gift_card = GiftCard.new
      @gift_card.send_on = 'now';
      @gift_card.delivery_type = 'recipient'
    end

    private

    def find_gift_card_variants
      gift_card_product_ids = Product.not_deleted.where(is_gift_card: true).where(product_type: 'JA').pluck(:id)
      @gift_card_variants = Variant.joins(:prices).where(["amount > 0 AND product_id IN (?)", gift_card_product_ids]).order("amount")
    end

    def gift_card_params
      params.require(:gift_card).permit(:email, :name, :note, :variant_id, :send_on)
    end

  end
end
