class Spree::ContactMailerController < Spree::StoreController
  def send_mail(email,name, message)
    Spree::ContactMailer.send_contact_us_data(email,name, message).deliver
  end	
end
