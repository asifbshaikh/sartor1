Spree::Api::VariantsController.class_eval do
 def index
    custom_shirt_flag = params[:custom_shirt_flag]
    if(custom_shirt_flag != nil && custom_shirt_flag != "true")
      @variants = @variants = scope.
      includes(:option_values).where(query).ransack(params[:q]).result.page(params[:page]).per(params[:per_page]) 
      respond_with(@variants)
    else
      @variants = scope.includes(:option_values).ransack(params[:q]).result.page(params[:page]).per(params[:per_page])
      respond_with(@variants)
    end
  end
  def query
    " \"spree_products\".\"product_type\"  NOT LIKE 'CUSTOMSHIRT' "
  end
end