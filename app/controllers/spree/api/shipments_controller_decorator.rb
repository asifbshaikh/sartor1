Spree::Api::ShipmentsController.class_eval do

  before_filter :find_and_update_shipment, only: [:ship, :ready, :add, :remove, :update_item, :delete_line_item]

  def update_item
    variant = Spree::Variant.find(params[:variant_id])
    price = params[:price].to_d
    line_item_id = params[:line_item_id].to_i
    notes = params[:note].to_s
    shirt_configuration = params[:shirt_configuration]
    job_number = params[:job_number].to_s
    @shipment.order.contents.edit_price(variant, price, line_item_id, @shipment)
    @shipment.order.contents.save_note(line_item_id, notes)
    @shipment.order.contents.save_shirt_configuration(line_item_id, shirt_configuration, @shipment)
    @shipment.order.contents.save_job_number(line_item_id, job_number)

    respond_with(@shipment, default_template: :show)
  end

  def create
    # TODO Can remove conditional here once deprecated #find_order is removed.
    unless @order.present?
      @order = Spree::Order.find_by!(number: params[:shipment][:order_id])
      authorize! :read, @order
    end
    authorize! :create, Spree::Shipment
    variant = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].to_i
    shirt_configuration= params[:shirt_configuration]
    price = params[:variant_price].to_d
    note = params[:variant_note]
    profile_id = params[:profile_id].to_i
    previous_line_item_id = params[:line_item_id].to_i
    parent_line_item_id = params[:parent_line_item_id].to_i

    @shipment = @order.shipments.create(stock_location_id: params[:stock_location_id])
    line_item = @order.contents.add(variant, quantity, nil, @shipment, price, note, profile_id)
    @order.contents.edit_price(variant, price, line_item.id, @shipment)
    @shipment.address = @order.ship_address
    @shipment.save!
    @shipment.add_shipping_methods
    @shipment.determine_shipment_state

    current_order = @order
    handle_different_types_of_items(variant, line_item, shirt_configuration, previous_line_item_id, current_order, parent_line_item_id)

    respond_with(@shipment.reload, default_template: :show)
  end

  def add
    variant = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].to_i
    shirt_configuration= params[:shirt_configuration]
    price = params[:variant_price].to_d
    note = params[:variant_note]
    profile_id = params[:profile_id].to_i
    previous_line_item_id = params[:line_item_id].to_i
    parent_line_item_id = params[:parent_line_item_id].to_i

    current_order = @shipment.order
    line_item = @shipment.order.contents.add(variant, quantity, nil, @shipment, price, note, profile_id)
    @shipment.order.contents.edit_price(variant, price, line_item.id, @shipment)
    @shipment.determine_shipment_state
    handle_different_types_of_items(variant, line_item, shirt_configuration, previous_line_item_id, current_order, parent_line_item_id)
    respond_with(@shipment, default_template: :show)
  end

  def delete_line_item
    variant = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].to_i
    line_item = Spree::LineItem.find(params[:line_item_id])

    @shipment.order.contents.delete_line_item(variant, quantity, @shipment, line_item)
    @shipment.reload if @shipment.persisted?
    respond_with(@shipment, default_template: :show)
  end

  def handle_different_types_of_items(variant, line_item, shirt_configuration, previous_line_item_id, current_order, parent_line_item_id)
    if variant.product.product_type == "SHIRT" || variant.product.product_type == "CUSTOMSHIRT"
       add_shirt_configuration(line_item, shirt_configuration)
    end
    if variant.product.product_type == "VIRTUALGIFTCARD"
      handle_virtual_card_info(previous_line_item_id, line_item, current_order)
    end
    if parent_line_item_id != nil && parent_line_item_id != 0
      handle_subline_items(line_item, parent_line_item_id)
    end
  end

  def add_shirt_configuration(line_item, shirt_configuration)

    item_shirt_configuration = ShirtConfiguration.new(
      :collar_code => shirt_configuration["collar_id"],
      :cuff_code => shirt_configuration["cuff_id"],
      :pocket_code => shirt_configuration["pocket_id"],
      :button_code => shirt_configuration["button_id"],
      :placket_code => shirt_configuration["placket_id"]
    )
    line_item.shirt_configuration = item_shirt_configuration
  end

  def handle_virtual_card_info(previous_line_item_id, line_item, current_order)
     gift_card_details = VirtualGiftCardsDetails.find_by(:line_item_id => previous_line_item_id)
      if gift_card_details != nil
        gift_card_details.line_item_id = line_item.id
        gift_card_details.save!
      end
      all_line_items = Spree::LineItem.where(:order_id => current_order.id)
      all_line_items.each do |item|
        if item.parent_lineitem_id != nil && item.parent_lineitem_id == previous_line_item_id
          item.parent_lineitem_id = line_item.id
          item.save!
        end
      end
  end

  def handle_subline_items(line_item, parent_line_item_id)
    line_item.parent_lineitem_id = parent_line_item_id
    line_item.save!
  end

end
