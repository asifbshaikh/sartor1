class Spree::AppointmentContactsController < Spree::StoreController

  helper "spree/products"
  def create
    @appointment_contact = Spree::AppointmentContact.new(contact_params)

    if @appointment_contact.save
      Spree::AppointmentContactMailer.appointment_contact_notification(@appointment_contact).deliver

      redirect_to(spree.root_path, :notice => Spree.t('Thank you for your appointment request. We will reach out to you within one business day.'))
    else
      render :new
    end
  end

  def new
    session[:return_to] = request.fullpath
    @appointment_contact = Spree::AppointmentContact.new
    @appoinmentControllerSelected = "selected"
    # @taxonomies = Spree::Taxonomy.includes(root: :children)   todo: remove if unused
  end

  def sartor_office

  end

  def office_visit

  end

  private

  def accurate_title
    "Appointment Request"
  end

  def contact_params
    params.require(:appointment_contact).permit(:name, :email, :phone, :location_type, :address, :message)
  end

end
