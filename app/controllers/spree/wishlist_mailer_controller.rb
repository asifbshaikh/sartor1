class Spree::WishlistMailerController < Spree::StoreController

  def email_form
    @wishlist_id = params[:id]

  end

  def send_mail
    wishlist_id = params[:wishlist_id].to_s
    sender_name = params[:sender_name]
    sender_email = spree_current_user.email
    recipient_name = params[:recipient_name]
    recipient_email = params[:recipient_email]
    options_note = params[:options_note]
    host_url = request.env['HTTP_HOST'].to_s 
    append_url = "http://" + host_url + "/wishlists/"
    Spree::WishlistMailer.send_wishlist_to_friend(sender_name, sender_email, recipient_name, recipient_email, options_note, wishlist_id, append_url).deliver
    @wishlists = Spree::Wishlist.where(user_id: spree_current_user.id)
    @wishlists.each do |wishlist|
      if wishlist.is_default
          @access_hash = wishlist.access_hash
      end  
    end  
    redirect_to append_url.to_s + @access_hash.to_s
  end
end
