class UsualStyle < ActiveRecord::Base

#NAMES
  COLLARS = {1 => "Semi Spread", 2 => "Full Spread", 3 => "Point Spread", 4 => "Double Button", 5 => "Button-down"}
  CUFFS = {1 => "Single Button", 3 => "Double Button", 5 => "French", 6 => "Short Sleeve"}
  POCKETS = {1 => "No Pocket", 2 => "Rounded", 3 => "Angled", 4 => "Double Flap"}
  LENGTHS = {1 => "Tucked", 2 => "Middle", 3 => "Untucked"}
  BUTTONS = {1 => "Normal", 2 => "White", 3 => "Grey"}
  PLACKETS ={1 => "Covered", 2 => "Regular", 3 => "French", 4 => "Tunic"}


#IMAGES
  COLLARS_IMAGE = {1 => "semi_spread", 2 => "full_spread", 3 => "point_spread", 4 => "double_button", 5 => "button_down"}
  CUFFS_IMAGE = {1 => "single_button", 3 => "double_button_cuff", 5 => "french", 6 => "short_sleeve"}
  POCKETS_IMAGE = {1 => "none", 2 => "rounded", 3 => "angled" , 4 => "double_flap"}
  LENGTHS_IMAGE = {1 => "tucked", 2 => "middle", 3 => "untucked"}
  BUTTONS_IMAGE = {1 => "normal", 2 => "white", 3 => "grey"}
  PLACKETS_IMAGE ={1 => "placket_covered",2 => "placket_regular", 3 => "placket_french", 4 => "placket_tunic"}

#FIT TYPES
  FITS = {1 => "Slim", 2 => "Extra Slim", 3 => "Classic", 4 => "Tailored Slim", 5 => "Tailored Classic" }

#TYPES
  def get_collar_types
    return safe_invert COLLARS
  end

  def get_cuff_types
    return safe_invert CUFFS
  end

  def get_pocket_types
    return safe_invert POCKETS
  end

  def get_fit_types
    return safe_invert FITS
  end

  def get_length_types
    return safe_invert LENGTHS
  end

  def get_button_types
    return safe_invert BUTTONS
  end

  def get_placket_types
    return safe_invert PLACKETS
  end

#NAME
  def collar_name()
    return COLLARS[collar_code]
  end

  def cuff_name()
    return CUFFS[cuff_code]
  end

  def pocket_name()
    return POCKETS[pocket_code]
  end

  def fit_name()
    return FITS[fit_code]
  end

  def length_name()
    return LENGTHS[length_code]
  end

   def button_name()
    return BUTTONS[button_code]
  end

  def placket_name()
    return PLACKETS[placket_code]
  end



 #For Images
 def collar_image()
    return COLLARS_IMAGE[collar_code]
  end

  def cuff_image()
    return CUFFS_IMAGE[cuff_code]
  end

  def pocket_image()
    return POCKETS_IMAGE[pocket_code]
  end

  def length_image()
    return LENGTHS_IMAGE[length_code]
  end

  def button_image()
    return BUTTONS_IMAGE[button_code]
  end

  def placket_image()
    return PLACKETS_IMAGE[placket_code]
  end


  def safe_invert config_hash
    config_hash.invert
  end
end
