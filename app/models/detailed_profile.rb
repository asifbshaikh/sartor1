class DetailedProfile < ActiveRecord::Base
  acts_as_heir_of :profile

  FITS = [
    'extra slim',
    'slim',
    'Regular',
    'Classic'
  ]

  attr_accessor :Generator_Fit
  @Generator_Fit = "slim"
  attr_accessor :Body_Profile_Unit
  Body_Profile_Unit = "inch"

  MINIMUM_HIP_ALLOWANCE_MULTIPLER = 0.8
  HIP_ALLOWANCE_MULTIPLER_FLARE_THRESHOLD = 0.75
  UNTUCKED_WAIST_HIP_MAX_RATIO = 0.5
  MIDTUCK_WIST_HIP_MAX_RATIO = 0.75
  BICEP_CHEST_DEFAULT_RATIO = 0.32

  # FIXED ALLOWANCES CONSTANTS
  CUFF_ALLOWANCE_INCH = 1.75
  COLLAR_ALLOWANCE_INCH = 0.5
  SLEEVE_ALLOWANCE_INCH = 0.75

  # ALLOWANCE FUNCTIONS
  def torso_allowance
    case @Generator_Fit
      when "extra slim"
        allowance = (chest - 38) * 0.05 + 3
      when "slim"
        allowance = (chest - 38) * 0.08 + 3.5
    end
   allowance
  end

  def bicep_allowance
    case @Generator_Fit
      when "extra slim"
        allowance = 1.6
      when "slim"
        allowance = 2.4
      when "Regular"
        allowance = 3.5
      when "Classic"
        allowance = 4.5
    end
    allowance
  end

  def max_hip_flare
    case @Generator_Fit
      when "extra slim"
        max_flare = 0.14
      when "slim"
        max_flare = 0.12
      when "Regular"
        max_flare = 0.10
      when "Classic"
        max_flare = 0.10
    end
    max_flare
  end

  # FIXED ALLOWANCE GETTERS
  def shirt_cuff
    profile_is_inches ? round_quarter(wrist + CUFF_ALLOWANCE_INCH) : wrist + inches_to_cm(CUFF_ALLOWANCE_INCH)
  end

  def shirt_collar
    profile_is_inches ? round_quarter(neck + COLLAR_ALLOWANCE_INCH) : collar + inches_to_cm(COLLAR_ALLOWANCE_INCH)
  end

  def shirt_sleeve
    profile_is_inches ? round_quarter(sleeve + SLEEVE_ALLOWANCE_INCH) : sleeve + inches_to_cm(SLEEVE_ALLOWANCE_INCH)
  end

  def shirt_shoulder
    profile_is_inches ? round_quarter(yoke) : yoke
  end

  def shirt_length
    profile_is_inches ? round_quarter(length) : length
  end

  # BICEP ALLOWANCE GETTER
  def shirt_bicep
    bicep_allowance_method = bicep + bicep_allowance
    chest_multiplier_method = BICEP_CHEST_DEFAULT_RATIO * shirt_chest
    round_quarter([bicep_allowance_method, chest_multiplier_method].max)
  end

  # TORSO ALLOWANCE GETTERS
  # added to_f to handle nil condition
  def shirt_chest
    shirt = profile_is_inches ? round_quarter(chest + torso_allowance.to_f) : chest + inches_to_cm(torso_allowance.to_f)
  end

  def shirt_waist
    waist_allowance_method = waist + torso_allowance.to_f
    base_hip_plus_allowance = hip + torso_allowance.to_f
    min_waist_tucked = base_hip_plus_allowance / (1 + max_hip_flare)
    round_quarter([waist_allowance_method, min_waist_tucked].max)
  end

  def shirt_hip
    base_hip_plus_allowance = hip + torso_allowance.to_f
    profile_is_inches ? round_quarter(hip + torso_allowance.to_f) : hip + inches_to_cm(torso_allowance.to_f)
  end


  # UTILITY FUNCTIONS
  def inches_to_cm (measurement)
    measurement * 2.54
  end

  def profile_is_inches
    Body_Profile_Unit == "inch"
  end

  def round_quarter (measurement)
    (4 * measurement).round.to_d / 4
  end

  # attr_accessible :name, :embroidery_name, :height_ft, :height_in, :weight, :default_fit, :neck, :yoke, :sleeve, :chest, :waist, :hip, :length, :length_untucked, :bicep, :wrist, :wrist_watch, :watch_hand
    validates :name, presence: {:message => "Measurement name can not be blank" }
    validates :neck, presence: {:message => "Neck can not be blank" }
    validates :yoke, presence: {:message => "Yoke can not be blank" }
    validates :sleeve, presence: {:message => "Sleeve can not be blank" }
    validates :chest, presence: {:message => "Chest can not be blank" }
    validates :waist, presence: {:message => "Chest can not be blank" }
    validates :hip, presence: {:message => "Hip can not be blank" }
    validates :bicep, presence: {:message => "Bicep can not be blank" }
    validates :wrist, presence: {:message => "Wrist can not be blank" }
    validates :length, presence: {:message => "Tucked Length can not be blank" }
    validates :length_untucked, presence: {:message => "Untucked length can not be blank" }
end
