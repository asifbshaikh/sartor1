class AbilitySartor
  include CanCan::Ability
  def initialize(user)
    can :create, Profile
    can :create, DetailedProfile
    can :create, ShirtProfile    
    can :index, Profile
  
    can :read, Profile do |profile|
      profile.user == user
    end
    can :read, DetailedProfile do |profile|
      profile.user == user
    end
    can :read, ShirtProfile do |profile|
      profile.user == user
    end
    
    can :update, Profile do |profile|
      profile.user == user
    end
    can :update, DetailedProfile do |profile|
      profile.user == user
    end
    can :update, ShirtProfile do |profile|
      profile.user == user
    end
    
    can :destroy, Profile do |profile|
      profile.user == user
    end
    can :destroy, DetailedProfile do |profile|
      profile.user == user
    end
    can :destroy, ShirtProfile do |profile|
      profile.user == user
    end
    
  end
end