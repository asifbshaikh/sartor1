Spree::LineItem.class_eval do

  belongs_to :profile
  has_one :shirt_configuration

  has_many :sub_lineitems, class_name: "Spree::LineItem",
                          foreign_key: "parent_lineitem_id"

  belongs_to :parent_lineitem, class_name: "Spree::LineItem"
  
  def update_tax_charge
    if (order.manual_order == false) #If order is a manual order, do not use spree taxes.
      Spree::TaxRate.adjust(order, [self])
    end
  end

end