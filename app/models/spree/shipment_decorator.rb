Spree::Shipment.class_eval do
  
  def determine_shipment_state
    new_shipment = self
    shipments = new_shipment.order.shipments
    shipments.each do |shipment|
      all_products_available_on_hand = false
      shipment.inventory_units.each do |unit|
        line_item = Spree::LineItem.find_by(:id => unit.line_item_id)
        stock_item = Spree::StockItem.find_by(:variant_id => line_item.variant.id, :stock_location_id => shipment.stock_location_id)
        if stock_item.count_on_hand > 0
        all_products_available_on_hand = true
        else
        all_products_available_on_hand = false
        break
        end
      end
      if shipment.state != "shipped"
        if all_products_available_on_hand && shipment.order.payment_state == "paid" && shipment.stock_location.shippable
          shipment.state = "ready"
        else
          shipment.state = "pending"
        end
      end
      shipment.save!
    end
  end

  def add_shipping_methods
    shipment = self
    if shipment.stock_location.shippable
      shipping_methods = Spree::ShippingMethod.all
      set_first_method_true = true
      shipping_methods.each do |method|
        if set_first_method_true
        shipment.add_shipping_method(method, true)
        set_first_method_true = false
        else
        shipment.add_shipping_method(method, false)
        end
      end
    else
      Spree::ShippingRate.where(:shipment_id => shipment.id).destroy_all
    end
    shipment.save!
  end
  
end