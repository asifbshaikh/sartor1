module Spree
  Image.class_eval do
    
    has_attached_file :attachment,
                  styles: { mini: '48x96>', small: '100x200>', product: '240x480>', large: '400x800>',
                   huge: '1200x2400', :'mini@2x' => '96x192>', :'small@2x' => '200x400>', :'product@2x' => '480x960>',
                    :'large@2x' => '1200x2400>', :'huge@2x' => '2400x4800' },
                  default_style: :product,
                  url: '/spree/products/:id/:basename-:style.:extension',
                  path: ':rails_root/public/spree/products/:id/:basename-:style.:extension',
                  convert_options: { all: '-strip -auto-orient' }
                  
  end
end