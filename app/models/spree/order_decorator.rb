Spree::Order.class_eval do

  def create_tax_charge!
    #Do not calculate taxes if order is a manual order
    if (self.manual_order == false)
      Spree::TaxRate.adjust(self, line_items)
      Spree::TaxRate.adjust(self, shipments) if shipments.any?
    end
  end
  
  def display_custom_tax_total
    Spree::Money.new(tax_amount, { currency: currency })
  end
  
  def display_tax_total
    Spree::Money.new(included_tax_total + additional_tax_total + (tax_amount || 0), { currency: currency })
  end

  #This was done to fix the bug related to getting wrong stock location...
  #...while adding the first product in the order
  def create_proposed_shipments
      if(self.manual_order == false)
        adjustments.shipping.delete_all
        shipments.destroy_all

        packages = Spree::Stock::Coordinator.new(self).packages
        packages.each do |package|
          shipments << package.to_shipment
        end
      end
      shipments
   end
   
    def deliver_order_confirmation_email
      Spree::OrderMailer.confirm_email(self.id).deliver
      update_column(:confirmation_delivered, true)
      send_gift_card_mail
    end
    
    def send_gift_card_mail
      line_items.each do |item|
        if item.gift_card
          virtual_gift_cards_details = VirtualGiftCardsDetails.find_by(
            :line_item_id => item.id
          )
          virtual_gift_card = Spree::VirtualGiftCard.find_by(
            :line_item_id => item.id
          )
          if virtual_gift_cards_details.card_type == "virtual"
            Spree::VirtualGiftCardsMailer.send_redemption_code(virtual_gift_cards_details, virtual_gift_card, item.order.user_id).deliver  
          end
        end
      end
    end
end

