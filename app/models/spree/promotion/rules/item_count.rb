module Spree
  class Promotion
    module Rules
      class ItemCount < PromotionRule
        preference :count, :integer, default: 3
        preference :operator, :string, default: '>='

        OPERATORS = ['gt', 'gte']

        def applicable?(promotable)
          promotable.is_a?(Spree::Order)
        end

        def eligible?(order, options = {})
          item_count = order.item_count
          logger.debug("!! ItemCount promo rule -- item_count: #{item_count}; preferred_count: #{preferred_count}; send: #{item_count.send(preferred_operator == 'gte' ? :>= : :>, preferred_count)}")
          item_count.send(preferred_operator == 'gte' ? :>= : :>, preferred_count)
        end
      end
    end
  end
end
