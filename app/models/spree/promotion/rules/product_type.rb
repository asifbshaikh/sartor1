module Spree
  class Promotion
    module Rules
      class ProductType < PromotionRule
        preference :product_type, :string, default: "SHIRT"

        def applicable?(promotable)
          promotable.is_a?(Spree::Order)
        end

        def eligible?(order, options = {})
          matching_items = order.line_items.select {|li| li.variant.product.product_type == preferred_product_type}
          logger.debug("!! ProductType promo -- required_type: #{preferred_product_type}; matching items: #{matching_items.to_yaml}; eligible: #{matching_items.count > 0}")
          return matching_items.count > 0
        end
      end
    end
  end
end
