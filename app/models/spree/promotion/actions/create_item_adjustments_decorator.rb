Spree::Promotion::Actions::CreateItemAdjustments.class_eval do
  def perform(payload = {})
    order = payload[:order]
    # Find only the line items which have not already been adjusted by this promotion
    # HACK: Need to use [0] because `pluck` may return an empty array, which AR helpfully
    # coverts to meaning NOT IN (NULL) and the DB isn't happy about that.
    already_adjusted_line_items = [0] + self.adjustments.pluck(:adjustable_id)
    result = false
    order.line_items.where("id NOT IN (?)", already_adjusted_line_items).find_each do |line_item|
      if promotion.line_item_actionable?(order, line_item)
        current_result = self.create_adjustment(line_item, order)
        result ||= current_result
      end
    end
    return result
  end
end