Spree::Product.class_eval do

  self.scope("ascend_by_sort_code", relation.order("spree_products.sort_code ASC"))

  has_one :shirt_configuration

  has_one :fabric_detail

  before_create :build_shirt_configuration

  accepts_nested_attributes_for :shirt_configuration

  accepts_nested_attributes_for :fabric_detail

  #scope :is_product_type, -> (type) { where product_type: type }

  def product_image
    image = images.find_by(image_type: 'Main')
    if image == nil
      image = images.first
    end

    image
  end

  def fabric_image
    image = images.find_by(image_type: 'Fabric')
  end

  def has_fabric_image
    images.find_by(image_type: 'Fabric') != nil
  end

  def image_url_or_noimage(image, type)
    image_url = nil

    if image == nil
      image_url = "/NoImageAvailable.jpg"
    else
      image_url = image.attachment.url(type)
    end
    image_url
  end

  def retina_url(url)
    #render all types of images for retina
    if url
      if url[".jpg"]
        url[".jpg"] = "@2x.jpg"
      elsif url[".png"]
        url[".png"] = "@2x.png"
      elsif url[".jpeg"]
        url[".jpeg"] = "@2x.jpeg"
      end
    else
      url = "/NoImageAvailable.jpg"
    end
    url
  end

  def product_image_url(type)
    image_url_or_noimage(product_image, type)
  end

  def fabric_image_url(type)
    image_url_or_noimage(fabric_image, type)
  end

  def product_image_retina_url(type)
    retina_url(product_image_url(type))
  end

  def fabric_image_retina_url(type)
    retina_url(fabric_image_url(type))
  end

end
