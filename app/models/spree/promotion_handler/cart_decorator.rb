Spree::PromotionHandler::Cart.class_eval do
  
  def activate
    promotions.each do |promotion|
      if (line_item && promotion.eligible?(line_item)) || promotion.eligible?(order)
        promotion.activate(line_item: line_item, order: order, current_promotion: promotion)
      end
    end
  end
end