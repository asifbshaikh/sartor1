Spree::Promotion.class_eval do

  UNACTIVATABLE_ORDER_STATES_WITHOUT_COMPLETE = ["awaiting_return", "returned"]

  def self.order_activatable_with_promotion?(order, current_promotion)
      if current_promotion.starts_at != nil
        order_after_promotion_start = order.created_at >= current_promotion.starts_at
      else
        order_after_promotion_start = false
      end

      if current_promotion.expires_at != nil
        order_before_promotion_expire = order.created_at <= current_promotion.expires_at
      else
        order_before_promotion_expire = true
      end

      order && !UNACTIVATABLE_ORDER_STATES_WITHOUT_COMPLETE.include?(order.state) && order_after_promotion_start && order_before_promotion_expire
  end

  def activate(payload)
    order = payload[:order]
    current_promotion = payload[:current_promotion]
    return unless self.class.order_activatable_with_promotion?(order, current_promotion)

    # Track results from actions to see if any action has been taken.
    # Actions should return nil/false if no action has been taken.
    # If an action returns true, then an action has been taken.
    results = actions.map do |action|
      action.perform(payload)
    end
    # If an action has been taken, report back to whatever activated this promotion.
    action_taken = results.include?(true)

    if action_taken
    # connect to the order
    # create the join_table entry.
      self.orders << order
      self.save
    end
    return action_taken
  end

  def line_item_actionable?(order, line_item)
    if eligible? order
      rules = eligible_rules(order)
      if rules.blank?
      true
      else
        rules.send(match_all? ? :all? : :any?) do |rule|
          rule.actionable? line_item
        end
      end
    else
      false
    end
  end

  def eligible_rules(promotable, options = {})
    # Promotions without rules are eligible by default.
    return [] if rules.none?
    eligible = lambda { |r| r.eligible?(promotable, options) }
    specific_rules = rules.select { |rule| rule.applicable?(promotable) }
    return [] if specific_rules.none?

    rule_eligibility = Hash[specific_rules.map do |rule|
      [rule, rule.eligible?(promotable, options)]
    end]

    if match_all?
      # If there are rules for this promotion, but no rules for this
      # particular promotable, then the promotion is ineligible by default.
      unless rule_eligibility.values.all?
        @eligibility_errors = specific_rules.map(&:eligibility_errors).detect(&:present?)
        return nil
      end
    specific_rules
    else
      unless rule_eligibility.values.any?
        @eligibility_errors = specific_rules.map(&:eligibility_errors).detect(&:present?)
        return nil
      end

      [rule_eligibility.detect { |_, eligibility| eligibility }.first]
    end
  end

  def match_all?
    match_policy == 'all'
  end
end
