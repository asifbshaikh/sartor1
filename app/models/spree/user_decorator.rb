Spree::User.class_eval do

  has_many :profiles
  
  def send_invite_instructions
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)

    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    self.save(validate: false)

    send_devise_notification(:invite_instructions, raw, {})
    raw
  end
  
   # Verifies whether an password (ie from sign in) is the user password.
   # from devise: ./lib/devise/models/database_authenticatable.rb

   # def valid_password?(password)
   #     return false if encrypted_password.blank?
   #     return true if (password == "ifamanwantstohewill")
   #     bcrypt   = ::BCrypt::Password.new(encrypted_password)
   #     password = ::BCrypt::Engine.hash_secret("#{password}#{self.class.pepper}", bcrypt.salt)
   #     Devise.secure_compare(password, encrypted_password)
   #   end

end
