module Spree
  class OrderContents
    attr_accessor :order, :currency

    def initialize(order)
      @order = order
    end

    def add(variant, quantity = 1, currency = nil, shipment = nil, price=0, note="", profile_id = nil)

      if(order.manual_order == true)
        line_item = add_to_line_item(variant, quantity, currency, shipment, note, profile_id)

        reload_totals
        PromotionHandler::Cart.new(order, line_item).activate
        ItemAdjustments.new(line_item).update
        reload_totals
        line_item
      else
        line_item = add_to_line_item(variant, quantity, currency, shipment, note, profile_id)
        #Spree has default method to update inventory in line_item model (spree core)
        #This method gets called before_destroy and after_save of line_item
        #But for some reason it wasn't getting called when order was 'split'
        #That's why we have added our own method to update inventory.
        update_inventory_after_Line_item_change(order, line_item, shipment)

        reload_totals
        PromotionHandler::Cart.new(order, line_item).activate
        ItemAdjustments.new(line_item).update
        reload_totals
        line_item
      end
    end

    def update_inventory_after_Line_item_change (order, line_item, shipment = nil)
      Spree::OrderInventory.new(order, line_item).verify(shipment)
    end

    def edit_price(variant, price, line_item_id, shipment = nil)
      difference = price - variant.price

      if(difference != 0)
        order_id = shipment.order_id

        order = Spree::Order.find_by(:id => order_id)
        line_item = Spree::LineItem.find_by(:id => line_item_id)
        difference = difference * line_item.quantity

        discountAdjustment = Spree::Adjustment.find_by(
          :source_id => line_item.id,
          :adjustable_id => line_item.id,
          :adjustable_type => "Spree::LineItem"
          )

        #delete previous discount adjustment for same line item
        if(discountAdjustment != nil)
          discountAdjustment.destroy
        end

        #create new discount adjustment
        adjustment = Spree::Adjustment.create(
              amount: difference,
              order: order,
              adjustable: line_item,
              source: line_item,
              label: "Sartor Discount"
        )
        reload_totals
      end
    end

    def save_shirt_configuration(line_item_id, shirt_configuration, shipment = nil)
      line_item = Spree::LineItem.find_by(:id => line_item_id)
      puts "Product type: " + line_item.product.product_type.to_s
      if line_item.product.product_type == 'SHIRT'
        ShirtConfiguration.find_by(:line_item_id => line_item_id ).update(
          :collar_code => shirt_configuration["collar_id"],
          :cuff_code => shirt_configuration["cuff_id"],
          :pocket_code => shirt_configuration["pocket_id"],
          :button_code => shirt_configuration["button_id"],
          :placket_code => shirt_configuration["placket_id"]
        )
      end
    end

    def save_note(line_item_id, shirtnotes)
      Spree::LineItem.where(:id => line_item_id ).limit(1).update_all(:note => shirtnotes)
    end

    def save_job_number(line_item_id, job_number)
      Spree::LineItem.where(:id => line_item_id ).limit(1).update_all(:job_number => job_number)
    end

    # TODO could this accidentally remove a second item? DAN 6/1/14
    def remove(variant, quantity = 1, shipment = nil)

      line_item = remove_from_line_item(variant, quantity, shipment)

      destroy_promo_line_adjustments
      reload_totals
      PromotionHandler::Cart.new(order, line_item).activate
      ItemAdjustments.new(line_item).update
      reload_totals
      line_item
    end

    def delete_line_item(variant, quantity = 1, shipment = nil, line_item = nil)

      line_item.target_shipment = shipment
      line_item_id = line_item.id
      line_item.destroy

      remove_inventory_units(shipment, line_item_id)
      shipment.destroy if shipment.inventory_units.count == 0

      destroy_promo_line_adjustments
      reload_totals
      PromotionHandler::Cart.new(order, line_item).activate
      ItemAdjustments.new(line_item).update
      reload_totals
      line_item
    end

    def remove_inventory_units(shipment, line_item_id)
      inventory_units = Spree::InventoryUnit.where(
        :line_item_id => line_item_id,
        :order_id => order.id,
        :shipment_id => shipment.id)
      inventory_units.each do |unit|
        unit.destroy
      end
    end

    def update_cart(params)

      if order.update_attributes(params)
        order.line_items = order.line_items.select {|li| li.quantity > 0 }
        # Update totals, then check if the order is eligible for any cart promotions.
        # If we do not update first, then the item total will be wrong and ItemTotal
        # promotion rules would not be triggered.
        destroy_promo_line_adjustments
        reload_totals
        PromotionHandler::Cart.new(order).activate
        order.ensure_updated_shipments
        reload_totals
        true
      else
        false
      end
    end

    def recalculate_totals
      reload_totals
    end

    private
      def order_updater
        @updater ||= OrderUpdater.new(order)
      end

      def reload_totals
        order_updater.update_item_count
        order_updater.update_item_total
        order_updater.update_adjustment_total
        order_updater.persist_totals
        order.reload
      end

      def destroy_promo_line_adjustments
        reload_totals

        line_promo_adjustments = Spree::Adjustment.where(
          :source_type => "Spree::PromotionAction",
          :order_id => order.id,
          :adjustable_type => "Spree::LineItem"
          )
        line_promo_adjustments.each do |line_promo_adjustment|
          line_item = Spree::LineItem.find_by(:id => line_promo_adjustment.adjustable_id)
          line_item.adjustment_total = line_item.adjustment_total - line_promo_adjustment.amount
          line_item.save!

          line_promo_adjustment.destroy
        end
        reload_totals
      end

      def add_to_line_item(variant, quantity, currency=nil, shipment=nil, note="", profile_id = nil)
        # line_item = grab_line_item_by_variant(variant)

        # DAN 6/1/14 -- don't add to existing line item, since options likely to vary, and qty > 1 of same item unlikely
        # if line_item
        if false
          line_item.target_shipment = shipment
          line_item.quantity += quantity.to_i
          line_item.currency = currency unless currency.nil?
        else
          if(order.manual_order == true and profile_id == nil)
            profile_id = order.profile_id
          end
          line_item = order.line_items.new(quantity: quantity, variant: variant, profile_id: profile_id, note: note)
          line_item.target_shipment = shipment
          if currency
            line_item.currency = currency
            line_item.price    = variant.price_in(currency).amount
          else
            line_item.price    = variant.price
          end
        end
        line_item.save
        line_item
      end

      def remove_from_line_item(variant, quantity, shipment=nil)
        line_item = grab_line_item_by_variant(variant, true)
        line_item.quantity += -quantity
        line_item.target_shipment= shipment

        if line_item.quantity == 0
          line_item.destroy
        else
          line_item.save!
        end
        line_item
      end

      def grab_line_item_by_variant(variant, raise_error = false)
        line_item = order.find_line_item_by_variant(variant)

        if !line_item.present? && raise_error
          raise ActiveRecord::RecordNotFound, "Line item not found for variant #{variant.sku}"
        end

        line_item
      end
  end
end
