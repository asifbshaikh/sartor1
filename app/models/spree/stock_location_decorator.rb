Spree::StockLocation.class_eval do
  scope :shippable, -> { where(shippable: true) }
end