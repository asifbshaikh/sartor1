Spree::Stock::Coordinator.class_eval do
  
  def build_packages(packages = Array.new)
    stock_locations = Spree::StockLocation.active.shippable
    stock_location = stock_locations.first
    stock_location.stock_items.where(:variant_id => order.line_items.pluck(:variant_id))
    packer = build_packer(stock_location, order)
    packages += packer.packages
    packages
  end
end