Spree::ItemAdjustments.class_eval do
  def update_adjustments

    sartor_discount = 0
    promo_total = 0
    included_tax_total = 0
    additional_tax_total = 0

    run_callbacks :promo_adjustments do
      promotion_total = adjustments.promotion.reload.map(&:update!).compact.sum
      unless promotion_total == 0
        choose_best_promotion_adjustment
      end
      promo_total = best_promotion_adjustment.try(:amount).to_f
    end

    #When 'item' is an order
    if (item.class.to_s == "Spree::Order")
      order = item
      #Do not include spree tax mechanism if order is a manual order
      if (order.manual_order == false)
        run_callbacks :tax_adjustments do
          tax = (item.respond_to?(:all_adjustments) ? item.all_adjustments : item.adjustments).tax
          included_tax_total = tax.included.reload.map(&:update!).compact.sum
          additional_tax_total = tax.additional.reload.map(&:update!).compact.sum
        end
      else
        taxAdjustment = Spree::Adjustment.find_by(
          :source_id => order.id,
          :adjustable_id => order.id,
          :adjustable_type => "Spree::Order"
        )
        if (taxAdjustment != nil)
          additional_tax_total = taxAdjustment.amount
        end
      end
    else if (item.class.to_s == "Spree::LineItem")
        #When 'item' is a line item
        order = Spree::Order.find_by(:id => item.order_id)
        custom_adjustments = adjustments.where(:source_type  => 'Spree::LineItem', :source_id => item.id);
        custom_adjustments.each do |a|
          sartor_discount = sartor_discount + a.amount
        end
        if (order.manual_order == false)
          run_callbacks :tax_adjustments do
            tax = (item.respond_to?(:all_adjustments) ? item.all_adjustments : item.adjustments).tax
            included_tax_total = tax.included.reload.map(&:update!).compact.sum
            additional_tax_total = tax.additional.reload.map(&:update!).compact.sum
          end
        end
      else
      #When 'item' is a shipment
        order = Spree::Order.find_by(:id => item.order_id)

        if (order.manual_order == false)
          run_callbacks :tax_adjustments do
            tax = (item.respond_to?(:all_adjustments) ? item.all_adjustments : item.adjustments).tax
            included_tax_total = tax.included.reload.map(&:update!).compact.sum
            additional_tax_total = tax.additional.reload.map(&:update!).compact.sum
          end
        end
      end
    end

    item.update_columns(
      :promo_total => promo_total,
      :included_tax_total => included_tax_total,
      :additional_tax_total => additional_tax_total,
      :adjustment_total => promo_total + additional_tax_total + sartor_discount,
      :updated_at => Time.now,
    )
  end
end
