Spree::Product.class_eval do
  
  after_create :add_quantity_stock_locations
    
  add_search_scope :is_product_type do |type|
    where product_type: type
  end
    
  add_search_scope :is_shirt do
    where product_type: 'SHIRT'
  end

  add_search_scope :is_available do
    where is_available: true
  end  
  
  add_search_scope :is_tie do
    where product_type: 'TIE'
  end
  
  add_search_scope :is_gift_card do
    where product_type: 'GIFT'
  end
    
  add_search_scope :is_partner_gift_cert do
    where product_type: 'PRTNR'
  end
  
  def add_quantity_stock_locations
        product_id = self.id
        variant_ids = []
        variant_ids = Spree::Variant.where(product_id: product_id).pluck(:id)
        variant_ids.each { |id|
              find_stock_items id 
        }
  end
  
  def find_stock_items(id)
       stock_items_ids = []
       stock_items_ids = Spree::StockItem.where(variant_id: id).pluck(:id)
       stock_items_ids.each { |stock_item_id|
            add_stock_movement stock_item_id
       } 
  end
  
  def add_stock_movement(stock_item_id)
        stock_item_object = Spree::StockMovement.new(stock_item_id: stock_item_id, quantity: 999)   
        stock_item_object.save! 
  end
  
  def self.not_available(available_on = nil, currency = nil)
    joins(:master => :prices)
  end

  def self.allshirts(currency = nil)
    not_deleted.not_available(nil, currency)
  end
  search_scopes << :allshirts  
    
end