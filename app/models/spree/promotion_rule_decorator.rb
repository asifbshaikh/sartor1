Spree::PromotionRule.class_eval do
  # This states if a promotion can be applied to the specified line item
    # It is true by default, but can be overridden by promotion rules to provide conditions
    def actionable?(line_item)
      true
    end
end