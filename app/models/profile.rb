class Profile < ActiveRecord::Base
  acts_as_predecessor
  belongs_to :user, :class_name => "Spree::User"

  #attr_accessible :name, :embroidery_name, :height_ft, :height_in, :weight, :default_fit

  HEIGHTS_FT = [4, 5, 6, 7]
  HEIGHTS_IN = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  WAISTS = [28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 42, 44, 46, 48]
  # [Dan] There are two definitions for LENGTHS in this file -- not sure why?
  # LENGTHS = ["X-Short", "Short", "Regular", "Long", "X-Long"]
  COLLARS = ["14", "14 1/4", "14 1/2", "14 3/4", "15", "15 1/4", "15 1/2", "15 3/4", "16", "16 1/4", "16 1/2", "16 3/4", "17", "17 1/4", "17 1/2", "17 3/4", "18", "18 1/4", "18 1/2", "18 3/4" , "19"]
  SLEEVES = ["30", "30 1/2", "31", "31 1/2", "32", "32 1/2", "33", "33 1/2", "34", "34 1/2", "35", "35 1/2", "36", "36 1/2", "37", "37 1/2", "38", "38 1/2", "39", "39 1/2", "40"]
  CHESTS = [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 50, 52, 54, 56]

  FITS = ["Extra Slim", "Slim", "Classic"]

  WAIST_HIPS = { "extra slim" => 1, "slim" => 2, "Regular" => 3,"Classic" => 4}
  CHEST_SLEEVES = { "extra slim" => 1,"slim" => 2,"Regular" => 3,"Classic" => 4}
  CLASSIFICATION = { "dress" => 1, "business" => 2, "casual" => 3, "play" => 4}
  LENGTHS = { "Tucked" => 1, "Middle" => 2, "Untucked" => 3}

# FIXME:
  WAIST_HIPS_CODE = { 1 => "extra slim",2 => "slim", 3 => "Regular",4 => "Classic"}
  CHEST_SLEEVES_CODE = { 1 => "extra slim",2 => "slim",3 => "Regular",4 => "Classic"}
  CLASSIFICATION_CODE = { 1 => "dress",2 =>  "business", 3 => "casual", 4 => "play"}
  LENGTHS_CODE = { 1 => "Tucked", 2 => "Middle", 3 => "Untucked"}

  WATCH_HANDS = {:None=>"None",:LeftHand=>"Left",:RightHand=>"Right"}
end
