class FabricDetail < ActiveRecord::Base

	WEIGHT = {1 => "1", 2 => "2", 3 => "3", 4 => "4", 5 => "5"}
	OPACITY = {1 => "1", 2 => "2", 3 => "3", 4 => "4", 5 => "5"}
	EASE_OF_CARE = {1 => "1", 2 => "2", 3 => "3", 4 => "4", 5 => "5"}
	WEAVE_TYPE = {1 =>"Poplin", 2 =>"Oxford", 3 =>"Pinpoint Oxford", 4 =>"Royal Oxford", 5 =>"Panama Oxford", 6 =>"End-on-End", 7 =>"Broadcloth", 8 =>"Twill", 9 =>"Herringbone", 10 =>"Dobby" , 11 =>"Jacquard"}

end
