class VirtualGiftCardsDetails < ActiveRecord::Base
  belongs_to :virtual_gift_cards, :class_name => "Spree::VirtualGiftCard"
  belongs_to :line_item, :class_name => "Spree::LineItem"
  belongs_to :ship_address, foreign_key: :ship_address_id, class_name: 'Spree::Address'
    alias_attribute :shipping_address, :ship_address
end
