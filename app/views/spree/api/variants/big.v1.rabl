object @variant
attributes *variant_attributes

cache [I18n.locale, 'big_variant', root_object]

extends "spree/api/variants/small"

child(:stock_items => :stock_items) do
  attributes :id, :count_on_hand, :stock_location_id, :backorderable
  attribute :available? => :available

  glue(:stock_location) do
    attribute :name => :stock_location_name
  end
end

child(:product => :product) do
	attributes :id, :product_type
	
	child(:shirt_configuration => :shirt_configuration) do
		attributes :id, :product_id, :collar_code, :cuff_code, :pocket_code, :fit_code, :length_code, :placket_code
	end
end