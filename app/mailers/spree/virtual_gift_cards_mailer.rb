module Spree
  class VirtualGiftCardsMailer < BaseMailer

    default from: 'test@techbulls.com'

    def send_redemption_code(virtual_gift_cards_details, virtual_gift_card, user_id)
      @virtual_gift_cards_details = virtual_gift_cards_details
      @virtual_gift_card = virtual_gift_card
      @user = Spree::User.find_by(:id => user_id)
      mail(:to => @virtual_gift_cards_details.recipient_mail, :subject => "SartorNYC Gift Certificate")
    end
  end
end