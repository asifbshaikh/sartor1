module Spree
  class WishlistMailer < BaseMailer

    #default from: 'test@techbulls.com'

    def send_wishlist_to_friend(sender_name, sender_email, recipient_name, recipient_email, options_note, wishlist_id, append_url)
      @sender_name = sender_name
      @sender_email = sender_email
      @recipient_name = recipient_name
      @options_note = options_note
      @wishlist_id = wishlist_id

      @imagepath = Rails.root.to_s + '/app/assets/images/core/header-logo.png'
      attachments.inline['image.png'] = File.read(@imagepath)
      @user_url = Spree::Wishlist.where(user_id: @wishlist_id,is_default: true).first.access_hash
      @link = append_url.to_s + @user_url.to_s + "/"
      subject_text = sender_name + " has shared a wish-list on SartorNYC with you!"
      mail(:from => sender_email,:to => recipient_email, :subject => subject_text)
    end
  end
end
