module Spree
  class ReferralMailer < BaseMailer
    
    #default from: 'test@techbulls.com'

    def send_referral_url_to_friend(sender_name, sender_email, recipient_name, recipient_email, options_note, referral_link)
      @sender_name = sender_name
      @sender_email = sender_email
      @recipient_name = recipient_name
      @options_note = options_note

      @referral_link = referral_link
      subject_text = sender_name + " wants you to shop on SartorNYC!"
      
      mail(:from => sender_email,:to => recipient_email, :subject => subject_text)
    end
  end
end
