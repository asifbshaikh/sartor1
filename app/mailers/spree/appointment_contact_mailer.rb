module Spree
  class AppointmentContactMailer < BaseMailer

    default from: 'info@sartornyc.com',
          to: 'info@sartornyc.com'
  
    def appointment_contact_notification (appointment_contact)
      @appointment_contact = appointment_contact
      mail(subject: "New Sartor NYC appointment contact: #{appointment_contact.name}")
    end
  end
end