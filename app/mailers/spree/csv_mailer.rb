module Spree
  class CsvMailer < BaseMailer

    #default from: 'test@techbulls.com'

    def send_notification_to_admin(csv_error, email, product_path, image_path, submitted_at)
        @csv_error = csv_error
        @csv_path = product_path
        @image_path = image_path
        @email = email
        @submited_at = submitted_at
        mail(:from => "noreply@sartornyc.com", :to => email, :subject => "Status of uploaded products on SartorNYC")
    end
    
    def send_notification_to_admin_orders(csv_error, email, order_path, submitted_at)
        @order_path = order_path
        @submited_at = submitted_at
        @csv_error = csv_error
        mail(:from => "noreply@sartornyc.com", :to => email, :subject => "Status of uploaded Historical Orders on SartorNYC")
    end
    
    
  end
end
