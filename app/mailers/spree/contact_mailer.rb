module Spree
	class Spree::ContactMailer < BaseMailer
	  # default from: "from@example.com"

	    def send_contact_us_data(email,name, message)
	    	to = "info@sartornyc.com"
	    	@email = email
	    	@name = name
	    	@message = message
	      	mail(:to => to, :subject => "Contact Us Mail")
	    end  
	end
end
