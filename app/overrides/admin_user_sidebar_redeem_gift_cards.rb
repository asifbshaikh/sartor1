Deface::Override.new(
  virtual_path: "spree/admin/users/_sidebar",
  name: "admin_user_sidebar_redeem_gift_cards",
  insert_bottom: "[data-hook='admin_user_tab_options']",
  partial: "spree/admin/users/redeem_gift_cards_sidebar",
)
