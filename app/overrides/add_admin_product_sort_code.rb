Deface::Override.new(:virtual_path => "spree/admin/products/_form",
                         :name => "sort_code_field",
                         :insert_bottom => "[data-hook='admin_product_form_left']",
                         :partial => "spree/admin/products/sort_code_field",
                         :disabled => false)
