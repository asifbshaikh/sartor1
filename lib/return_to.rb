module ReturnTo

  def store_referer
    logger.debug("?? storing _return_to: #{params[:_return_to]}")
    if (params[:_return_to])
      session['spree_user_return_to'] = params[:_return_to]
    end
  end

end
