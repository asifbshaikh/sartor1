class AddManualOrderToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :manual_order, :boolean, :default => false
  end
end
