class AddButtonCodeToShirtConfiguration < ActiveRecord::Migration
  def change
    add_column :shirt_configurations, :button_code, :integer
  end
end
