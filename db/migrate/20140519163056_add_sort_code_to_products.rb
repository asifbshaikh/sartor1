class AddSortCodeToProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :sort_code, :string
  end
end
