class AddTaxAmountToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :tax_amount, :decimal, precision: 8, scale: 5
  end
end
