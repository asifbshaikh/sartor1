class CreateDetailedProfiles < ActiveRecord::Migration
  def change
    create_table :detailed_profiles do |t|
      t.decimal :neck
      t.decimal :sleeve
      t.decimal :yoke
      t.decimal :chest
      t.decimal :waist
      t.decimal :hip
      t.decimal :length
      t.decimal :bicep
      t.decimal :wrist
      t.decimal :length_untucked
      t.decimal :wrist_watch
      t.string :watch_hand
      
      t.integer :default_fit_code
      t.integer :default_length_code
      
      t.timestamps
    end
  end
end
