class AddProfileIdToSpreeWishedProducts < ActiveRecord::Migration
  def change
    add_column :spree_wished_products, :profile_id, :integer
  end
end
