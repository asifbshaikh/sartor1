class RemoveTaxRateFromOrders < ActiveRecord::Migration
  def up
    remove_column :spree_orders, :tax_rate
  end
  
  def down
    add_column :spree_orders, :tax_rate, :decimal, precision: 8, scale: 5
  end
end
