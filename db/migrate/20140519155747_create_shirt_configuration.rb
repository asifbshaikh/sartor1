class CreateShirtConfiguration < ActiveRecord::Migration
  def change
    create_table :shirt_configurations do |t|
      t.integer :line_item_id
      t.integer :product_id
      t.integer :collar_code
      t.integer :cuff_code
      t.integer :pocket_code
      t.integer :placket_code
      t.integer :fit_code
      t.integer :length_code
      t.text :comment
    end
  end
end
