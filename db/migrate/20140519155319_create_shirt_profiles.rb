class CreateShirtProfiles < ActiveRecord::Migration
  def change
    create_table :shirt_profiles do |t|
      t.decimal :neck
      t.decimal :sleeve
      t.decimal :yoke
      t.decimal :chest
      t.decimal :waist
      t.decimal :hip
      t.decimal :length
      t.decimal :bicep
      t.decimal :wrist
      t.timestamps
    end
  end
end
