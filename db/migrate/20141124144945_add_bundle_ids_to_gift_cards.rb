class AddBundleIdsToGiftCards < ActiveRecord::Migration
  def change
    add_column :spree_gift_cards, :tie_variant_id, :integer
    add_column :spree_gift_cards, :partner_gift_cert_variant_id, :integer
  end
end
