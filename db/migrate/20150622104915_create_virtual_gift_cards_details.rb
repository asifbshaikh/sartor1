class CreateVirtualGiftCardsDetails < ActiveRecord::Migration
  def up
    create_table :virtual_gift_cards_details do |t|
      t.decimal :amount, :precision => 5, :scale => 2
      t.integer :purchaser_id
      t.string :recipient_name
      t.string :recipient_mail
      t.text :email_message
      t.integer :line_item_id
      t.timestamps
    end
  end
  
  def down
    drop_table :virtual_gift_cards_details
  end
end 
