# This migration comes from spree_store_credits (originally 20140523190528)
class CreateSpreeStoreCreditCategories < ActiveRecord::Migration
  def change
    create_table :spree_store_credit_categories do |t|
      t.string :name
      t.timestamps
    end
  end
end
