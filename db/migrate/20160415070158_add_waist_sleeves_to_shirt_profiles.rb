class AddWaistSleevesToShirtProfiles < ActiveRecord::Migration
  def change
    add_column :shirt_profiles, :waist_hips_code, :integer
    add_column :shirt_profiles, :chest_sleeves_code, :integer
  end
end
