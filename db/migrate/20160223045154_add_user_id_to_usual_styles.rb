class AddUserIdToUsualStyles < ActiveRecord::Migration
  def change
    add_column :usual_styles, :user_id, :integer
  end
end
