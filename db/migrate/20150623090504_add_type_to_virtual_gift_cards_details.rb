class AddTypeToVirtualGiftCardsDetails < ActiveRecord::Migration
  def change
    add_column :virtual_gift_cards_details, :card_type, :string
  end
end
