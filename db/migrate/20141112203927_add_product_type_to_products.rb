class AddProductTypeToProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :product_type, :string, :limit => 5
  end
end
