# This migration comes from spree_store_credits (originally 20140604214313)
class AddUserIdIndexToStoreCredits < ActiveRecord::Migration
  def change
    add_index :spree_store_credits, :user_id
  end
end
