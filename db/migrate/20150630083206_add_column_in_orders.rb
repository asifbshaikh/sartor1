class AddColumnInOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :imported_at, :datetime
  end
end
