class AddDetailsToUsualStyles < ActiveRecord::Migration
  def change
    add_column :usual_styles, :placket_code, :integer
    add_column :usual_styles, :button_code, :integer
  end
end
