class AddProductDescriptionToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :short_product_description, :text
  end
end
