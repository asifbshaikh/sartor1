class AllowNullGiftCardEmailCode < ActiveRecord::Migration
  def up
    change_column :spree_gift_cards, :email, :string, :null => true
    change_column :spree_gift_cards, :code, :string, :null => true
  end

  def down
    change_column :spree_gift_cards, :email, :string, :null => false
    change_column :spree_gift_cards, :code, :string, :null => false
  end
end
