class AddWishedProductIdToShirtConfiguration < ActiveRecord::Migration
  def change
  	add_column :shirt_configurations, :wished_product_id, :integer
  end
end
