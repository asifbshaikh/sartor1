class AddPaymentKnownToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :payment_known, :boolean, :default => true
  end
end
