class AddShippingAddressToGiftCards < ActiveRecord::Migration
  def change
    add_column :virtual_gift_cards_details, :ship_address_id, :integer
  end
end
