# This migration comes from spree_store_credits (originally 20140611215057)
class AddDeletedAtToStoreCreditEvents < ActiveRecord::Migration
  def change
    add_column :spree_store_credit_events, :deleted_at, :datetime
  end
end
