class AddProfileToLineItems < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :profile_id, :integer
  end
end
