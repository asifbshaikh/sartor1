class AddNotesSpreeLineItems < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :note, :string
  end
end
