class AddColumnsToShirtProfiles < ActiveRecord::Migration
  def change
    add_column :shirt_profiles, :classification_code, :integer
    add_column :shirt_profiles, :length_code, :integer  	
  end
end
