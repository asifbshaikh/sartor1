# This migration comes from spree_store_credits (originally 20140528145853)
class AddCurrencyToStoreCredits < ActiveRecord::Migration
  def change
    add_column :spree_store_credits, :currency, :string
  end
end
