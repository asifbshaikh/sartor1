class AddIsAvailableToProducts < ActiveRecord::Migration
  def change
  	add_column :spree_products, :is_available, :boolean, default: true
  end
end
