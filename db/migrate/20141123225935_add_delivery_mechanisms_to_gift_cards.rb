class AddDeliveryMechanismsToGiftCards < ActiveRecord::Migration
  def change
    add_column :spree_gift_cards, :send_on, :string
    add_column :spree_gift_cards, :send_on_date, :date
    add_column :spree_gift_cards, :delivery_type, :string
  end
end
