class ModifyVirtualGiftCardsDetailsColumn < ActiveRecord::Migration
  def change
  	change_column :virtual_gift_cards_details, :amount, :decimal, :precision => 6, :scale => 2
  end
end
