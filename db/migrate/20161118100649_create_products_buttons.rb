class CreateProductsButtons < ActiveRecord::Migration
  def change
    create_table :products_buttons do |t|
      t.integer :product_id
      t.integer :button_id

      t.timestamps
    end
  end
end
