class AddOfflineOrderdate < ActiveRecord::Migration
  def change
    add_column :spree_orders, :offline_order_date, :datetime 
  end
end
