class CreateUsualStyles < ActiveRecord::Migration
  def change
    create_table :usual_styles do |t|
      t.integer :collar_code
      t.integer :cuff_code
      t.integer :pocket_code
      t.integer :fit_code
      t.integer :length_code
    end
  end
end
