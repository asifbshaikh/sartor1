class AddJobNumberToLineItems < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :job_number, :string
  end
end
