class AddIsIndexProductToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :is_index_product, :int
  end
end
