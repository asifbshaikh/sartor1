class AddParentLineItemIdToLineItems < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :parent_lineitem_id, :integer
    add_index :spree_line_items, :parent_lineitem_id
  end
end
