class AddColumnsToWishedProducts < ActiveRecord::Migration
  def change
    add_column :spree_wished_products, :collar_code, :integer
    add_column :spree_wished_products, :cuff_code, :integer
    add_column :spree_wished_products, :pocket_code, :integer
    add_column :spree_wished_products, :placket_code, :integer
  end
end
