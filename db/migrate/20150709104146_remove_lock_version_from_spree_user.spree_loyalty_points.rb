# This migration comes from spree_loyalty_points (originally 20150709103501)
class RemoveLockVersionFromSpreeUser < ActiveRecord::Migration
  def change
  	remove_column :spree_users, :lock_version, :integer
  end
end
