class CreateFabricDetail < ActiveRecord::Migration
  def change
    create_table :fabric_details do |t|
      t.integer :product_id
      t.integer :weight
      t.integer :opacity
      t.integer :easy_of_care
      t.string :wave
      t.string :yarn_count
    end
  end
end
