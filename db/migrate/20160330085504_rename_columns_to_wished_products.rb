class RenameColumnsToWishedProducts < ActiveRecord::Migration
  def change
    rename_column :spree_wished_products, :placket_code, :length_code
  end
end
