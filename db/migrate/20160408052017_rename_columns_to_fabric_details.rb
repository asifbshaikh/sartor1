class RenameColumnsToFabricDetails < ActiveRecord::Migration
  def change
    rename_column :fabric_details, :easy_of_care, :ease_of_care
    rename_column :fabric_details, :wave, :weave_type
  end
end
