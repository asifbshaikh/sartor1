class AddStatusToStockLocations < ActiveRecord::Migration
  def change
    add_column :spree_stock_locations, :status, :string
  end
end
