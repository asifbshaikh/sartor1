class ModifySpreeProductColumn < ActiveRecord::Migration
  def up
    change_column :spree_products, :product_type, :string, :limit => 15
  end
  
  def down
    change_column :spree_products, :product_type, :string, :limit => 15
  end
  
end
