class AddInviteSentAtToUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :invite_sent_at, :datetime
  end
end
