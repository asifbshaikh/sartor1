class CreateAppointmentContact < ActiveRecord::Migration
  def change
    create_table :spree_appointment_contacts do |t|
      t.string :email
      t.string :phone
      t.string :message
      t.string :name
      t.string :location_type
      t.string :address
      t.timestamps
    end
  end
end
