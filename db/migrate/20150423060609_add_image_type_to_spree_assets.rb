class AddImageTypeToSpreeAssets < ActiveRecord::Migration
  def change
    add_column :spree_assets, :image_type, :string, :default => 'other'
  end
end
