# This migration comes from spree_tax_cloud (originally 20140122165618)
class AddTaxCloudResponseAtToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :tax_cloud_response_at, :datetime
  end
end

