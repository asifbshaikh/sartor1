class AddShippableToSpreeStockLocations < ActiveRecord::Migration
  def change
    add_column :spree_stock_locations, :shippable, :boolean, :default => true
  end
end
