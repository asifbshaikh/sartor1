class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.boolean :is_default
      
      t.integer :heir_id
      t.string :heir_type

      t.string :name
      t.integer :height_ft
      t.integer :height_in
      t.integer :weight
      
      t.text :embroidery_name

      t.timestamps
    end
  end
end
