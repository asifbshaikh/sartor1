# This migration comes from spree_store_credits (originally 20140812120911)
class AddOriginatorToStoreCredits < ActiveRecord::Migration
  def change
    add_column :spree_store_credit_events, :originator_id, :integer
    add_column :spree_store_credit_events, :originator_type, :string
  end
end
