class AddProfileIdToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :profile_id, :integer
  end
end
