# This migration comes from spree_store_credits (originally 20140617144038)
class RenameCreditTypes < ActiveRecord::Migration
  def change
    default_type = Spree::StoreCreditType.where(name: 'Promotional').first
    default_type.update_column(:name, 'Expiring')
    Spree::StoreCreditType.create(name: 'Non-expiring', priority: 2)
  end
end
