# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#Spree::Core::Engine.load_seed if defined?(Spree::Core)
#Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

SpreeStoreCredits::Engine.load_seed if defined?(SpreeStoreCredits::Engine)

puts "Enter Promotional Store Credit Amount:-"
amount = STDIN.gets.chomp
users = Spree::User.all
users.each do |u|
  if !Spree::StoreCredit.where(:user_id => u.id).present?
   Spree::StoreCredit.create(user_id: u.id, category_id: 1, created_by_id: 1, amount: amount, amount_used: 0, memo: "Promotional Store Credit", currency: "USD", amount_authorized: 0, type_id: 4).save
  end
end
puts "Promotional Store Credit Added"

