require 'test_helper'

class Detailed_ProfileTest < ActiveSupport::TestCase

  test "40 Narrow Waist (1) Slim" do
    profile = detailed_profiles(:one)
    profile.Body_Profile_Unit = "inches"
    profile.Generator_Fit = "slim"

    assert_equal(16.5, profile.shirt_collar, "Collar mismatch")
    assert_equal(19, profile.shirt_shoulder, "Shoulder mismatch")
    assert_equal(25.75, profile.shirt_sleeve, "Sleeve mismatch")

    assert_equal(43.75, profile.shirt_chest, "Chest mismatch")
    assert_equal(38, profile.shirt_waist, "Waist mismatch")
    assert_equal(42, profile.shirt_hip, "Hip mismatch")

    assert_equal(15.25, profile.shirt_bicep, "Bicep mismatch")
    assert_equal(8.5, profile.shirt_cuff, "Cuff mismatch")
    assert_equal(29, profile.shirt_length, "Length mismatch")
  end

  test "40 Regular Waist (2) Slim" do
    profile = detailed_profiles(:two)
    profile.Body_Profile_Unit = "inches"
    profile.Generator_Fit = "slim"

    assert_equal(43.75, profile.shirt_chest, "Chest mismatch")
    assert_equal(39.75, profile.shirt_waist, "Waist mismatch")
    assert_equal(42.75, profile.shirt_hip, "Hip mismatch")

  end

  test "Shirt Chest Extra Slim" do
    profile = detailed_profiles(:one)
    profile.Body_Profile_Unit = "inches"
    profile.Generator_Fit = "extra slim"
    assert_equal(43, profile.shirt_chest)
  end

end
