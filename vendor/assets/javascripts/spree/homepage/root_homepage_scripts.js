// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require spree/homepage/js-plugin/respond/respond.min.js
// require spree/homepage/js-plugin/jquery/1.8.3/jquery.min.js

//= require jquery_ujs
// require spree/homepage/js-plugin/jquery-ui/jquery-ui-1.8.23.custom.min.js 

//= require bootstrap/transition
//= require bootstrap/alert
//= require bootstrap/button
// require bootstrap/carousel
//= require bootstrap/collapse
// require bootstrap/modal
// require bootstrap/tooltip
// require bootstrap/popover
//= require bootstrap/scrollspy
// require bootstrap/tab
//= require bootstrap/affix

// require spree/homepage/js-plugin/easing/jquery.easing.1.3.js 
// require spree/homepage/js-plugin/flexslider/jquery.flexslider-min.js 
// require spree/homepage/js-plugin/isotope/jquery.isotope.js 
// require spree/homepage/js-plugin/neko-contact-ajax-plugin/js/jquery.form.js 
// require spree/homepage/js-plugin/neko-contact-ajax-plugin/js/jquery.validate.min.js 
// require spree/homepage/js-plugin/magnific-popup/jquery.magnific-popup.min.js 
//= require spree/homepage/js-plugin/parallax/js/jquery.scrollTo-1.4.3.1-min.js 
//= require spree/homepage/js-plugin/parallax/js/jquery.localscroll-1.2.7-min.js 
//= require spree/homepage/js-plugin/parallax/js/jquery.stellar.min.js 

// require spree/homepage/js-plugin/pageSlide/jquery.pageslide-custom.js 
//= require spree/homepage/js-plugin/jquery.sharrre-1.3.4/jquery.sharrre-1.3.4.min.js 

// require spree/homepage/js-plugin/owl.carousel/owl-carousel/owl.carousel.min.js 

//= require spree/homepage/custom.js 