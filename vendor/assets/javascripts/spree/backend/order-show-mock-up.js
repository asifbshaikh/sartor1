//mock-up page ajax
$(document).ready(function () {

  $('#save_jobs').click(function() {
    saveJobsToLineItems();
  });

  $('#update-status').click(function() {
    updateShirtStatus();
  });

  $('#show-shirt-details').click(function() {
    getShirtsToShow();
    return false;
  });

  $('a.add-tracking-number').click(function(){
    var track = $(this);
    var shipment_number_for_tracking = track.data('shipment-number');
    addTrackingNumberToShipment(shipment_number_for_tracking)
    return false;
  });

  $(this).find('*[class*="line_item_details_"]').hide();

  var select_all = $("#select_all_checkbox").click(function () {
		  if(select_all.is(':checked')){
			  $('#show-mock-up-table tr').each(function() {
				   $(this).find("input[type*='checkbox']").prop('checked',true);
			   });
		  }
		  else{
			  $('#show-mock-up-table tr').each(function() {
				   $(this).find("input[type*='checkbox']").prop('checked',false);
			   });
		  }
	});

});

saveJobsToLineItems = function () {
  var order_id = $('#order-id').val();
  var jobs = {};
  var job_empty = true;
  $('#show-mock-up-table tr').each(function() {
    id = $(this).find("input[name*='job']").attr('id');
    job_value = $(this).find("input[name*='job']").val();
    if (job_value != "" && job_value != undefined && job_value != null){
      var splitId = id.split("_");
      jobs[splitId[1]] = job_value;
      job_empty = false;
    }
  });
  if (job_empty == true){
    alert("You didn't assign any new job.");
  }else{
    $.ajax({
        type: "PUT",
        url: '/admin/orders/'+ order_id +'/save-jobs',
        data: { jobs: jobs }
      }).done(function( msg ) {
        window.location.reload();
      });
  }
}

getShirtsToShow = function () {
  var checkboxMap = {};
  $('#show-mock-up-table tr').each(function() {
    var id = $(this).find("input[name*='checkbox']").attr('id');
    if (id != undefined) {
      var splitId = id.split("_");
      if ($(this).find("input[name*='checkbox']").is(":checked")){
        checkboxMap[splitId[1]] = true;
      }else{
        checkboxMap[splitId[1]] = false;
      }
    }
  });
  showShirtDetails(checkboxMap);
}

showShirtDetails = function (checkboxMap) {
  for (var key in checkboxMap) {
    if (checkboxMap[key] == true) {
      $('.line_item_details_' + key).show();
    }else {
      $('.line_item_details_' + key).hide();
    }
  }
}

updateShirtStatus = function () {
  var checkboxMap = {};
  var shirtSelected = false;
  var order_id = $('#order-id').val();
  var new_stock_location_id = $('#shipment_stock_location').val();
  var shipped_item_present = false;
  $('#show-mock-up-table tr').each(function() {
    var id = $(this).find("input[name*='checkbox']").attr('id');
    var inventory_unit_state = $(this).find("input[name*='inventory-unit-state']").val();
    console.log("inventory_unit_state: " + inventory_unit_state);

    if (id != undefined) {
      var splitId = id.split("_");
      if ($(this).find("input[name*='checkbox']").is(":checked")){
        if (inventory_unit_state == "shipped") {
          shipped_item_present = true;
        }
        checkboxMap[splitId[1]] = true;
        shirtSelected = true;
      }else{
        checkboxMap[splitId[1]] = false;
      }
    }
  });
  if (shirtSelected == true && shipped_item_present == false){
    $.ajax({
        type: "PUT",
        url: '/admin/orders/'+ order_id +'/update-shirt-status',
        data: { checkboxMap: checkboxMap, new_stock_location_id: new_stock_location_id }
      }).done(function( msg ) {
        window.location.reload();
      });
    } else{
      if (shirtSelected == false)
        alert("No shirt was selected. Try again.");
      if (shipped_item_present == true)
        alert("At least one of the shirts you selected is already shipped. Try again.");
   }
}

addTrackingNumberToShipment = function (shipment_number_for_tracking) {
  var order_id = $('#order-id').val();
  var shipment_number = shipment_number_for_tracking;
  var input_id = "#tracking-number-" + shipment_number;
  var tracking_number = $(input_id).val();

  if(tracking_number != undefined && tracking_number != ""){
    $.ajax({
        type: "PUT",
        url: '/admin/orders/'+ order_id +'/add-tracking-number',
        data: { shipment_number: shipment_number, tracking_number:tracking_number }
      }).done(function( msg ) {
        window.location.reload();
      });
  }else{
    alert("Please enter valid trakcing number");
  }

}

