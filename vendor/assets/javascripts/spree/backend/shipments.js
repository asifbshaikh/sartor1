// Shipments AJAX API
$(document).ready(function () {
  'use strict';

  $("a.add-adjustment").click(function(){
    var add_adjustments = $(this);
    var order_number = add_adjustments.data('order-number');
    addNewAdjustmentToOrder(order_number);
    return false;
  });

  //handle save click
  $('a.save-item').unbind('click').click(function(){
    var save = $(this);
    var shipment_number = save.data('shipment-number');
    var variant_id = save.data('variant-id');
    var line_item_id = save.data('line-item-id');
    var line_item_price = save.data('line-item-price');
    var quantity = parseInt(save.parents('tr').find('input.line_item_quantity').val());
    var price = parseInt(save.parents('tr').find('input.line_item_price').val());
    var note =  $('#notes_' + line_item_id).val();
    var job_number = $('#job_number' + line_item_id).val();

    //shirt-configuration properties
    var shirt_configuration = {}
    shirt_configuration["collar_id"] = $( "#item_collar_" + line_item_id + " option:selected" ).val();
    shirt_configuration["cuff_id"] = $( "#item_cuff_" + line_item_id + " option:selected" ).val();
    shirt_configuration["pocket_id"] = $( "#item_pocket_" + line_item_id + " option:selected" ).val();
    shirt_configuration["fit_id"] = $( "#item_fit_" + line_item_id + " option:selected" ).val();
    shirt_configuration["length_id"] = $( "#item_length_" + line_item_id + " option:selected" ).val();
    shirt_configuration["button_id"] = $("#item_button_" + line_item_id + " option:selected" ).val()
    shirt_configuration["placket_id"] = $( "#item_placket_" + line_item_id + " option:selected" ).val();
    toggleItemEdit();
    adjustShipmentItems(shipment_number, variant_id, quantity, line_item_id, line_item_price, price, note, shirt_configuration, job_number);
    return false;
  });


    //handle delete click
  $('a.delete-item').unbind('click').click(function(){
    if (confirm(Spree.translations.are_you_sure_delete)) {
      var del = $(this);
      var shipment_number = del.data('shipment-number');
      var variant_id = del.data('variant-id');
      var line_item_id = del.data('line-item-id');

      toggleItemEdit();
      adjustShipmentItems(shipment_number, variant_id, 0, line_item_id);
    }
  });

});

adjustShipmentItems = function(shipment_number, variant_id, quantity, line_item_id, line_item_price, price, note, shirt_configuration, job_number){
  var shipment = _.findWhere(shipments, {number: shipment_number + ''});
  var inventory_units = _.where(shipment.inventory_units, {variant_id: variant_id});

  var url = Spree.routes.shipments_api + "/" + shipment_number;
  var new_quantity = 0;
  if(inventory_units.length<quantity){
    url += "/add"
    new_quantity = (quantity - inventory_units.length);
  }else if(inventory_units.length>quantity){
    url += "/delete_line_item"
    new_quantity = (inventory_units.length - quantity);
  }else{
      url += "/update_item"

  }
  url += '.json';
  if(new_quantity!=0){
    $.ajax({
      type: "PUT",
      url: Spree.url(url),
      data: { variant_id: variant_id, quantity: new_quantity, line_item_id: line_item_id}
    }).done(function( msg ) {
      window.location.reload();
    });
  }else{
      $.ajax({
      type: "PUT",
      url: Spree.url(url),
      data: { variant_id: variant_id, price: price, line_item_id: line_item_id, note: note, shirt_configuration: shirt_configuration, job_number: job_number}
    }).done(function( msg ) {
      window.location.reload();
    });
  }
}

toggleItemEdit = function(){
  var link = $(this);
  link.parent().find('a.edit-item').toggle();
  link.parent().find('a.cancel-item').toggle();
  link.parent().find('a.split-item').toggle();
  link.parent().find('a.save-item').toggle();
  link.parent().find('a.delete-item').toggle();
  link.parent().find('a.add-notes').toggle();
  link.parents('tr').find('td.item-price-show').toggle();
  link.parents('tr').find('td.item-price-edit').toggle();
  link.parents('tr').children('td').find('div.job-number-show').toggle();
  link.parents('tr').children('td').find('div.job-number-edit').toggle();

  link.closest('tr').next().children('td').children('table').children('tbody').children('tr').find('td.item-note-show').toggle();
  link.closest('tr').next().children('td').children('table').children('tbody').children('tr').find('td.item-note-edit').toggle();

  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-collar-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-collar-edit').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-cuff-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-cuff-edit').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-pocket-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-pocket-edit').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-length-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-length-edit').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-fit-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-fit-edit').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-button-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-button-edit').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-placket-show').toggle();
  link.parents('tr').children('td').children('table').children('tbody').children('tr').find('td.shirt-placket-edit').toggle();
  return false;
}

addVariantFromStockLocation = function() {
  $('#stock_details').hide();

  var variant_id = $('input.variant_autocomplete').val();
  var stock_location_id = $(this).data('stock-location-id');
  var quantity = $("input.quantity[data-stock-location-id='" + stock_location_id + "']").val();

  //shirt-configuration properties
  var shirt_configuration = {}
  shirt_configuration["collar_id"] = $( "#collar option:selected" ).val();
  shirt_configuration["cuff_id"] = $( "#cuff option:selected" ).val();
  shirt_configuration["pocket_id"] = $( "#pocket option:selected" ).val();
  shirt_configuration["button_id"] = $("#button option:selected" ).val()
  shirt_configuration["placket_id"] = $( "#placket option:selected" ).val();
  var variant_price =  $("#add_variant_price ").val();
  var variant_note = $("#add_variant_note ").val();

  var shipment = _.find(shipments, function(shipment){
    return shipment.stock_location_id == stock_location_id && (shipment.state == 'ready' || shipment.state == 'pending');
  });

  if(shipment==undefined){
    $.ajax({
      type: "POST",
      async: false,
      url: Spree.url(Spree.routes.shipments_api + "?shipment[order_id]=" + order_number),
      data: { variant_id: variant_id, quantity: quantity, stock_location_id: stock_location_id, shirt_configuration: shirt_configuration, variant_price: variant_price, variant_note: variant_note }
    }).done(function( msg ) {
      window.location.reload();
    }).error(function( msg ) {
    });
  }else{
    //add to existing shipment
    addNewItemsToOrder(shipment.number, variant_id, quantity, shirt_configuration, variant_price, variant_note);
  }
  return 1
}

addNewItemsToOrder = function(shipment_number, variant_id, quantity, shirt_configuration, variant_price, variant_note){
    var shipment = _.findWhere(shipments, {number: shipment_number + ''});
    var url = Spree.routes.shipments_api + "/" + shipment_number;
    url += "/add"
    url += '.json';
    if(quantity>0){
      $.ajax({
        type: "PUT",
        async: false,
        url: Spree.url(url),
        data: { variant_id: variant_id, quantity: quantity, shirt_configuration: shirt_configuration, variant_price: variant_price, variant_note: variant_note}
      }).done(function( msg ) {
        window.location.reload();
      });
    }
}

addNewAdjustmentToOrder = function(order_number){
  var adjustment = {}
  adjustment["amount"] = $("#adjustment_amount").val();
  adjustment["label"] = $("#adjustment_text").val();
  var url = "/admin/orders/" + order_number + "/adjustments"

  $.ajax({
   type: "POST",
    url: url,
    data: { adjustment: adjustment}
  }).done(function( msg ) {
    window.location.reload();
  });
}

toggleTrackingEdit = function(event){
  event.preventDefault();
  var link = $(this);
  link.parents('tbody').find('tr.edit-tracking').toggle();
  link.parents('tbody').find('tr.show-tracking').toggle();
}

toggleMethodEdit = function(event){
  event.preventDefault();
  var link = $(this);
  link.parents('tbody').find('tr.edit-method').toggle();
  link.parents('tbody').find('tr.show-method').toggle();
}

startItemSplit = function(event){
  event.preventDefault();
  var link = $(this);
  link.parent().find('a.edit-item').toggle();
  link.parent().find('a.split-item').toggle();
  link.parent().find('a.delete-item').toggle();
  var variant_id = link.data('variant-id');

  var variant = {};
  $.ajax({
    type: "GET",
    async: false,
    url: Spree.url(Spree.routes.variants_api),
    data: {
      q: {
        "id_eq": variant_id
      }
    }
  }).success(function( data ) {
    variant = data['variants'][0];
  }).error(function( msg ) {
  });

  var max_quantity = link.closest('tr').data('item-quantity');
  var price = link.closest('tr').data('item-total-price');

  var collar_id = link.closest('tr').data('item-collar-id');
  var cuff_id = link.closest('tr').data('item-cuff-id');
  var pocket_id = link.closest('tr').data('item-pocket-id');
  var fit_id = link.closest('tr').data('item-fit-id');
  var length_id = link.closest('tr').data('item-length-id');
  var button_id = link.closest('tr').data('item-button-id');
  var placket_id = link.closest('tr').data('item-placket-id');
  var note = link.closest('tr').data('item-note');
  var profile_id = link.closest('tr').data('item-profile-id');
  var line_item_id = link.closest('tr').data('item-line-item-id');
  var parent_line_item_id = link.closest('tr').data('item-parent-line-item-id');

  //find this template in app-views-spree-admin-variants-split.js.erb
  //Just like spree, I am fetching values from shipment_manifest view and passing it to template
  //Though we need to find a better way to do this.
  var split_item_template = Handlebars.compile($('#variant_split_template').text());
  link.closest('tr').after(split_item_template({ variant: variant, shipments: shipments, max_quantity: max_quantity }));

  $('a.cancel-split').click(cancelItemSplit);
  //$('a.save-split').click(completeItemSplit);

  $('a.save-split').click(function(){
    completeItemSplit($(this), price, collar_id, cuff_id, pocket_id, placket_id , button_id , length_id , note, profile_id, line_item_id, parent_line_item_id);
    return false;
  });

   // Add some tips
  $('.with-tip').powerTip({
    smartPlacement: true,
    fadeInTime: 50,
    fadeOutTime: 50,
    intentPollInterval: 300
  });
  $('#item_stock_location').select2({ width: 'resolve', placeholder: Spree.translations.item_stock_placeholder });
}

completeItemSplit = function(nodeSaveSplit, price, collar_id, cuff_id, pocket_id, length_id ,fit_id, placket_id, button_id, note, profile_id, line_item_id, parent_line_item_id) {
  //event.preventDefault();

  var link = nodeSaveSplit;
  var order_number = link.closest('tbody').data('order-number');
  var stock_item_row = link.closest('tr');
  var variant_id = stock_item_row.data('variant-id');

  var variant_price = price;
  var collar_id = collar_id;
  var cuff_id = cuff_id;
  var pocket_id = pocket_id;
  var length_id  = length_id;
  var fit_id = fit_id;
  var button_id = button_id;
  var placket_id = placket_id;
  var variant_note = note;
  var profile_id = profile_id;
  var line_item_id = line_item_id;
  var parent_line_item_id = parent_line_item_id;

  //shirt-configuration properties
  var shirt_configuration = {}
  shirt_configuration["collar_id"] = collar_id;
  shirt_configuration["cuff_id"] = cuff_id;
  shirt_configuration["pocket_id"] = pocket_id;
  shirt_configuration["length_id"] = length_id;
  shirt_configuration["fit_id"] = fit_id;
  shirt_configuration["button_id"] = button_id;
  shirt_configuration["placket_id"] = placket_id;
  //Quantity is hardcoded to 1 to avoid complications for now, might need to revisit this.
  var quantity = 1;

  var stock_location_id = stock_item_row.find('#item_stock_location').val();
  var original_shipment_number = link.closest('tbody').data('shipment-number');
  var selected_shipment = stock_item_row.find($('#item_stock_location').select2('data').element);
  var target_shipment_number = selected_shipment.data('shipment-number');
  var new_shipment = selected_shipment.data('new-shipment');

  if (original_shipment_number != target_shipment_number) {
    // first remove item(s) from original shipment
    $.ajax({
      type: "PUT",
      async: false,
      url: Spree.url(Spree.routes.shipments_api + "/" + original_shipment_number + "/delete_line_item.json"),
      data: { variant_id: variant_id, quantity: quantity, line_item_id: line_item_id }
    });

    if (new_shipment != undefined) {
      $.ajax({
        type: "POST",
        async: false,
        url: Spree.url(Spree.routes.shipments_api + "?shipment[order_id]=" + order_number),
        data: { variant_id: variant_id, quantity: quantity, stock_location_id: stock_location_id, variant_price: variant_price, variant_note: variant_note, shirt_configuration: shirt_configuration, profile_id: profile_id, line_item_id: line_item_id, parent_line_item_id: parent_line_item_id }
      }).done(function(msg) {
        advanceOrder();
      });
    } else {
      $.ajax({
        type: "PUT",
        async: false,
        url: Spree.url(Spree.routes.shipments_api + "/" + target_shipment_number + "/add.json"),
        data: { variant_id: variant_id, quantity: quantity, variant_price: variant_price, variant_note: variant_note, shirt_configuration: shirt_configuration, profile_id: profile_id, line_item_id: line_item_id, parent_line_item_id: parent_line_item_id }
      }).done(function(msg) {
        advanceOrder();
      });
    }
  }
}
