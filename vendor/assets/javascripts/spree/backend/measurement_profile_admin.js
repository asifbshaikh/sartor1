function adminCreateShirt()
{
profile_id = $('#detailedProfile').val();
chest_sleeves = $('#chest_sleeves').val();
waist_hips = $('#waist_hips').val();
length = $('#lengths').val();
classification = $('#classification').val();
$.ajax({
        type: "POST",
        url: '/admin/admin-get-measurements',
        data: { profile:  profile_id ,classification: classification,length: length,waist_hips: waist_hips,chest_sleeves: chest_sleeves}}).success(function( msg ) {
        /*console.log(JSON.stringify(msg))*/
        /*neck*/
        $('#profile_neck').val(msg.generated_shirt.neck)
        $("#neck").text(msg.shirt_profile.neck);
        $("#ease_neck").text(msg.ease.neck);
        /*shoulder*/
        $('#profile_yoke').val(msg.generated_shirt.yoke)
        $("#yoke").text(msg.shirt_profile.yoke);
        $("#ease_yoke").text(msg.ease.yoke);
        /*cuff*/
        $('#profile_cuff').val(msg.generated_shirt.sleeve)
        $("#cuff").text(msg.shirt_profile.sleeve);
        $("#ease_cuff").text(msg.ease.sleeve);
        /*bicep*/
        $('#profile_bicep').val(msg.generated_shirt.bicep)
        $("#bicep").text(msg.shirt_profile.bicep);
        $("#ease_bicep").text(msg.ease.bicep);
        /*wrist*/
        $('#profile_wrist').val(msg.generated_shirt.wrist)
        $("#wrist").text(msg.shirt_profile.wrist);
        $("#ease_wrist").text(msg.ease.wrist);
        /*chest*/
        $('#profile_chest').val(msg.generated_shirt.chest)
        $("#chest").text(msg.shirt_profile.chest);
        $("#ease_chest").text(msg.ease.chest);
        /*waist*/
        $('#profile_waist').val(msg.generated_shirt.waist)
        $("#waist").text(msg.shirt_profile.waist);
        $("#ease_waist").text(msg.ease.waist);
        /*hip*/
        $('#profile_hip').val(msg.generated_shirt.hip)
        $("#hip").text(msg.shirt_profile.hip);
        $("#ease_hip").text(msg.ease.hip);
        /*length*/
        $('#profile_length').val(msg.generated_shirt.length)
        $("#length").text(msg.shirt_profile.length);
        $("#ease_length").text(msg.ease.length);
        /*hiddenfields for dropdowns*/
        $('#profile_classification_code').val(msg.classification)
        $('#profile_waist_hips_code').val(msg.length)
        $('#profile_chest_sleeves_code').val(msg.waist_hips)
        $('#profile_length_code').val(msg.chest_sleeves)
        /*on generate click show table */
        $('#generated_profile').show();
        $('#generate-button-to-regenerate').val('regenerate measurements');
      });
}






