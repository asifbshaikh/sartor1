// variant autocompletion

$(document).ready(function() {
  if ($('#variant_autocomplete_template').length > 0) {
    window.variantTemplate = Handlebars.compile($('#variant_autocomplete_template').text());
    window.variantStockTemplate = Handlebars.compile($('#variant_autocomplete_stock_template').text());
    Handlebars.registerHelper('ifCond', function(v1, v2, options) {
      if(v1 === v2) {
        return options.fn(this);
      }
      return options.inverse(this);
    });
  }
});

formatVariantResult = function(variant) {
  if (variant["images"][0] != undefined && variant["images"][0].mini_url != undefined) {
    variant.image = variant.images[0].mini_url
  }
  return variantTemplate({ variant: variant })
}

$.fn.variantAutocomplete = function() {
  this.parent().children(".options_placeholder").prop('id', this.parent().data('index'))
  this.select2({
    placeholder: Spree.translations.variant_placeholder,
    minimumInputLength: 3,
    initSelection: function (element, callback) {
      $.get(Spree.routes.variants_search + '/' + element.val(), {}, function (data) { callback(data) })
    },
    ajax: {
      url: Spree.url(Spree.routes.variants_search),
      datatype: 'json',
      data: function(term, page) {
        return {
          custom_shirt_flag: $('#custom_shirt').is(':checked'),
          q: {
            "product_name_or_sku_cont": term
          }
        }
      },
      results: function (data, page) {
        window.variants = data['variants'];

        return { results: data['variants'] }
      }
    },
    formatResult: formatVariantResult,
    formatSelection: function (variant) {
      $(this.element).parent().children('.options_placeholder').html(variant.options_text)
      return variant.name;
    }
  })
}
