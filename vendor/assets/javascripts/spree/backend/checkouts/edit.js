
//= require_self
$(document).ready(function() {
  if ($('#customer_link_id').val() != undefined ){
      $('#add_profile').attr("href", "/admin/users/"+ $('#customer_link_id').val() +"/shirt_profiles/new");
  }
  if ($('#customer_autocomplete_template').length > 0) {
    window.customerTemplate = Handlebars.compile($('#customer_autocomplete_template').text());
  }

  formatCustomerResult = function(customer) {
    return customerTemplate({
      customer: customer,
      bill_address: customer.bill_address,
      ship_address: customer.ship_address
    })
  }

  if ($("#customer_search").length > 0) {
    $("#customer_search").select2({
      placeholder: Spree.translations.choose_a_customer,
      ajax: {
        url: Spree.routes.user_search,
        datatype: 'json',
        data: function(term, page) {
          return { q: term }
        },
        results: function(data, page) {
          return { results: data }
        }
      },
      dropdownCssClass: 'customer_search',
      formatResult: formatCustomerResult,
      formatSelection: function (customer) {
    	ShowProfile(customer);
        $('#order_email').val(customer.email);
        $('#user_id').val(customer.id);
        $('#guest_checkout_true').prop("checked", false);
        $('#guest_checkout_false').prop("checked", true);
        $('#guest_checkout_false').prop("disabled", false);

        return customer.email;
      }
    })
  }



  ShowProfile = function(customer) {
    var detailprofilecount =0;
    selectProfiles = $('#profile_id')[0];
    selectProfiles.options.length = 0;
     /*console.log("customer="+JSON.stringify(customer));*/
    for (i = 0; i < customer.ShirtProfile.length; i++) {
        if (customer.ShirtProfile[i].heir_type == "ShirtProfile"){
            selectProfiles.options[selectProfiles.options.length] = new Option(customer.ShirtProfile[i].name, customer.ShirtProfile[i].id ,customer.ShirtProfile[i].heir_type)
        }
        if (customer.ShirtProfile[i].heir_type == "DetailedProfile"){
            detailprofilecount++;
        }
    }
    $('#add_profile').attr("href", "/admin/users/"+ customer.id +"/shirt_profiles/new");
    $('#add_profile').addClass("c-pointer");
    $('#add_profile').show();
    $('#add_body_profile').attr("href", "/admin/users/"+ customer.id +"/detailed_profiles/new");
    $('#add_body_profile').addClass("c-pointer");
    if(detailprofilecount <= 0){
        $('#add_profile').hide();
    }

  }

  var payment_unknown_input = $('input#payment_unknown');

  var payment_unknown = function () {
    if (!payment_unknown_input.is(':checked')) {
      $('#tax_rate').show();
      $('.tax_rate_label').show();
      $("#payment-link").show();
    } else {
      $('#tax_rate').hide();
      $('.tax_rate_label').hide();
      $("#payment-link").hide();
    }
  };

  payment_unknown_input.click(function() {
    payment_unknown();
  });

  var order_use_billing_input = $('input#order_use_billing');

  var order_use_billing = function () {
    if (!order_use_billing_input.is(':checked')) {
      $('#shipping').show();
    } else {
      $('#shipping').hide();
    }
  };

  order_use_billing_input.click(function() {
    order_use_billing();
  });

  var skip_address_input = $('input#skip_address');

  var skip_address = function () {
    if (!skip_address_input.is(':checked')) {
      $(".shipping-address-form").show();
      reset_default_shipping_address();
    } else {
    $(".shipping-address-form").hide();
      set_default_shipping_address();
    }
  };


  skip_address_input.click(function() {
	  skip_address();
  });

  if( $("#order_ship_address_attributes_city").val() == "N/A"){
    $('#skip_address').prop('checked',true);
	  $(".shipping-address-form").hide();
  }

  function set_default_shipping_address(){
    $("#order_ship_address_attributes_firstname").val("N/A");
    $("#order_ship_address_attributes_lastname").val("N/A");
    $("#order_ship_address_attributes_address1").val("N/A");
    $("#order_ship_address_attributes_address2").val("N/A");
    $("#order_ship_address_attributes_city").val("N/A");
    $("#order_ship_address_attributes_zipcode").val("90001");
    $("#order_ship_address_attributes_phone").val("1234567890");
  }

  function reset_default_shipping_address(){
    $("#order_ship_address_attributes_firstname").val("");
    $("#order_ship_address_attributes_lastname").val("");
    $("#order_ship_address_attributes_address1").val("");
    $("#order_ship_address_attributes_address2").val("");
    $("#order_ship_address_attributes_city").val("");
    $("#order_ship_address_attributes_zipcode").val("");
    $("#order_ship_address_attributes_phone").val("");
  }

  order_use_billing();

  $('#guest_checkout_true').change(function() {
    $('#customer_search').val("");
    $('#user_id').val("");
    $('#checkout_email').val("");

    var fields = ["firstname", "lastname", "company", "address1", "address2",
              "city", "zipcode", "state_id", "country_id", "phone"]
    $.each(fields, function(i, field) {
      $('#order_bill_address_attributes' + field).val("");
      $('#order_ship_address_attributes' + field).val("");
    })
  });
});
