
//INCREMENT DECREMENT TEXTBOX VALUES FUNCTION
function incrementValue(sender){
    var data = parseFloat($(sender).siblings('input').val());
    data = data + 1;
    $(sender).siblings('input').val(data.toFixed(2));
}
function decrementValue(sender){
   console.log("hey");
    var data = parseFloat($(sender).siblings('input').val());
    data = data - 1;
    if(data >= 1){
      $(sender).siblings('input').val(data.toFixed(2));
    }
    else{
     $(sender).siblings('input').val(0);
    }
}

$(function () {
  $(window).bind("load resize", function() {
      // Minimalize menu when screen is less than 768px
      if ($(document).width() < 769) {
           $('#account_side_bar_main').css('border','none');
      }
      else {
          division_height = $('#divwidth').height();
          $('#account_side_bar_border').css('border-right','1px solid gray');
            if (division_height > $('#account_side_bar_main').height()){
            $('#account_side_bar_main').height(division_height);
            }
      }
    });

  $('#generateshirt').click(function(){
    profile_id = $('#detailedProfile').val();
    chest_sleeves = $('#chest_sleeves').val();
    waist_hips = $('#waist_hips').val();
    length = $('#length').val();
    classification = $('#classification').val();
    //console.log(profile_id+chest_sleeves+waist_hips+length+classification);
    $.ajax({
            type: "POST",
            url: '/generate-measurements',
            dataType: "json",
            data: { profile:  profile_id ,classification: classification,length: length,waist_hips: waist_hips,chest_sleeves: chest_sleeves}}).success(function( msg ) {
            //console.log(JSON.stringify(msg));
            $('#profile_neck').val(msg.generated_shirt.neck)
            $("#neck").text(msg.shirt_profile.neck);
            $("#ease_neck").text(msg.ease.neck);
            /*shoulder*/
            $('#profile_yoke').val(msg.generated_shirt.yoke)
            $("#yoke").text(msg.shirt_profile.yoke);
            $("#ease_yoke").text(msg.ease.yoke);
            /*cuff*/
            $('#profile_cuff').val(msg.generated_shirt.sleeve)
            $("#cuff").text(msg.shirt_profile.sleeve);
            $("#ease_cuff").text(msg.ease.sleeve);
            /*bicep*/
            $('#profile_bicep').val(msg.generated_shirt.bicep)
            $("#bicep").text(msg.shirt_profile.bicep);
            $("#ease_bicep").text(msg.ease.bicep);
            /*wrist*/
            $('#profile_wrist').val(msg.generated_shirt.wrist)
            $("#wrist").text(msg.shirt_profile.wrist);
            $("#ease_wrist").text(msg.ease.wrist);
            /*chest*/
            $('#profile_chest').val(msg.generated_shirt.chest)
            $("#chest").text(msg.shirt_profile.chest);
            $("#ease_chest").text(msg.ease.chest);
            /*waist*/
            $('#profile_waist').val(msg.generated_shirt.waist)
            $("#waist").text(msg.shirt_profile.waist);
            $("#ease_waist").text(msg.ease.waist);
            /*hip*/
            $('#profile_hip').val(msg.generated_shirt.hip)
            $("#hip").text(msg.shirt_profile.hip);
            $("#ease_hip").text(msg.ease.hip);
            /*length*/
            $('#profile_length').val(msg.generated_shirt.length)
            $("#lengths").text(msg.shirt_profile.length);
            $("#ease_length").text(msg.ease.length);
            /*hiddenfields for dropdowns*/
            //$('#profile_name').val($("#detailedProfile option:selected").text())
            $('#profile_classification').val($('#classification').val())
            $('#profile_waist_hips_code').val($('#waist_hips').val())
            $('#profile_chest_sleeves_code').val($('#chest_sleeves').val())
            $('#profile_length_code').val($('#length').val())
            /*on generate click show table */
            $('#generated_profile').show();
            $('#generateshirt').val('regenerate measurements');
            $("#generateshirt").blur();
            /*Account dynamic border genration*/
            if ($(document).width() < 769) {
                $('#account_side_bar_main').css('border','none');
            }
            else {
                division_height = $('#divwidth').height();
                $('#account_side_bar_border').css('border-right','1px solid gray');
                if (division_height > $('#account_side_bar_main').height()){
                    $('#account_side_bar_main').height(division_height);
                }
            }

          });
    });


});
