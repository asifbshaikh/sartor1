$(function () {
	$("#main-image").css('overflow', 'hidden');

// RESIZE MAIN IMAGE SECTION HEIGHT
  $(window).bind("load resize", function() {
    resizeMainImageSection();
  });

// AUTO SLIDE GALLARY AFTER 10 sec
  playinterval();



  function resizeMainImageSection(){
    if($(document).width() > 768){
      $("#main-image").css('height', 900);
      // $(".mz-figure").css('height', $(window).height()-130);
    }else{
      $("#main-image").css('height', 500);
      // $(".mz-figure").css('height', 300);
    }
  }

  function resizeMzFigure(){
    if($(document).width() > 768){
      $(".mz-figure").css('height', $(window).height()-130);
    }else{
      $(".mz-figure").css('height', 300);
    }
  }
});

function playinterval(){

  interval = setInterval(function(){displayNextImage();},10000);
  return false;
}

function stopinterval(){
  clearInterval(interval);
  return false;
}

function displayNextImage() {
  MagicZoom.next('zoom');
}

function disableSlideShow(flag){
  if(flag){
    stopinterval();
  }else{
    playinterval();
  }
}


var mzOptions = {
  onZoomIn: function(zoom_id){
    stopinterval();
  },
  onZoomOut: function(zoom_id){
    playinterval();
  }
};




