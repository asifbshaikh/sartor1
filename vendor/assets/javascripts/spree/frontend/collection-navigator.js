$(document).ready(function() {
  $('.submenu li').click(function() {
	  $('.submenu li.active').removeClass('active');
	    $(this).addClass('active');
	});
});

$( document ).ajaxStart(function() {
	$(".product-info").remove();
	$("#no-product-found").remove();
	$("#loading").css("display","block");
});

$( document ).ajaxComplete(function() {
	$("#loading").css("display","none")
});

function getProducts(collection){
	getSelectedCollection(collection);
}

getSelectedCollection = function (collection) {

  $.ajax({
    type: "POST",
    url: '/shirts-collections',
    dataType: "script",
    data: { collection:  collection}
  }).done(function( msg ) {
    $("img").unveil(200);
    showFilterLabels();
  });
}

selectedCategory = function (new_collection) {
  var collection = new_collection;
  getSelectedCollection(collection);

}
