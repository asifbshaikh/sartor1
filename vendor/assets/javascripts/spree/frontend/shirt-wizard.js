
$(window).load(function(){
  
})

//Window Scroll Slide up Slide down
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if ($(document).width() > 991) {
      if(height  >= ($("#mainHeader").height())/2) {
          // do something
          $("#container-header").slideUp(300);
      }else{
          $("#container-header").slideDown(300);
      }
    }
});

var windw = this;
$.fn.followTo = function ( pos ) {
    var $this = $('.config-widget'),
        $window = $(windw);

    $window.scroll(function(e){
      // TO ADJUST SCROLL EXACT POINT SUBTRACT 100
        if ($window.scrollTop() > pos-100) {
            $this.css({
                position: 'absolute',
                top: pos
            });
        } else {
            $this.css({
                position: 'fixed',
                top: 120
            });
        }
    });
};

$.fn.followToOptions = function ( pos ) {
    var $this = $('.window'),
        $window = $(windw);

    $window.scroll(function(e){
      // TO ADJUST SCROLL EXACT POINT SUBTRACT 100
      // :FIXME
        if ($window.scrollTop() > pos-100) {
            $this.css({
                position: 'absolute',
                top: pos + 200
            });
            $('.window_config_scroll').css('overflow','visible');
            $('.window_config_scroll').css('background-color','white');
            $('.window').css('z-index','1020');
        } else {
            $this.css({
                position: 'fixed',
                top: 'auto'
            });
            $('.window_config_scroll').css('overflow','auto');
            $('.window_config_scroll').css('background-color','');
            $('.window').css('z-index','2030');
        }
    });
};

$.fn.is_on_screen = function(){
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom || bounds.bottom < viewport.bottom && bounds.top > viewport.top + ($("#header-stick").height())));
};



$(function () {
  //SCROLL ON CLICK OF SHIRT CONFIG
  $('.detail-optionbox-selector-btn').click(function() {
      if(($('.config-widget').is_on_screen()) ) {
          $('html, body').animate({
           'scrollTop':   $('#detail-optionbox-container').offset().top - ($(".navbar-collapse").height()+1)
         }, 500);
      }
    })

  //OPEN WINDOW ONCLICK
  $('#order_collar_div').click(function(e){
    if ($(document).width() < 992) {
      $('#window_collar').toggleClass('hide');
      $('.wizard_windows').find('.window:not(#window_'+'collar'+')').addClass('hide');

      var offset = $(this).offset();
      var domWidth = $('#window_collar').width();
      var domHeight = $('#window_collar').height();
      var leftPosition = Math.abs(offset.left - domWidth - 5);
      var topPosition = offset.top;
      if (Math.abs(domHeight + offset.top) > $(window).height()){
          topPosition = $(window).height() - domHeight
      }
      $('.window').css('left',leftPosition);
      $('.window').css('top',topPosition);
    }
  });
  $('#order_cuff_div').click(function(){
    if ($(document).width() < 992) {
      $('#window_cuff').toggleClass('hide');
      $('.wizard_windows').find('.window:not(#window_'+'cuff'+')').addClass('hide');

      var offset = $(this).offset();
      var domWidth = $('#window_cuff').width();
      var domHeight = $('#window_cuff').height();
      var leftPosition = Math.abs(offset.left - domWidth - 5);
      var topPosition = offset.top;
      if (Math.abs(domHeight + offset.top) > $(window).height()){
          topPosition = $(window).height() - domHeight
      }
      $('.window').css('left',leftPosition);
      $('.window').css('top',topPosition);
    }
  });
  $('#order_pocket_div').click(function(){
    if ($(document).width() < 992) {
      $('#window_pocket').toggleClass('hide');
      $('.wizard_windows').find('.window:not(#window_'+'pocket'+')').addClass('hide');

      var offset = $(this).offset();
      var domWidth = $('#window_pocket').width();
      var domHeight = $('#window_pocket').height();
      var leftPosition = Math.abs(offset.left - domWidth - 5);
      var topPosition = offset.top;
      if (Math.abs(domHeight + offset.top) > $(window).height()){
          topPosition = $(window).height() - domHeight
      }
      $('.window').css('left',leftPosition);
      $('.window').css('top',topPosition);
    }
  });
  $('#order_button_div').click(function(){
    if ($(document).width() < 992) {
      $('#window_button').toggleClass('hide');
      $('.wizard_windows').find('.window:not(#window_'+'button'+')').addClass('hide');

      var offset = $(this).offset();
      var domWidth = $('#window_button').width();
      var domHeight = $('#window_button').height();
      var leftPosition = Math.abs(offset.left - domWidth - 5);
      var topPosition = offset.top;
      if (Math.abs(domHeight + offset.top) > $(window).height()){
          topPosition = $(window).height() - domHeight
      }
      $('.window').css('left',leftPosition);
      $('.window').css('top',topPosition);
    }
  });
  $('#order_placket_div').click(function(){
    if ($(document).width() < 992) {
      $('#window_placket').toggleClass('hide');
      $('.wizard_windows').find('.window:not(#window_'+'placket'+')').addClass('hide');

      var offset = $(this).offset();
      var domWidth = $('#window_placket').width();
      var domHeight = $('#window_placket').height();
      var leftPosition = Math.abs(offset.left - domWidth - 5);
      var topPosition = offset.top;
      if (Math.abs(domHeight + offset.top) > $(window).height()){
          topPosition = $(window).height() - domHeight
      }
      $('.window').css('left',leftPosition);
      $('.window').css('top',topPosition);
    }
  });

  //CLOSE WINDOW CLICK
  $('.close-option-box').click(function(){
      $(".sidebar").css( 'right', '-400px');
  });
  //FLOATING WINDOW CLOSE CLICK
  $('.close_window').click(function(){
      config = $(this).data('config');
      $('#'+config).toggleClass('hide');
  });
  //ON HOVER DISPLAY NAMES IN TEXT SECTION
  $('.choiceItem').hover(function() {
   var name =  $(this).attr('data-short-name');
   $('.option-description').find('p').text(name);
   
   // Set Option Description
   var id = $(this).attr('id');
   desc = $(this).data('desc');

   if (desc == 'button'){
    $('.option-description').find('p').text($(this).data('description'));
   }else{
    $('.option-description').find('p').text($('#'+desc).text());
   }

  });
  //ON MOUSE LEAVE SET DEFAULT TEXT
  $('.choiceItem').mouseleave(function() {
    $('.option-description').find('p').text("Click To Select");
  });


  //SET ACTIVE CLASS FOR SELECTED
  $('.choiceItem').click(function(){
        //SET SELECTED VALUE TO DROPDOWN
        selectedItemId = $(this).find('input').attr('value');
        name = $(this).find('input').attr('name');
        $('#order_' + name).val(selectedItemId);
        // $( '#order_' + name).change(); /*TAKES MORE TIME*/

        // SWITCH TO CUSTOM MODE
        $('#customized_style_config').click();

        // SET VALUES FOR SELECT BOXES
        switch (name) {
            case 'collar_code':
                collar_value = $( "#order_collar_code").val();
                break;
            case 'cuff_code':
                cuff_value = $( "#order_cuff_code").val();
                break;
            case 'pocket_code':
                pocket_value = $( "#order_pocket_code").val();
                break;
            case 'button_code':
                button_value = $( "#order_button_code").val();
                break;
            case 'placket_code':
                placket_value = $( "#order_placket_code").val();
                break;
            default:
                break;
        }


        $("#" + selectedItemId).prop("checked", true);
        var optionType = $(this).data('option-type');
        var current_tab = $('.tabs-wizard').find('li.tabs__item_wizard.current').attr("id");

        //SET ACTIVE FOR WINDOW
        config_type = $(this).attr("id");
        selectedSidebar = $(this).data('sidebar');

        $('#'+selectedSidebar).find(".choiceItem").removeClass('selected');
        $('#'+config_type).addClass('selected');
        $('#min_'+config_type).addClass('selected');

        // CLOSE ALL WINDOWS
        $('.wizard_windows').find('.window').addClass('hide');
  });

  //INITIALIZING VALUES FOR CUSTOMIZED CHANGES
  collar_value =  undefined;
  cuff_value =    undefined;
  pocket_value =  undefined;
  button_value =  undefined;
  placket_value = undefined;

  //ONCHANGE FOR THE SELECT BOXES
  //COLLARCHANGE
  $( "#order_collar_code").change(function() {
        $('#customized_style_config').click();
        collar_value = $( "#order_collar_code").val();
  });
  //CUFFCHANGE
  $( "#order_cuff_code").change(function() {
        $('#customized_style_config').click();
        cuff_value = $( "#order_cuff_code").val();
  });
  //POCKETCHANGE
  $( "#order_pocket_code").change(function() {
        $('#customized_style_config').click();
        pocket_value = $( "#order_pocket_code").val();
  });
  //BUTTONCHANGE
  $( "#order_button_code").change(function() {
        $('#customized_style_config').click();
        button_value = $( "#order_button_code").val();
  });
  //PLACKETCHANGE
  $( "#order_placket_code").change(function() {
        $('#customized_style_config').click();
        placket_value = $( "#order_placket_code").val();
  });

  //SELECTED PROFILE ID
  $('.profile-list').click(function(){
      selected_profile_id = $(this).data('val');
      $('#profile_id').val(selected_profile_id);
  });

  // SHOW SHIRT CONFIG WINDOWS
  $('.config-item').click(function(){
  //TO ADJUST WINDOW POSITIONS
  var window_width = $(window).width()
  if(window_width > 700){
    var top_position = $('#add-to-cart-button').position().top
    if(top_position < 120){
      $('.window').css('bottom',top_position + 10)
    }else{
      $('.window').css('bottom',60)
    }
  }

  config = $(this).data('config');
   $('.wizard_windows').find('.window:not(#window_'+config+')').addClass('hide');
    $('#window_'+config).toggleClass('hide');
  });


    //ON SELECT CONFIG
    /*$('.config_option_button').click(function() {
        selectedItemId = $(this).find('input').attr('value');
        name = $(this).find('input').attr('name');
        $('#order_' + name).val(selectedItemId);
        $( '#order_' + name).change();
        $("#" + selectedItemId).prop("checked", true);
        var optionType = $(this).data('option-type');
        var current_tab = $('.tabs-wizard').find('li.tabs__item_wizard.current').attr("id");
        $('.wizard_windows').find('.window').addClass('hide');
    });*/

    //PRODUCT ID FOR FINDING PRODUCTS SHIRTCONFIGURATIONS IN CONTROLLER
    product_id = $('#product_id').val();

    //ONCLICK OF TABS AJAX RESPONSE TO SETSELECT BOXES
    //YOUR USUAL TAB
    $('#usual_style_config').click(
    function() {
      var style = "usual_style"
      $.ajax({
            type: "POST",
            url: '/shirts-tabs-click',
            dataType: "json",
            data: { config_tab:  style ,product_id: product_id}
            }).success(function( msg ) {
            //console.log(JSON.stringify(msg.usual_shirt_configuration))
            //console.log(JSON.stringify(msg.default_shirt_configuration))
            /*SETTING THE RESPONSE TO THE SELECT BOXES*/
            //COLLAR
            $('#order_collar_code').val(msg.usual_shirt_configuration.collar_code);
            $('#order_collar_text').text($('#order_collar_code option:selected').text());
            $('#input_collar').find('ul').find('li').removeClass("selected");
            $('#collar-sidebar').find('.choiceItem').removeClass("selected");

            $('#collar_'+msg.usual_shirt_configuration.collar_code+'').addClass("selected");
            $('#min_collar_'+msg.usual_shirt_configuration.collar_code+'').addClass("selected");
            //CUFF
            $('#order_cuff_code').val(msg.usual_shirt_configuration.cuff_code);
            $('#order_cuff_text').text($('#order_cuff_code option:selected').text());
            $('#input_cuff').find('ul').find('li').removeClass("selected");
            $('#cuff-sidebar').find('.choiceItem').removeClass("selected");

            $('#cuff_'+msg.usual_shirt_configuration.cuff_code+'').addClass("selected");
            $('#min_cuff_'+msg.usual_shirt_configuration.cuff_code+'').addClass("selected");
            //BUTTON
            $('#order_button_code').val(msg.usual_shirt_configuration.button_code);
            $('#order_button_text').text($('#order_button_code option:selected').text());
            $('#input_button').find('ul').find('li').removeClass("selected");
            $('#button-sidebar').find('.choiceItem').removeClass("selected");

            $('#button_'+msg.usual_shirt_configuration.button_code+'').addClass("selected");
            $('#min_button_'+msg.usual_shirt_configuration.button_code+'').addClass("selected");
            //POCKET
            $('#order_pocket_code').val(msg.usual_shirt_configuration.pocket_code);
            $('#order_pocket_text').text($('#order_pocket_code option:selected').text());
            $('#input_pocketPlacement').find('ul').find('li').removeClass("selected");
            $('#pocket-sidebar').find('.choiceItem').removeClass("selected");

            $('#pocketPlacement_'+msg.usual_shirt_configuration.pocket_code+'').addClass("selected");
            $('#min_pocketPlacement_'+msg.usual_shirt_configuration.pocket_code+'').addClass("selected");
            //PLACKET
            $('#order_placket_code').val(msg.usual_shirt_configuration.placket_code);
            $('#order_placket_text').text($('#order_placket_code option:selected').text());
            $('#input_placket').find('ul').find('li').removeClass("selected");
            $('#placket-sidebar').find('.choiceItem').removeClass("selected");

            $('#placket_'+msg.usual_shirt_configuration.placket_code+'').addClass("selected");
            $('#min_placket_'+msg.usual_shirt_configuration.placket_code+'').addClass("selected");
            /*HIGHLIGHTING SELECTED BUTTONS*/
            $('#usual_style_config').addClass("selectedbutton");
            $('#customized_style_config').removeClass("selectedbutton");
            $('#sartor_style_config').removeClass("selectedbutton");
        });
          	$('.save-usual-style-btn-section').fadeOut(); //HIDE SAVE AS USUAL BTN
    });

  //SARTOR STYLE TAB
  $('#sartor_style_config').click(
  function() {
    var style = "sartor_style"
    $.ajax({
            type: "POST",
            url: '/shirts-tabs-click',
            dataType: "json",
            data: { config_tab: style ,product_id: product_id}
          }).success(function( msg ) {
            /*SETTING THE RESPONSE TO THE SELECT BOXES*/
            //COLLAR
            $('#order_collar_code').val(msg.default_shirt_configuration.collar_code);
            $('#order_collar_text').text($('#order_collar_code option:selected').text());
            $('#input_collar').find('ul').find('li').removeClass("selected");
            $('#collar-sidebar').find('.choiceItem').removeClass("selected");

            $('#collar_'+msg.default_shirt_configuration.collar_code+'').addClass("selected");
            $('#min_collar_'+msg.default_shirt_configuration.collar_code+'').addClass("selected");

            //CUFF
            $('#order_cuff_code').val(msg.default_shirt_configuration.cuff_code);
            $('#order_cuff_text').text($('#order_cuff_code option:selected').text());
            $('#input_cuff').find('ul').find('li').removeClass("selected");
            $('#cuff-sidebar').find('.choiceItem').removeClass("selected");

            $('#cuff_'+msg.default_shirt_configuration.cuff_code+'').addClass("selected");
            $('#min_cuff_'+msg.default_shirt_configuration.cuff_code+'').addClass("selected");
            //BUTTON
            $('#order_button_code').val(msg.default_shirt_configuration.button_code);
            $('#order_button_text').text($('#order_button_code option:selected').text());
            $('#input_button').find('ul').find('li').removeClass("selected");
            $('#button-sidebar').find('.choiceItem').removeClass("selected");

            $('#button_'+msg.default_shirt_configuration.button_code+'').addClass("selected");
            $('#min_button_'+msg.default_shirt_configuration.button_code+'').addClass("selected");
            //POCKET
            $('#order_pocket_code').val(msg.default_shirt_configuration.pocket_code);
            $('#order_pocket_text').text($('#order_pocket_code option:selected').text());
            $('#input_pocketPlacement').find('ul').find('li').removeClass("selected");
            $('#pocket-sidebar').find('.choiceItem').removeClass("selected");

            $('#pocketPlacement_'+msg.default_shirt_configuration.pocket_code+'').addClass("selected");
            $('#min_pocketPlacement_'+msg.default_shirt_configuration.pocket_code+'').addClass("selected");
            //PLACKET
            $('#order_placket_code').val(msg.default_shirt_configuration.placket_code);
            $('#order_placket_text').text($('#order_placket_code option:selected').text());
            $('#input_placket').find('ul').find('li').removeClass("selected");
            $('#placket-sidebar').find('.choiceItem').removeClass("selected");

            $('#placket_'+msg.default_shirt_configuration.placket_code+'').addClass("selected");
            $('#min_placket_'+msg.default_shirt_configuration.placket_code+'').addClass("selected");
            /*HIGHLIGHTING SELECTED BUTTONS*/
            $('#sartor_style_config').addClass("selectedbutton");
            $('#customized_style_config').removeClass("selectedbutton");
            $('#usual_style_config').removeClass("selectedbutton");
        });
          	$('.save-usual-style-btn-section').fadeOut(); //HIDE SAVE AS USUAL BTN
  });

  //CUSTOMIZED STYLE TAB
  $('#customized_style_config').click(
  function() {
    var style= "customized_style"
    if($('#p_id').val()){
       style= "lineitem_style" 
    }
    if($('#wished_product_id').val()){
      style= "wishlist_style"
    }
     $.ajax({
            type: "POST",
            url: '/shirts-tabs-click',
            dataType: "json",
            data: { config_tab:  style ,product_id: product_id, pid: $('#p_id').val(),wished_product_id: $('#wished_product_id').val()}
         }).success(function( msg ) {
            /*SETTING THE RESPONSE TO THE SELECT BOXES*/
            //COLLAR
            $('#input_collar').find('ul').find('li').removeClass("selected");
            $('#collar-sidebar').find('.choiceItem').removeClass("selected");
            
            if (collar_value != undefined) {
                $('#order_collar_code').val(collar_value);
                $('#min_collar_'+collar_value).addClass("selected");
                $('#collar_'+collar_value).addClass("selected");
             }
             else{
                $('#order_collar_code').val(msg.default_shirt_configuration.collar_code);
                $('#min_collar_'+msg.default_shirt_configuration.collar_code).addClass("selected");
                $('#collar_'+msg.default_shirt_configuration.collar_code).addClass("selected");
             }
             $('#order_collar_text').text($('#order_collar_code option:selected').text());
            //CUFF
            $('#input_cuff').find('ul').find('li').removeClass("selected");
            $('#cuff-sidebar').find('.choiceItem').removeClass("selected");

            if (cuff_value != undefined) {
                $('#order_cuff_code').val(cuff_value);
                $('#min_cuff_'+cuff_value).addClass("selected");
                $('#cuff_'+cuff_value).addClass("selected");
             }
            else{
                $('#order_cuff_code').val(msg.default_shirt_configuration.cuff_code);
                $('#min_cuff_'+msg.default_shirt_configuration.cuff_code).addClass("selected");
                $('#cuff_'+msg.default_shirt_configuration.cuff_code).addClass("selected");
             }
            $('#order_cuff_text').text($('#order_cuff_code option:selected').text());
            //POCKET
            $('#input_pocketPlacement').find('ul').find('li').removeClass("selected");
            $('#pocket-sidebar').find('.choiceItem').removeClass("selected");

            if (pocket_value != undefined) {
                $('#order_pocket_code').val(pocket_value);
                $('#min_pocketPlacement_'+pocket_value).addClass("selected");
                $('#pocketPlacement_'+pocket_value).addClass("selected");
             }
            else{
                $('#order_pocket_code').val(msg.default_shirt_configuration.pocket_code);
                $('#min_pocketPlacement_'+msg.default_shirt_configuration.pocket_code).addClass("selected");
                $('#pocketPlacement_'+msg.default_shirt_configuration.pocket_code).addClass("selected");                
             }
             $('#order_pocket_text').text($('#order_pocket_code option:selected').text());
            //BUTTON
            $('#input_button').find('ul').find('li').removeClass("selected");
            $('#button-sidebar').find('.choiceItem').removeClass("selected");

            if (button_value != undefined) {
                $('#order_button_code').val(button_value);
                $('#min_button_'+button_value).addClass("selected");
                $('#button_'+button_value).addClass("selected");
             }
             else{
                $('#order_button_code').val(msg.default_shirt_configuration.button_code);
                $('#min_button_'+msg.default_shirt_configuration.button_code).addClass("selected");
                $('#button_'+msg.default_shirt_configuration.button_code).addClass("selected");
             }
            $('#order_button_text').text($('#order_button_code option:selected').text());
            //PLACKET
            $('#input_placket').find('ul').find('li').removeClass("selected");
            $('#placket-sidebar').find('.choiceItem').removeClass("selected");

            if (placket_value != undefined) {
                $('#order_placket_code').val(placket_value);
                $('#min_placket_'+placket_value).addClass("selected");
                $('#placket_'+placket_value).addClass("selected");
             }
             else{
                $('#order_placket_code').val(msg.default_shirt_configuration.placket_code);
                $('#min_placket_'+msg.default_shirt_configuration.placket_code).addClass("selected");
                $('#placket_'+msg.default_shirt_configuration.placket_code).addClass("selected");
             }
            $('#order_placket_text').text($('#order_placket_code option:selected').text());

            /*HIGHLIGHTING SELECTED BUTTONS*/
            $('#customized_style_config').addClass("selectedbutton");
            $('#sartor_style_config').removeClass("selectedbutton");
            $('#usual_style_config').removeClass("selectedbutton");
        });
          	$('.save-usual-style-btn-section').fadeIn(); //SHOW SAVE AS USUAL BTN
  });



  //SAVE AS YOUR USUAL CLICK
  $('#save_usual_style_btn').click(
  	function() {
            var configurations = {};
            configurations.collar_code =  $('#order_collar_code').val();
            configurations.pocket_code = $('#order_pocket_code').val();
            configurations.cuff_code = $('#order_cuff_code').val();
            configurations.button_code =  $('#order_button_code').val();
            configurations.placket_code = $('#order_placket_code').val();
            configurations.profile_id = $('#profile_id').val();
            var pid=$('#lineitem_id').val();
        $.ajax({
    	    	type: "POST",
    	    	url: '/shirts-usual-style',
    	    	dataType: "json",
    	    	data: { configurations:  configurations}
  	  	}).success(function( msg ) {
           $('#usual_style_config').click();
           $('.save-usual-style-btn-section').fadeOut(); //SHOW SAVE AS USUAL BTN
  	  	});
  });

  if (($('#p_id').val()) || ($('#wished_product_id').val())){
    $('#customized_style_config').click();
  }else{
    $('#sartor_style_config').click();
  }

});






