jQuery(document).ready(function ($) {

        var filters = {};

        var productContainer = $('#product-container');

        $('#filters').on( 'click', 'label', function() {
          var $this = $(this);
          // get group key
          var $buttonGroup = $this.parents('.btn-group');
          var filterGroup = $buttonGroup.attr('data-filter-group');
          // set filter for group
          filters[ filterGroup ] = $this.attr('data-filter');
          // combine filters
          var filterValue = '';
          for ( var prop in filters ) {
            filterValue += filters[ prop ];
          }
        });

        productContainer.imagesLoaded( function() {
        });

        $("img").unveil(200, function() {

            $(this).load( function() {
            });
        });
    });
