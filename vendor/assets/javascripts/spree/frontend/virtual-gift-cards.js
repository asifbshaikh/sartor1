  var certValueComments = {};
  certValueComments['135'] = "A $135 certificate will allow the recipient to choose from the majority of our shirt collection.";
  certValueComments['150'] = "A $150 certificate will allow the recipient to choose from our entire shirt collection, including premium shirts.";
  certValueComments['300'] = "A $300 certificate will allow the recipient to choose any 2 shirts.";
  certValueComments['450'] = "A $450 certificate will allow the recipient to choose any 3 shirts. Lucky fellow!";

  function recalcValues() {
    gift_card_price = parseInt($("#variant_id").children(":selected").text().substring(1));
    comment = certValueComments[gift_card_price];
    $("#gift_value_comments").text(comment);

    tie_price = parseInt($("#inset_product_price").text().substring(1)) || 0;
    total_price = (gift_card_price + tie_price).toFixed(2);
    $("#_header_total_price").text(total_price);
    $("#_footer_gift_card_price").text(gift_card_price);
    $("#_footer_total_price").text(total_price);

  }

  $(function() {
    recalcValues();

    if($("#virtual_card_type").is(':checked')){
    	 $('#shipping-address-form').hide();
    }

    $("#variant_id").click(
      function() {
        recalcValues();
      }
    );

    /*for setting values while submitting*/
    $('#submit-gift-card').click(function(){
      if($('#gift_card_type_current').val() === 'virtual'){
        set_default_shipping_address();
      }
      $('#gift_card_form').submit();
    });

    /*for setting values while submitting*/
    $('#add-to-cart-button').click(function(){
      if($('#gift_card_type_current').val() === 'virtual'){
        set_default_shipping_address();
      }
      $('#gift_card_form').submit();
    });




    /*if gift_card_type_current field is virtual click on virual checkbox else do nothing*/
    if($('#gift_card_type_current').val() === 'virtual')
    {
      $('#virtual_card_type').prop('checked',true);
        if ($("#virtual_card_type").is(':checked'))
        {
          $('#shipping-address-form').hide();
        };
    }

    /*On click of checkbox-Virtual Card Type*/
    $('#virtual_card_type').click(function() {

        $('#gift_card_type_current').val('virtual');

        if ($("#virtual_card_type").is(':checked')){
          $('#shipping-address-form').hide();
          $('#bundle_tie_with_giftcard').hide();
          $('#product-inset').html("");
          recalcValues();

        };
    });
    /*On click of checkbox-Physical Card Type*/
    $('#physical_card_type').click(function() {

        $('#gift_card_type_current').val('physical');

        if($("#physical_card_type").is(':checked')){
          $('#shipping-address-form').show();
          $('#bundle_tie_with_giftcard').show();
          /*reset_default_shipping_address();*/
          $('#product-inset').html("");
          recalcValues();
        }
    });
    /*Setting N/A to fields as we cannot keep them blank while submitting the form*/
    function set_default_shipping_address(){
      $("#giftcard_address_firstname").val("N/A");
      if($("#giftcard_address_lastname").val() == 0){
        $("#giftcard_address_lastname").val("");
      }
      $("#giftcard_address_address1").val("N/A");
      $("#giftcard_address_address2").val("N/A");
      $("#giftcard_address_city").val("N/A");
      $("#giftcard_address_state").val("N/A");
      $("#giftcard_address_zipcode").val("90001");
      $("#giftcard_address_phone").val("1234567890");
    }
    /*Setting blank value to the fields on click of the physical checkbox*/
    function reset_default_shipping_address(){
      $("#giftcard_address_firstname").val("");
      if($("#giftcard_address_lastname").val() == 0)
        $("#giftcard_address_lastname").val("");
      $("#giftcard_address_address1").val("");
      $("#giftcard_address_address2").val("");
      $("#giftcard_address_city").val("");
      $("#giftcard_address_zipcode").val("");
      $("#giftcard_address_state").val("");
      $("#giftcard_address_phone").val("");
    }


      function updateFieldDisplay() {
        var recipient = $('#delivery_type_recipient');
        if ($('#send_on_date').is(':checked')) {
          $('#datepicker_field').removeAttr('readonly');
        }
        else {
          $('#datepicker_field').attr('readonly','readonly');
        }
      }

      updateFieldDisplay();
        $('input:radio').change(function() {
        updateFieldDisplay();
      });
  });
