$(function () {

	$('#manage_profile_link').click(function(){
		selectedProfile = $('#profile_id').val();
		url = "/profiles/";
      	$( location ).attr("href", url);
	});

 $('#shirtdetail_profile_link').click(function(){
    var configurations = {};
    configurations.collar_code =  $('#order_collar_code').val();
    configurations.pocket_code = $('#order_pocket_code').val();
    configurations.cuff_code = $('#order_cuff_code').val();
    configurations.button_code =  $('#order_button_code').val();
    configurations.placket_code = $('#order_placket_code').val();
    configurations.profile_id = $('#profile_id').val();
    $.ajax({
        type: "POST",
        url: '/shirts-shirtconfig',
        dataType: "json",
        data: { configurations:  configurations}
      }).success(function( msg ) {
         window.location = '/login';
      });
  });

  $('#shirtdetail_measurement_link').click(function(){
    var configurations = {};
    configurations.collar_code =  $('#order_collar_code').val();
    configurations.pocket_code = $('#order_pocket_code').val();
    configurations.cuff_code = $('#order_cuff_code').val();
    configurations.button_code =  $('#order_button_code').val();
    configurations.placket_code = $('#order_placket_code').val();
    configurations.profile_id = $('#profile_id').val();
    configurations.redirect_to = '/profiles';
    $.ajax({
        type: "POST",
        url: '/shirts-shirtconfig',
        dataType: "json",
        data: { configurations:  configurations}
      }).success(function( msg ) {
         window.location = '/login';
      });
  });


 	// RESIZE THE BANNER HEIGHT
	var resizeBannerHeightFlag = false;
    $(window).resize(function() {
    	if(resizeBannerHeightFlag){
	        var bodyheight = $('.product-item').height();
	        $("#banner-image").height(bodyheight);
    	}
    	resizeBannerHeightFlag = true;
    }).resize();

    //FABRIC DETAIL
    var weight_code = $('#fabric-detail-weight').val();
    $('#rating-container-weight').find('.dot').slice(0,weight_code).show().addClass('active');

    var opacity_code = $('#fabric-detail-opacity').val();
    $('#rating-container-opacity').find('.dot').slice(0,opacity_code).show().addClass('active');

    var easy_of_care_code = $('#fabric-detail-easy-of-care').val();
    $('#rating-container-easy-of-care').find('.dot').slice(0,easy_of_care_code).show().addClass('active');

    //Add configuration value to the hidden field in form for wishlist
    $('#wishlist_form_generate').click(
      function() {
        var wishlist_configurations = {};
        wishlist_configurations.collar_code = $('#order_collar_code').val();
        wishlist_configurations.cuff_code = $('#order_cuff_code').val();
        wishlist_configurations.pocket_code = $('#order_pocket_code').val();
        wishlist_configurations.placket_code = $('#order_placket_code').val();
        wishlist_configurations.button_code = $('#order_button_code').val();
        wishlist_configurations.length_code = $('input[name="shirt_configuration[length_code]"]:checked').val();
        wishlist_configurations.profile_id = $('#profile_id').val();
        wishlist_configurations.comment = $('#order_comment').val();
        wishlist_configurations.quantity = $('#quantity').val();


      //alert("collar_code"+wishlist_configurations.collar_code+"cuff_code"+wishlist_configurations.cuff_code+"pocket_code"+wishlist_configurations.pocket_code+"length_code"+wishlist_configurations.length_code);

            $("#wished_product_collar_code").val(wishlist_configurations.collar_code);
            $("#wished_product_cuff_code").val(wishlist_configurations.cuff_code);
            $("#wished_product_pocket_code").val(wishlist_configurations.pocket_code);
            $("#wished_product_placket_code").val(wishlist_configurations.placket_code);
            $("#wished_product_button_code").val(wishlist_configurations.button_code);
            $("#wished_product_length_code").val(wishlist_configurations.length_code);
            $("#wished_product_profile_id").val(wishlist_configurations.profile_id);
            $("#wished_product_comment").val(wishlist_configurations.comment);
            $("#wished_product_quantity").val(wishlist_configurations.quantity);
            //submitting form getting data from selected shirt configuration and setting to fields
          $('#wishlist_form_button_click').click();
    });
    $('#wishlist_form_edit').click(
      function() {
        var wished_product_configuration = {};
        wished_product_configuration.collar_code = $('#order_collar_code').val();
        wished_product_configuration.cuff_code = $('#order_cuff_code').val();
        wished_product_configuration.pocket_code = $('#order_pocket_code').val();
        wished_product_configuration.placket_code = $('#order_placket_code').val();
        wished_product_configuration.button_code = $('#order_button_code').val();
        wished_product_configuration.length_code = $('input[name="shirt_configuration[length_code]"]:checked').val();
        wished_product_configuration.profile_id = $('#profile_id').val();
        wished_product_configuration.comment = $('#order_comment').val();
        wished_product_configuration.quantity = $('#quantity').val();
        wished_product_configuration.wishlist_id = $('#wished_product_id').val();
        
        $.ajax({
        type: "POST",
        url: '/update_wished_products',
        dataType: "json",
        data: { wished_product_configuration:  wished_product_configuration}
      }).success(function( msg ) {
         window.location = msg.wishlist_path;
      });
        
    });

});
