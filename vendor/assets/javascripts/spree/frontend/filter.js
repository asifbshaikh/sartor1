$(document).ready(function() {
  $('#filter .field').each(function() {
    var node = $(this);
    var span = $("span", node);
    var checkbox = $("input[type='checkbox']", node);
    span.click(function() {
      checkbox.click();
    });
  });

  $('#filter-button').click(function(event){
    filterShirts();
    event.preventDefault();
  });

  $(this).find('*[type*="checkbox"]').change(function(event){
    filterShirts();
    event.preventDefault();
  });
});

filterShirts = function() {
  var checkboxMap = {};
  $('#filter .field').each(function() {
    var node = $(this);
    var span = $("span", node);
    var checkbox = $("input[type='checkbox']", node);
    if (checkbox.is(":checked")){
      checkboxMap[checkbox.attr('id')] = true;
    }
  });

  $.ajax({
    type: "POST",
    url: '/shirts-filter',
    dataType: "script",
    data: { checkboxMap: checkboxMap }
  }).done(function( msg ) {
    $("img").unveil(200);
    showFilterLabels();
  });
}

showFilterLabels = function() {
  $('#filter .field').each(function() {
    var node = $(this);
    var span = $("span", node);
    var checkbox = $("input[type='checkbox']", node);
    if (checkbox.is(":checked")){
      lableId = '#filterLabel_' + checkbox.attr('id')
      $(lableId).css('display', 'inline-block');
    }else{
      lableId = '#filterLabel_' + checkbox.attr('id')
      $(lableId).hide();
    }
  });
}
