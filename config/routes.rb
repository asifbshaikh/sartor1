Sartor::Application.routes.draw do

  mount Spree::Core::Engine, :at => '/'

  Spree::Core::Engine.routes.draw do
    resources :appointment_contacts

    resources :profiles
    resources :simple_profiles
    resources :detailed_profiles
    resources :shirt_profiles

    resources :ja_giftsets

    get '/privacy', :to => "info#privacy"
    get '/terms', :to => "info#terms_of_service"
    get '/shipping', :to => "info#shipping"
    get '/returns', :to => "info#returns"
    get '/about_sartor', :to => "info#about_sartor"
    get '/perscholas', :to => "info#per_scholas"
    get '/mygift', :to => "info#mygift"

    get '/password/change' => 'user_passwords#edit', :as => :simple_edit_password

    post '/shirts-shirtconfig' => 'products#shirtConfigSession'
    get '/shirts' => 'products#index_shirts'
    post '/shirts-filter' => 'products#filtered_shirts'
    post '/shirts-tabs-click' => 'products#shirtresponse'
    post '/shirts-collections' => 'products#shirts_collections'
    post '/shirts-usual-style' => 'your_usual#add'
    get '/ties' => 'products#index_ties'

    get '/johnallans' => 'ja_giftsets#new'

    get '/products/show_no_tie'


    #Routes for virtual gift cards
    get '/gifting' => 'virtual_gift_cards#new'
    post '/gifting' => 'virtual_gift_cards#create'
    get  '/admin/users/virtual_gift_cards' => "admin/gift_cards#index", :as => :admin_virtual_gift_cards
    get  '/admin/users/:id/virtual_gift_cards/' => "admin/gift_cards#show", :as => :admin_virtual_gift_card
    post '/admin/users/redeem_gift_cards/' => "admin/gift_cards#redeem_gift_cards_complete"
    get '/admin/users/:id/redeem_gift_cards/' => "admin/gift_cards#redeem_gift_cards", :as => :admin_redeem_gift_cards
    post '/user/redeem-gift-cards' => 'users#redeem_gift_cards'

    put '/api/shipments/:id/update_item' => "api/shipments#update_item", :defaults => { :format => 'json' }
    get '/admin/orders/:id/detail_profile' => "admin/detailed_profiles#new"
    get '/admin/orders/:id/users/new' =>  "admin/users#new"
    get '/admin/orders/:id/show' => "admin/orders#show", :as => :show_admin_order
    put '/admin/orders/:id/show' => "admin/orders#show", :as => :show_admin_order_put
    put '/admin/orders/:id/save-jobs' =>  "admin/show_mockup#save_jobs_to_line_items"
    put '/admin/orders/:id/update-shirt-status' =>  "admin/show_mockup#update_shirt_status"
    put '/admin/orders/:id/add-tracking-number' =>  "admin/show_mockup#add_tracking_number"
    put '/api/shipments/:id/delete_line_item' => "api/shipments#delete_line_item", :defaults => {:format=>"json"}
    get '/admin/update-stocks' => "admin/stock_locations#update_stocks_for_all_products", :as => :update_stocks
    #routes for measurement profile
    post '/admin/admin-get-measurements' => "admin/create_shirt#create_my_shirt"

    #Routes for CSV uploads
    post '/admin/read_csv' => "admin/csv_uploaders#read_csv"
    get '/admin/csv/show' => "admin/csv_uploaders#show", :as => :admin_csv_show
    get '/admin/historical_orders' => "admin/historical_orders#show", :as => :admin_historical_orders_show
    post '/admin/historical_order_csv' => "admin/historical_orders#import_historical_orders"
    get '/admin/csv/notify' => "admin/csv_uploaders#notify", :as => :admin_import_notify
    #Routes for wishlist
    
    get '/wishlist/refer_to_friend/:id' => "wishlist_mailer#email_form", :as => :wishlist_email_to_friend
    post '/wishlist/refer_to_friend/' => "wishlist_mailer#send_mail"
    post '/update_wished_products' => 'wished_products#update_wished_product'    


    #Routes for refer sartor to a friend
    get '/referral/referral_url_to_friend/:link' => "referral_mailer#email_form", :as => :referral_url_email_to_friend
    post '/referral/referral_url_to_friend/' => "referral_mailer#send_mail"

    #Frontend account page
    get '/account/orders' => 'users#show_orders'
    get '/account/history' => 'users#show_history'
    get '/account/address' => 'users#show_address'
    get '/account/sartor-wardrobe' => 'users#sartor_wardrobe'
    get '/account/loyalty' => 'users#loyalty_points'
    get '/account/redeem' => 'users#redeem_cards'
    post '/shirt/wardrobe' => 'wardrobes#show', :as => :show_wardrobe

    #appointment routes
    get '/sartor_office' => 'appointment_contacts#sartor_office'
    get '/office_visit' => 'appointment_contacts#office_visit'

    #routes for contact us
    get '/contact' => 'contact_us#show'
    post '/contact' => 'contact_us#create'

    #routes for measurement profile
    get '/get-detailedProfile' => 'shirt_profiles#get_detailed_profile'
    post '/generate-measurements' => 'shirt_profiles#createnewmeasurements'

    post '/removeUnavailableLineItem' => 'orders#removeUnavailableLineItem'
    
    namespace :admin do
      resources :users do
        resources :profiles
        resources :detailed_profiles
        resources :shirt_profiles
        member do
          get 'invite'
        end
      end
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
