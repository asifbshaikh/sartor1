# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
Spree.config do |config|
  # Example:
  # Uncomment to override the default site name.
  # config.site_name = "Spree Demo Site"

  config.admin_products_per_page = 100
  config.products_per_page = 100

  config.allow_guest_checkout = false
  config.max_level_in_taxons_menu = 2
end

Spree::Config[:check_for_spree_alerts] = false

Spree.user_class = "Spree::User"
require 'return_to'

Rails.application.config.spree.promotions.rules << Spree::Promotion::Rules::ItemCount
Rails.application.config.spree.promotions.rules << Spree::Promotion::Rules::ProductType

Spree::PermittedAttributes.user_attributes << :first_name
Spree::PermittedAttributes.user_attributes << :last_name
Spree::PermittedAttributes.user_attributes << :subscribed
Spree::PermittedAttributes.line_item_attributes << :profile_id

Spree::Ability.register_ability(AbilitySartor)

Spree::StoreCredits::Configuration.set_configs(non_expiring_credit_types: ['Non-expiring'])
